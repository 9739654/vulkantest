#include "ImGuiHandler.hpp"

const float ImGuiHandler::DEFAULT_FONT_SIZE = 18.0f;
// TODO przenieść gdzie indziej?
const uint32_t ImGuiHandler::glslVertShaderSpv[] =
{
	0x07230203,0x00010000,0x00080001,0x0000002e,0x00000000,0x00020011,0x00000001,0x0006000b,
	0x00000001,0x4c534c47,0x6474732e,0x3035342e,0x00000000,0x0003000e,0x00000000,0x00000001,
	0x000a000f,0x00000000,0x00000004,0x6e69616d,0x00000000,0x0000000b,0x0000000f,0x00000015,
	0x0000001b,0x0000001c,0x00030003,0x00000002,0x000001c2,0x00040005,0x00000004,0x6e69616d,
	0x00000000,0x00030005,0x00000009,0x00000000,0x00050006,0x00000009,0x00000000,0x6f6c6f43,
	0x00000072,0x00040006,0x00000009,0x00000001,0x00005655,0x00030005,0x0000000b,0x0074754f,
	0x00040005,0x0000000f,0x6c6f4361,0x0000726f,0x00030005,0x00000015,0x00565561,0x00060005,
	0x00000019,0x505f6c67,0x65567265,0x78657472,0x00000000,0x00060006,0x00000019,0x00000000,
	0x505f6c67,0x7469736f,0x006e6f69,0x00030005,0x0000001b,0x00000000,0x00040005,0x0000001c,
	0x736f5061,0x00000000,0x00060005,0x0000001e,0x73755075,0x6e6f4368,0x6e617473,0x00000074,
	0x00050006,0x0000001e,0x00000000,0x61635375,0x0000656c,0x00060006,0x0000001e,0x00000001,
	0x61725475,0x616c736e,0x00006574,0x00030005,0x00000020,0x00006370,0x00040047,0x0000000b,
	0x0000001e,0x00000000,0x00040047,0x0000000f,0x0000001e,0x00000002,0x00040047,0x00000015,
	0x0000001e,0x00000001,0x00050048,0x00000019,0x00000000,0x0000000b,0x00000000,0x00030047,
	0x00000019,0x00000002,0x00040047,0x0000001c,0x0000001e,0x00000000,0x00050048,0x0000001e,
	0x00000000,0x00000023,0x00000000,0x00050048,0x0000001e,0x00000001,0x00000023,0x00000008,
	0x00030047,0x0000001e,0x00000002,0x00020013,0x00000002,0x00030021,0x00000003,0x00000002,
	0x00030016,0x00000006,0x00000020,0x00040017,0x00000007,0x00000006,0x00000004,0x00040017,
	0x00000008,0x00000006,0x00000002,0x0004001e,0x00000009,0x00000007,0x00000008,0x00040020,
	0x0000000a,0x00000003,0x00000009,0x0004003b,0x0000000a,0x0000000b,0x00000003,0x00040015,
	0x0000000c,0x00000020,0x00000001,0x0004002b,0x0000000c,0x0000000d,0x00000000,0x00040020,
	0x0000000e,0x00000001,0x00000007,0x0004003b,0x0000000e,0x0000000f,0x00000001,0x00040020,
	0x00000011,0x00000003,0x00000007,0x0004002b,0x0000000c,0x00000013,0x00000001,0x00040020,
	0x00000014,0x00000001,0x00000008,0x0004003b,0x00000014,0x00000015,0x00000001,0x00040020,
	0x00000017,0x00000003,0x00000008,0x0003001e,0x00000019,0x00000007,0x00040020,0x0000001a,
	0x00000003,0x00000019,0x0004003b,0x0000001a,0x0000001b,0x00000003,0x0004003b,0x00000014,
	0x0000001c,0x00000001,0x0004001e,0x0000001e,0x00000008,0x00000008,0x00040020,0x0000001f,
	0x00000009,0x0000001e,0x0004003b,0x0000001f,0x00000020,0x00000009,0x00040020,0x00000021,
	0x00000009,0x00000008,0x0004002b,0x00000006,0x00000028,0x00000000,0x0004002b,0x00000006,
	0x00000029,0x3f800000,0x00050036,0x00000002,0x00000004,0x00000000,0x00000003,0x000200f8,
	0x00000005,0x0004003d,0x00000007,0x00000010,0x0000000f,0x00050041,0x00000011,0x00000012,
	0x0000000b,0x0000000d,0x0003003e,0x00000012,0x00000010,0x0004003d,0x00000008,0x00000016,
	0x00000015,0x00050041,0x00000017,0x00000018,0x0000000b,0x00000013,0x0003003e,0x00000018,
	0x00000016,0x0004003d,0x00000008,0x0000001d,0x0000001c,0x00050041,0x00000021,0x00000022,
	0x00000020,0x0000000d,0x0004003d,0x00000008,0x00000023,0x00000022,0x00050085,0x00000008,
	0x00000024,0x0000001d,0x00000023,0x00050041,0x00000021,0x00000025,0x00000020,0x00000013,
	0x0004003d,0x00000008,0x00000026,0x00000025,0x00050081,0x00000008,0x00000027,0x00000024,
	0x00000026,0x00050051,0x00000006,0x0000002a,0x00000027,0x00000000,0x00050051,0x00000006,
	0x0000002b,0x00000027,0x00000001,0x00070050,0x00000007,0x0000002c,0x0000002a,0x0000002b,
	0x00000028,0x00000029,0x00050041,0x00000011,0x0000002d,0x0000001b,0x0000000d,0x0003003e,
	0x0000002d,0x0000002c,0x000100fd,0x00010038
};
const uint32_t ImGuiHandler::glslFragShaderSpv[] =
{
	0x07230203,0x00010000,0x00080001,0x0000001e,0x00000000,0x00020011,0x00000001,0x0006000b,
	0x00000001,0x4c534c47,0x6474732e,0x3035342e,0x00000000,0x0003000e,0x00000000,0x00000001,
	0x0007000f,0x00000004,0x00000004,0x6e69616d,0x00000000,0x00000009,0x0000000d,0x00030010,
	0x00000004,0x00000007,0x00030003,0x00000002,0x000001c2,0x00040005,0x00000004,0x6e69616d,
	0x00000000,0x00040005,0x00000009,0x6c6f4366,0x0000726f,0x00030005,0x0000000b,0x00000000,
	0x00050006,0x0000000b,0x00000000,0x6f6c6f43,0x00000072,0x00040006,0x0000000b,0x00000001,
	0x00005655,0x00030005,0x0000000d,0x00006e49,0x00050005,0x00000016,0x78655473,0x65727574,
	0x00000000,0x00040047,0x00000009,0x0000001e,0x00000000,0x00040047,0x0000000d,0x0000001e,
	0x00000000,0x00040047,0x00000016,0x00000022,0x00000000,0x00040047,0x00000016,0x00000021,
	0x00000000,0x00020013,0x00000002,0x00030021,0x00000003,0x00000002,0x00030016,0x00000006,
	0x00000020,0x00040017,0x00000007,0x00000006,0x00000004,0x00040020,0x00000008,0x00000003,
	0x00000007,0x0004003b,0x00000008,0x00000009,0x00000003,0x00040017,0x0000000a,0x00000006,
	0x00000002,0x0004001e,0x0000000b,0x00000007,0x0000000a,0x00040020,0x0000000c,0x00000001,
	0x0000000b,0x0004003b,0x0000000c,0x0000000d,0x00000001,0x00040015,0x0000000e,0x00000020,
	0x00000001,0x0004002b,0x0000000e,0x0000000f,0x00000000,0x00040020,0x00000010,0x00000001,
	0x00000007,0x00090019,0x00000013,0x00000006,0x00000001,0x00000000,0x00000000,0x00000000,
	0x00000001,0x00000000,0x0003001b,0x00000014,0x00000013,0x00040020,0x00000015,0x00000000,
	0x00000014,0x0004003b,0x00000015,0x00000016,0x00000000,0x0004002b,0x0000000e,0x00000018,
	0x00000001,0x00040020,0x00000019,0x00000001,0x0000000a,0x00050036,0x00000002,0x00000004,
	0x00000000,0x00000003,0x000200f8,0x00000005,0x00050041,0x00000010,0x00000011,0x0000000d,
	0x0000000f,0x0004003d,0x00000007,0x00000012,0x00000011,0x0004003d,0x00000014,0x00000017,
	0x00000016,0x00050041,0x00000019,0x0000001a,0x0000000d,0x00000018,0x0004003d,0x0000000a,
	0x0000001b,0x0000001a,0x00050057,0x00000007,0x0000001c,0x00000017,0x0000001b,0x00050085,
	0x00000007,0x0000001d,0x00000012,0x0000001c,0x0003003e,0x00000009,0x0000001d,0x000100fd,
	0x00010038
};

ImGuiHandler::ImGuiHandler(VulkanHandler &vulkanHandler, const std::string fontPath, float fontSize, const std::pair<float, float> mousePosition)
	: vulkanHandler(vulkanHandler),
	  keyCode(-1),
	  mouseX(mousePosition.first),
	  mouseY(mousePosition.second),
	  mouseScrollOffset(0.0f),
	  imGuiWantsKeyboard(false),
	  imGuiWantsMouse(false),
	  imGuiWantsTextInput(false),
	  initialized(false),
	  renderFailed(false)
{
	for (int i = 0; i < IMGUI_MOUSE_BUTTONS_COUNT; ++i)
	{
		mouseButtonDown[i] = false;
	}
	ImGuiIO& io = ImGui::GetIO();
	// Mapowanie klawiszy GLFW
	io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;
	io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
	io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
	io.KeyMap[ImGuiKey_PageUp] = GLFW_KEY_PAGE_UP;
	io.KeyMap[ImGuiKey_PageDown] = GLFW_KEY_PAGE_DOWN;
	io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
	io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
	io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
	io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
	io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
	io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
	io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
	io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
	io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
	io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
	io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
	io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;
	// Build texture atlas
	io.IniFilename = nullptr;
	io.RenderDrawListsFn = imGuiRenderDrawLists;
	if (vulkanCreateDeviceObjects())
	{
		if (fontSize <= 0.0f) fontSize = DEFAULT_FONT_SIZE;
		io.Fonts->AddFontFromFileTTF(fontPath.c_str(), fontSize, nullptr, ImGui::GetGlyphRangesPolish());
		initialized = uploadFonts();
	}
	io.UserData = static_cast<void*>(this);
}

ImGuiHandler::~ImGuiHandler()
{
	vulkanInvalidateDeviceObjects();
	ImGui::Shutdown();
}

bool ImGuiHandler::isInitialized() const
{
	return initialized;
}

bool ImGuiHandler::doRenderFailed() const
{
	return renderFailed;
}

bool ImGuiHandler::doesImGuiWantsKeyboard() const
{
	return imGuiWantsKeyboard;
}

bool ImGuiHandler::doesImGuiWantsMouse() const
{
	return imGuiWantsMouse;
}

bool ImGuiHandler::doesImGuiWantsTextInput() const
{
	return imGuiWantsTextInput;
}

bool ImGuiHandler::setClipboardCallbacks(ImGuiClipboardGetFun clipboardGetCallback, ImGuiClipboardSetFun clipboardSetCallback, void *userData)
{
	if ((clipboardGetCallback != nullptr) && (clipboardSetCallback != nullptr))
	{
		ImGuiIO& io = ImGui::GetIO();
		io.GetClipboardTextFn = clipboardGetCallback;
		io.SetClipboardTextFn = clipboardSetCallback;
		io.ClipboardUserData = userData;
		return true;
	}
	else return false;
}

void ImGuiHandler::handleEvent(const Event &event)
{
	switch (event.getType())
	{
		case Event::KEYBOARD_EVENT:
			handleKeyboardEvent(static_cast<const KeyboardEvent&>(event));
			break;
		case Event::MOUSE_EVENT:
			handleMouseEvent(static_cast<const MouseEvent&>(event));
			break;
		default: break;
	}
}

void ImGuiHandler::newFrame(const float deltaTime, float width, float height)
{
	ImGuiIO& io = ImGui::GetIO();
	io.DisplaySize = ImVec2(width, height);
	io.DeltaTime = deltaTime;
	io.MousePos = ImVec2(mouseX, mouseY);
	for (int i = 0; i < IMGUI_MOUSE_BUTTONS_COUNT; ++i) {
		io.MouseDown[i] = mouseButtonDown[i];
	}
	io.MouseWheel = mouseScrollOffset;
	mouseScrollOffset = 0.0;
	io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
	io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
	io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
	io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];
	ImGui::NewFrame();
	imGuiWantsKeyboard = io.WantCaptureKeyboard;
	imGuiWantsMouse = io.WantCaptureMouse;
	imGuiWantsTextInput = io.WantTextInput;
}

void ImGuiHandler::render()
{
	ImGui::Render();
}

void ImGuiHandler::handleKeyboardEvent(const KeyboardEvent &event)
{
	ImGuiIO& io = ImGui::GetIO();
	switch (event.getKeyboardEventType())
	{
		case KeyboardEvent::REPEAT:
		case KeyboardEvent::KEY_DOWN:
			io.KeysDown[event.getKey()] = true;
			break;
		case KeyboardEvent::KEY_UP:
			io.KeysDown[event.getKey()] = false;
			break;
		case KeyboardEvent::TEXT_INPUT:
		{
			unsigned int character = event.getCharacter();
			if ((character > 0) && (character < std::numeric_limits<unsigned short>::max()))
			{
				ImGui::GetIO().AddInputCharacter(static_cast<unsigned short>(character));
			}
			break;
		}
	}
}

void ImGuiHandler::handleMouseEvent(const MouseEvent &event)
{
	switch (event.getMouseEventType())
	{
		case MouseEvent::BUTTON_DOWN:
		{
			int button = event.getButton();
			if ((button >= 0) && (button < IMGUI_MOUSE_BUTTONS_COUNT))
			{
				mouseButtonDown[button] = true;
			}
			break;
		}
		case MouseEvent::BUTTON_UP:
		{
			int button = event.getButton();
			if ((button >= 0) && (button < IMGUI_MOUSE_BUTTONS_COUNT))
			{
				mouseButtonDown[button] = false;
			}
			break;
		}
		case MouseEvent::MOVE:
			mouseX = event.getX();
			mouseY = event.getY();
			break;
		case MouseEvent::SCROLL:
			mouseScrollOffset += event.getScrollOffset();
			break;
		case MouseEvent::MOUSE_LEFT_WINDOW:
			if (!event.hasEnteredWindow())
			{
				mouseX = -FLT_MAX;
				mouseY = -FLT_MAX;
			}
			break;
	}
}

void ImGuiHandler::imGuiRenderDrawLists(ImDrawData *draw_data)
{
	VkResult err;
	ImGuiIO& io = ImGui::GetIO();
	ImGuiHandler& object = *(static_cast<ImGuiHandler*>(io.UserData));

	VulkanHandler& vkh = object.vulkanHandler;
	VkDevice& device = vkh.device;
	VkAllocationCallbacks*& allocator = vkh.allocator;
	VkBuffer* vertexBuffer = object.vertexBuffer;
	uint32_t& frameIndex = vkh.frameIndex;
	size_t* vertexBufferSize = object.vertexBufferSize;
	VkDeviceMemory* vertexBufferMemory = object.vertexBufferMemory;
	size_t& bufferMemoryAlignment = object.bufferMemoryAlignment;
	VkBuffer* indexBuffer = object.indexBuffer;
	size_t* indexBufferSize = object.indexBufferSize;
	VkDeviceMemory* indexBufferMemory = object.indexBufferMemory;
	VkCommandBuffer& commandBuffer = vkh.commandBuffer[frameIndex];
	VkPipeline& pipeline = object.pipeline;
	VkDescriptorSet& descriptorSet = object.descriptorSet;
	VkPipelineLayout& pipelineLayout = object.pipelineLayout;

	// Create the Vertex Buffer:
	size_t vertex_size = draw_data->TotalVtxCount * sizeof(ImDrawVert);
	if (!vertexBuffer[frameIndex] || vertexBufferSize[frameIndex] < vertex_size)
	{
		if (vertexBuffer[frameIndex])
		{
			vkDestroyBuffer(device, vertexBuffer[frameIndex], allocator);
		}
		if (vertexBufferMemory[frameIndex])
		{
			vkFreeMemory(device, vertexBufferMemory[frameIndex], allocator);
		}
		size_t vertex_buffer_size = ((vertex_size - 1) / bufferMemoryAlignment + 1) * bufferMemoryAlignment;
		VkBufferCreateInfo buffer_info = {};
		buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		buffer_info.size = vertex_buffer_size;
		buffer_info.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
		buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		err = vkCreateBuffer(device, &buffer_info, allocator, &vertexBuffer[frameIndex]);
		if (!VulkanHandler::checkVulkanResult(err))
		{
			object.renderFailed = true;
			return;
		}
		VkMemoryRequirements req;
		vkGetBufferMemoryRequirements(device, vertexBuffer[frameIndex], &req);
		bufferMemoryAlignment = (bufferMemoryAlignment > req.alignment) ? bufferMemoryAlignment : req.alignment;
		VkMemoryAllocateInfo alloc_info = {};
		alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		alloc_info.allocationSize = req.size;
		alloc_info.memoryTypeIndex = vkh.getMemoryType(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, req.memoryTypeBits);
		err = vkAllocateMemory(device, &alloc_info, allocator, &vertexBufferMemory[frameIndex]);
		if (!VulkanHandler::checkVulkanResult(err))
		{
			object.renderFailed = true;
			return;
		}
		err = vkBindBufferMemory(device, vertexBuffer[frameIndex], vertexBufferMemory[frameIndex], 0);
		if (!VulkanHandler::checkVulkanResult(err))
		{
			object.renderFailed = true;
			return;
		}
		vertexBufferSize[frameIndex] = vertex_buffer_size;
	}

	// Create the Index Buffer:
	size_t index_size = draw_data->TotalIdxCount * sizeof(ImDrawIdx);
	if (!indexBuffer[frameIndex] || indexBufferSize[frameIndex] < index_size)
	{
		if (indexBuffer[frameIndex])
		{
			vkDestroyBuffer(device, indexBuffer[frameIndex], allocator);
		}
		if (indexBufferMemory[frameIndex])
		{
			vkFreeMemory(device, indexBufferMemory[frameIndex], allocator);
		}
		size_t index_buffer_size = ((index_size - 1) / bufferMemoryAlignment + 1) * bufferMemoryAlignment;
		VkBufferCreateInfo buffer_info = {};
		buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		buffer_info.size = index_buffer_size;
		buffer_info.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
		buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		err = vkCreateBuffer(device, &buffer_info, allocator, &indexBuffer[frameIndex]);
		if (!VulkanHandler::checkVulkanResult(err))
		{
			object.renderFailed = true;
			return;
		}
		VkMemoryRequirements req;
		vkGetBufferMemoryRequirements(device, indexBuffer[frameIndex], &req);
		bufferMemoryAlignment = (bufferMemoryAlignment > req.alignment) ? bufferMemoryAlignment : req.alignment;
		VkMemoryAllocateInfo alloc_info = {};
		alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		alloc_info.allocationSize = req.size;
		alloc_info.memoryTypeIndex = vkh.getMemoryType(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, req.memoryTypeBits);
		err = vkAllocateMemory(device, &alloc_info, allocator, &indexBufferMemory[frameIndex]);
		if (!VulkanHandler::checkVulkanResult(err))
		{
			object.renderFailed = true;
			return;
		}
		err = vkBindBufferMemory(device, indexBuffer[frameIndex], indexBufferMemory[frameIndex], 0);
		if (!VulkanHandler::checkVulkanResult(err))
		{
			object.renderFailed = true;
			return;
		}
		indexBufferSize[frameIndex] = index_buffer_size;
	}

	// Upload Vertex and index Data:
	{
		ImDrawVert* vtx_dst;
		ImDrawIdx* idx_dst;
		err = vkMapMemory(device, vertexBufferMemory[frameIndex], 0, vertex_size, 0, (void**)(&vtx_dst));
		if (!VulkanHandler::checkVulkanResult(err))
		{
			object.renderFailed = true;
			return;
		}
		err = vkMapMemory(device, indexBufferMemory[frameIndex], 0, index_size, 0, (void**)(&idx_dst));
		if (!VulkanHandler::checkVulkanResult(err))
		{
			object.renderFailed = true;
			return;
		}
		for (int n = 0; n < draw_data->CmdListsCount; n++)
		{
			const ImDrawList* cmd_list = draw_data->CmdLists[n];
			memcpy(vtx_dst, cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
			memcpy(idx_dst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
			vtx_dst += cmd_list->VtxBuffer.Size;
			idx_dst += cmd_list->IdxBuffer.Size;
		}
		VkMappedMemoryRange range[2] = {};
		range[0].sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
		range[0].memory = vertexBufferMemory[frameIndex];
		range[0].size = VK_WHOLE_SIZE;
		range[1].sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
		range[1].memory = indexBufferMemory[frameIndex];
		range[1].size = VK_WHOLE_SIZE;
		err = vkFlushMappedMemoryRanges(device, 2, range);
		if (!VulkanHandler::checkVulkanResult(err))
		{
			object.renderFailed = true;
			return;
		}
		vkUnmapMemory(device, vertexBufferMemory[frameIndex]);
		vkUnmapMemory(device, indexBufferMemory[frameIndex]);
	}

	// Bind pipeline and descriptor sets:
	{
		vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
		VkDescriptorSet desc_set[1] = { descriptorSet };
		vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, desc_set, 0, nullptr);
	}

	// Bind Vertex And Index Buffer:
	{
		VkBuffer vertex_buffers[1] = { vertexBuffer[frameIndex] };
		VkDeviceSize vertex_offset[1] = {0};
		vkCmdBindVertexBuffers(commandBuffer, 0, 1, vertex_buffers, vertex_offset);
		vkCmdBindIndexBuffer(commandBuffer, indexBuffer[frameIndex], 0, VK_INDEX_TYPE_UINT16);
	}

	// Setup viewport:
	{
		VkViewport viewport;
		viewport.x = 0;
		viewport.y = 0;
		viewport.width = ImGui::GetIO().DisplaySize.x;
		viewport.height = ImGui::GetIO().DisplaySize.y;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
	}

	// Setup scale and translation:
	{
		float scale[2];
		scale[0] = 2.0f / io.DisplaySize.x;
		scale[1] = 2.0f / io.DisplaySize.y;
		float translate[2];
		translate[0] = -1.0f;
		translate[1] = -1.0f;
		vkCmdPushConstants(commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, sizeof(float) * 0, sizeof(float) * 2, scale);
		vkCmdPushConstants(commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, sizeof(float) * 2, sizeof(float) * 2, translate);
	}

	// Render the command lists:
	int vtx_offset = 0;
	int idx_offset = 0;
	for (int n = 0; n < draw_data->CmdListsCount; n++)
	{
		const ImDrawList* cmd_list = draw_data->CmdLists[n];
		for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
		{
			const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
			if (pcmd->UserCallback)
			{
				pcmd->UserCallback(cmd_list, pcmd);
			}
			else
			{
				VkRect2D scissor;
				scissor.offset.x = (int32_t)(pcmd->ClipRect.x) > 0 ? (int32_t)(pcmd->ClipRect.x) : 0;
				scissor.offset.y = (int32_t)(pcmd->ClipRect.y) > 0 ? (int32_t)(pcmd->ClipRect.y) : 0;
				scissor.extent.width = (uint32_t)(pcmd->ClipRect.z - pcmd->ClipRect.x);
				scissor.extent.height = (uint32_t)(pcmd->ClipRect.w - pcmd->ClipRect.y + 1); // FIXME: Why +1 here?
				vkCmdSetScissor(commandBuffer, 0, 1, &scissor);
				vkCmdDrawIndexed(commandBuffer, pcmd->ElemCount, 1, idx_offset, vtx_offset, 0);
			}
			idx_offset += pcmd->ElemCount;
		}
		vtx_offset += cmd_list->VtxBuffer.Size;
	}
}

bool ImGuiHandler::vulkanCreateDeviceObjects()
{
	VkResult err;
	VkShaderModule vert_module;
	VkShaderModule frag_module;

	VkDevice& device = vulkanHandler.device;
	VkAllocationCallbacks*& allocator = vulkanHandler.allocator;
	VkDescriptorPool& descriptorPool = vulkanHandler.descriptorPool;
	VkRenderPass& renderPass = vulkanHandler.renderPass;
	VkPipelineCache& pipelineCache = vulkanHandler.pipelineCache;

	// Create The Shader Modules:
	{
		VkShaderModuleCreateInfo vert_info = {};
		vert_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		vert_info.codeSize = sizeof(glslVertShaderSpv);
		vert_info.pCode = (uint32_t*)glslVertShaderSpv;
		err = vkCreateShaderModule(device, &vert_info, allocator, &vert_module);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
		VkShaderModuleCreateInfo frag_info = {};
		frag_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		frag_info.codeSize = sizeof(glslFragShaderSpv);
		frag_info.pCode = (uint32_t*)glslFragShaderSpv;
		err = vkCreateShaderModule(device, &frag_info, allocator, &frag_module);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
	}

	if (!fontSampler)
	{
		VkSamplerCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		info.magFilter = VK_FILTER_LINEAR;
		info.minFilter = VK_FILTER_LINEAR;
		info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		info.minLod = -1000;
		info.maxLod = 1000;
		info.maxAnisotropy = 1.0f;
		err = vkCreateSampler(device, &info, allocator, &fontSampler);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
	}

	if (!descriptorSetLayout)
	{
		VkSampler sampler[1] = { fontSampler };
		VkDescriptorSetLayoutBinding binding[1] = {};
		binding[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		binding[0].descriptorCount = 1;
		binding[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		binding[0].pImmutableSamplers = sampler;
		VkDescriptorSetLayoutCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		info.bindingCount = 1;
		info.pBindings = binding;
		err = vkCreateDescriptorSetLayout(device, &info, allocator, &descriptorSetLayout);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
	}

	// Create Descriptor Set:
	{
		VkDescriptorSetAllocateInfo alloc_info = {};
		alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		alloc_info.descriptorPool = descriptorPool;
		alloc_info.descriptorSetCount = 1;
		alloc_info.pSetLayouts = &descriptorSetLayout;
		err = vkAllocateDescriptorSets(device, &alloc_info, &descriptorSet);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
	}

	if (!pipelineLayout)
	{
		VkPushConstantRange push_constants[1] = {};
		push_constants[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		push_constants[0].offset = sizeof(float) * 0;
		push_constants[0].size = sizeof(float) * 4;
		VkDescriptorSetLayout set_layout[1] = { descriptorSetLayout };
		VkPipelineLayoutCreateInfo layout_info = {};
		layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		layout_info.setLayoutCount = 1;
		layout_info.pSetLayouts = set_layout;
		layout_info.pushConstantRangeCount = 1;
		layout_info.pPushConstantRanges = push_constants;
		err = vkCreatePipelineLayout(device, &layout_info, allocator, &pipelineLayout);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
	}

	VkPipelineShaderStageCreateInfo stage[2] = {};
	stage[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	stage[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
	stage[0].module = vert_module;
	stage[0].pName = "main";
	stage[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	stage[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	stage[1].module = frag_module;
	stage[1].pName = "main";

	VkVertexInputBindingDescription binding_desc[1] = {};
	binding_desc[0].stride = sizeof(ImDrawVert);
	binding_desc[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	VkVertexInputAttributeDescription attribute_desc[3] = {};
	attribute_desc[0].location = 0;
	attribute_desc[0].binding = binding_desc[0].binding;
	attribute_desc[0].format = VK_FORMAT_R32G32_SFLOAT;
	attribute_desc[0].offset = (size_t)(&((ImDrawVert*)0)->pos);
	attribute_desc[1].location = 1;
	attribute_desc[1].binding = binding_desc[0].binding;
	attribute_desc[1].format = VK_FORMAT_R32G32_SFLOAT;
	attribute_desc[1].offset = (size_t)(&((ImDrawVert*)0)->uv);
	attribute_desc[2].location = 2;
	attribute_desc[2].binding = binding_desc[0].binding;
	attribute_desc[2].format = VK_FORMAT_R8G8B8A8_UNORM;
	attribute_desc[2].offset = (size_t)(&((ImDrawVert*)0)->col);

	VkPipelineVertexInputStateCreateInfo vertex_info = {};
	vertex_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertex_info.vertexBindingDescriptionCount = 1;
	vertex_info.pVertexBindingDescriptions = binding_desc;
	vertex_info.vertexAttributeDescriptionCount = 3;
	vertex_info.pVertexAttributeDescriptions = attribute_desc;

	VkPipelineInputAssemblyStateCreateInfo ia_info = {};
	ia_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	ia_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

	VkPipelineViewportStateCreateInfo viewport_info = {};
	viewport_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewport_info.viewportCount = 1;
	viewport_info.scissorCount = 1;

	VkPipelineRasterizationStateCreateInfo raster_info = {};
	raster_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	raster_info.polygonMode = VK_POLYGON_MODE_FILL;
	raster_info.cullMode = VK_CULL_MODE_NONE;
	raster_info.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	raster_info.lineWidth = 1.0f;

	VkPipelineMultisampleStateCreateInfo ms_info = {};
	ms_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	ms_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	VkPipelineColorBlendAttachmentState color_attachment[1] = {};
	color_attachment[0].blendEnable = VK_TRUE;
	color_attachment[0].srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	color_attachment[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	color_attachment[0].colorBlendOp = VK_BLEND_OP_ADD;
	color_attachment[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	color_attachment[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	color_attachment[0].alphaBlendOp = VK_BLEND_OP_ADD;
	color_attachment[0].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

	VkPipelineDepthStencilStateCreateInfo depth_info = {};
	depth_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;

	VkPipelineColorBlendStateCreateInfo blend_info = {};
	blend_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	blend_info.attachmentCount = 1;
	blend_info.pAttachments = color_attachment;

	VkDynamicState dynamic_states[2] = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
	VkPipelineDynamicStateCreateInfo dynamic_state = {};
	dynamic_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamic_state.dynamicStateCount = 2;
	dynamic_state.pDynamicStates = dynamic_states;

	VkGraphicsPipelineCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	info.flags = pipelineCreateFlags;
	info.stageCount = 2;
	info.pStages = stage;
	info.pVertexInputState = &vertex_info;
	info.pInputAssemblyState = &ia_info;
	info.pViewportState = &viewport_info;
	info.pRasterizationState = &raster_info;
	info.pMultisampleState = &ms_info;
	info.pDepthStencilState = &depth_info;
	info.pColorBlendState = &blend_info;
	info.pDynamicState = &dynamic_state;
	info.layout = pipelineLayout;
	info.renderPass = renderPass;
	err = vkCreateGraphicsPipelines(device, pipelineCache, 1, &info, allocator, &pipeline);
	if (!VulkanHandler::checkVulkanResult(err)) return false;

	vkDestroyShaderModule(device, vert_module, allocator);
	vkDestroyShaderModule(device, frag_module, allocator);

	return true;
}

void ImGuiHandler::vulkanInvalidateDeviceObjects()
{
	vulkanInvalidateFontUploadObjects();

	VkDevice& device = vulkanHandler.device;
	VkAllocationCallbacks*& allocator = vulkanHandler.allocator;

	for (int i = 0; i < VulkanHandler::VK_QUEUED_FRAMES; ++i)
	{
		if (vertexBuffer[i])
		{
			vkDestroyBuffer(device, vertexBuffer[i], allocator);
			vertexBuffer[i] = VK_NULL_HANDLE;
		}
		if (vertexBufferMemory[i])
		{
			vkFreeMemory(device, vertexBufferMemory[i], allocator);
			vertexBufferMemory[i] = VK_NULL_HANDLE;
		}
		if (indexBuffer[i])
		{
			vkDestroyBuffer(device, indexBuffer[i], allocator);
			indexBuffer[i] = VK_NULL_HANDLE;
		}
		if (indexBufferMemory[i])
		{
			vkFreeMemory(device, indexBufferMemory[i], allocator);
			indexBufferMemory[i] = VK_NULL_HANDLE;
		}
	}

	if (fontView)
	{
		vkDestroyImageView(device, fontView, allocator);
		fontView = VK_NULL_HANDLE;
	}
	if (fontImage)
	{
		vkDestroyImage(device, fontImage, allocator);
		fontImage = VK_NULL_HANDLE;
	}
	if (fontMemory)
	{
		vkFreeMemory(device, fontMemory, allocator);
		fontMemory = VK_NULL_HANDLE;
	}
	if (fontSampler)
	{
		vkDestroySampler(device, fontSampler, allocator);\
		fontSampler = VK_NULL_HANDLE;
	}
	if (descriptorSetLayout)
	{
		vkDestroyDescriptorSetLayout(device, descriptorSetLayout, allocator);
		descriptorSetLayout = VK_NULL_HANDLE;
	}
	if (pipelineLayout)
	{
		vkDestroyPipelineLayout(device, pipelineLayout, allocator);
		pipelineLayout = VK_NULL_HANDLE;
	}
	if (pipeline)
	{
		vkDestroyPipeline(device, pipeline, allocator);
		pipeline = VK_NULL_HANDLE;
	}
}

void ImGuiHandler::vulkanInvalidateFontUploadObjects()
{
	VkDevice& device = vulkanHandler.device;
	VkAllocationCallbacks*& allocator = vulkanHandler.allocator;

	if (uploadBuffer)
	{
		vkDestroyBuffer(device, uploadBuffer, allocator);
		uploadBuffer = VK_NULL_HANDLE;
	}
	if (uploadBufferMemory)
	{
		vkFreeMemory(device, uploadBufferMemory, allocator);
		uploadBufferMemory = VK_NULL_HANDLE;
	}
}

bool ImGuiHandler::vulkanCreateFontsTexture(VkCommandBuffer command_buffer)
{
	ImGuiIO& io = ImGui::GetIO();

	unsigned char* pixels;
	int width, height;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);
	size_t upload_size = width * height * 4 * sizeof(char);

	VkResult err;
	VkDevice& device = vulkanHandler.device;
	VkAllocationCallbacks*& allocator = vulkanHandler.allocator;

	// Create the Image:
	{
		VkImageCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		info.imageType = VK_IMAGE_TYPE_2D;
		info.format = VK_FORMAT_R8G8B8A8_UNORM;
		info.extent.width = width;
		info.extent.height = height;
		info.extent.depth = 1;
		info.mipLevels = 1;
		info.arrayLayers = 1;
		info.samples = VK_SAMPLE_COUNT_1_BIT;
		info.tiling = VK_IMAGE_TILING_OPTIMAL;
		info.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		err = vkCreateImage(device, &info, allocator, &fontImage);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
		VkMemoryRequirements req;
		vkGetImageMemoryRequirements(device, fontImage, &req);
		VkMemoryAllocateInfo alloc_info = {};
		alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		alloc_info.allocationSize = req.size;
		alloc_info.memoryTypeIndex = vulkanHandler.getMemoryType(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, req.memoryTypeBits);
		err = vkAllocateMemory(device, &alloc_info, allocator, &fontMemory);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
		err = vkBindImageMemory(device, fontImage, fontMemory, 0);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
	}

	// Create the Image View:
	{
		VkImageViewCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		info.image = fontImage;
		info.viewType = VK_IMAGE_VIEW_TYPE_2D;
		info.format = VK_FORMAT_R8G8B8A8_UNORM;
		info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		info.subresourceRange.levelCount = 1;
		info.subresourceRange.layerCount = 1;
		err = vkCreateImageView(device, &info, allocator, &fontView);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
	}

	// Update the Descriptor Set:
	{
		VkDescriptorImageInfo desc_image[1] = {};
		desc_image[0].sampler = fontSampler;
		desc_image[0].imageView = fontView;
		desc_image[0].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		VkWriteDescriptorSet write_desc[1] = {};
		write_desc[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		write_desc[0].dstSet = descriptorSet;
		write_desc[0].descriptorCount = 1;
		write_desc[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		write_desc[0].pImageInfo = desc_image;
		vkUpdateDescriptorSets(device, 1, write_desc, 0, nullptr);
	}

	// Create the Upload Buffer:
	{
		VkBufferCreateInfo buffer_info = {};
		buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		buffer_info.size = upload_size;
		buffer_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
		buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		err = vkCreateBuffer(device, &buffer_info, allocator, &uploadBuffer);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
		VkMemoryRequirements req;
		vkGetBufferMemoryRequirements(device, uploadBuffer, &req);
		bufferMemoryAlignment = (bufferMemoryAlignment > req.alignment) ? bufferMemoryAlignment : req.alignment;
		VkMemoryAllocateInfo alloc_info = {};
		alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		alloc_info.allocationSize = req.size;
		alloc_info.memoryTypeIndex = vulkanHandler.getMemoryType(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, req.memoryTypeBits);
		err = vkAllocateMemory(device, &alloc_info, allocator, &uploadBufferMemory);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
		err = vkBindBufferMemory(device, uploadBuffer, uploadBufferMemory, 0);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
	}

	// Upload to Buffer:
	{
		char* map = nullptr;
		err = vkMapMemory(device, uploadBufferMemory, 0, upload_size, 0, (void**)(&map));
		if (!VulkanHandler::checkVulkanResult(err)) return false;
		memcpy(map, pixels, upload_size);
		VkMappedMemoryRange range[1] = {};
		range[0].sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
		range[0].memory = uploadBufferMemory;
		range[0].size = upload_size;
		err = vkFlushMappedMemoryRanges(device, 1, range);
		if (!VulkanHandler::checkVulkanResult(err)) return false;
		vkUnmapMemory(device, uploadBufferMemory);
	}
	// Copy to Image:
	{
		VkImageMemoryBarrier copy_barrier[1] = {};
		copy_barrier[0].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		copy_barrier[0].dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		copy_barrier[0].oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		copy_barrier[0].newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		copy_barrier[0].srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		copy_barrier[0].dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		copy_barrier[0].image = fontImage;
		copy_barrier[0].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		copy_barrier[0].subresourceRange.levelCount = 1;
		copy_barrier[0].subresourceRange.layerCount = 1;
		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_HOST_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, copy_barrier);

		VkBufferImageCopy region = {};
		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.layerCount = 1;
		region.imageExtent.width = width;
		region.imageExtent.height = height;
		region.imageExtent.depth = 1;
		vkCmdCopyBufferToImage(command_buffer, uploadBuffer, fontImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

		VkImageMemoryBarrier use_barrier[1] = {};
		use_barrier[0].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		use_barrier[0].srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		use_barrier[0].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		use_barrier[0].oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		use_barrier[0].newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		use_barrier[0].srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		use_barrier[0].dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		use_barrier[0].image = fontImage;
		use_barrier[0].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		use_barrier[0].subresourceRange.levelCount = 1;
		use_barrier[0].subresourceRange.layerCount = 1;
		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, use_barrier);
	}

	// Store our identifier
	io.Fonts->TexID = (void *)(intptr_t)fontImage;

	return true;
}

bool ImGuiHandler::uploadFonts()
{
	VkResult err;

	err = vkResetCommandPool(vulkanHandler.device, vulkanHandler.commandPool[vulkanHandler.frameIndex], 0);
	if (!VulkanHandler::checkVulkanResult(err)) return false;
	VkCommandBufferBeginInfo begin_info = {};
	begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	begin_info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	err = vkBeginCommandBuffer(vulkanHandler.commandBuffer[vulkanHandler.frameIndex], &begin_info);
	if (!VulkanHandler::checkVulkanResult(err)) return false;

	vulkanCreateFontsTexture(vulkanHandler.commandBuffer[vulkanHandler.frameIndex]);

	VkSubmitInfo end_info = {};
	end_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	end_info.commandBufferCount = 1;
	end_info.pCommandBuffers = &vulkanHandler.commandBuffer[vulkanHandler.frameIndex];
	err = vkEndCommandBuffer(vulkanHandler.commandBuffer[vulkanHandler.frameIndex]);
	if (!VulkanHandler::checkVulkanResult(err)) return false;
	err = vkQueueSubmit(vulkanHandler.queue, 1, &end_info, VK_NULL_HANDLE);
	if (!VulkanHandler::checkVulkanResult(err)) return false;

	err = vkDeviceWaitIdle(vulkanHandler.device);
	if (!VulkanHandler::checkVulkanResult(err)) return false;
	vulkanInvalidateFontUploadObjects();
	return true;
}
