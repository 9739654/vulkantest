#include <vector>
#include "Monitor.h"

GLFWmonitor* Monitor::getHandle() const {
	return handle;
}

const VideoMode* Monitor::getVideoMode() const {
	return glfwGetVideoMode(handle);
}

bool Monitor::equals(Monitor* other) const {
	return handle == other->handle;
}

static std::vector<Monitor> monitors;

std::vector<Monitor>* Monitor::getMonitors() {
	int count;
	GLFWmonitor** raw = glfwGetMonitors(&count);

	auto** raw_p = reinterpret_cast<void**>(raw);

	auto* monitorArray = (Monitor*) raw_p;

	monitors.clear();
	for (int i = 0; i < count; i++) {
		monitors.push_back(monitorArray[i]);
	}

	return &monitors;
}
