#include "ModelLoader.h"

#include <unordered_map>
#include <utility>
#include <iostream>
#include <memory>

#define TINYOBJLOADER_IMPLEMENTATION

#include "../include/tiny_obj_loader.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Common.h"
#include <ImageLoader.h>

#ifndef NDEBUG

#include "Timer.h"

#endif

Model* TinyobjLoader::loadFromFile(const std::string& filename) {
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;

#ifndef NDEBUG
	Timer timer;
	std::cout << "Loading model: " << filename << std::endl;
	timer.start();
#endif

	bool result = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, filename.c_str());
	if (!result) {
		throw std::runtime_error(err);
	}

	auto model = new Model();
	model->meshes.resize(1);
	model->directory = filename.substr(0, filename.find_last_of('/'));

	std::unordered_map<Vertex, uint32_t> uniqueVertices;

#ifndef NDEBUG
	std::cout << "\tReading file done. [" << timer.measureInterval() << " s]" << std::endl;
#endif

	for (const auto& shape : shapes) {
		for (const auto& index : shape.mesh.indices) {
			Vertex vertex = {};
			vertex.pos = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};

			vertex.texCoord = {
				attrib.texcoords[2 * index.texcoord_index + 0],
				1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
			};

			if (uniqueVertices.count(vertex) == 0) {
				uniqueVertices[vertex] = static_cast<uint32_t>(model->meshes[0].vertices.size());
				model->meshes[0].vertices.push_back(vertex);
			}

			model->meshes[0].indices.push_back(uniqueVertices[vertex]);
		}
	}

#ifndef NDEBUG
	std::cout << "\tRemoving double vertices done. [" << timer.measureInterval() << " s]"
	          << std::endl;
	std::cout << "\tDone: ";

	std::cout << model->meshes.size() << (model->meshes.size() == 1 ? " mesh, " : " meshes, ");
	std::cout << model->vertexCount() << " vertices, ";
	std::cout << model->faceCount() << " faces." << std::endl;
#endif
	return model;
}

Model* AssimpLoader::loadFromFile(const std::string& filename) {
#ifndef NDEBUG
	Timer timer;
	std::cout << "Loading model: " << filename << std::endl;
	timer.start();
#endif

	const aiScene* scene = importer.ReadFile(filename, aiProcess_Triangulate | aiProcess_FlipUVs);

#ifndef NDEBUG
	std::cout << "\tReading file done. [" << timer.measureInterval() << " s]" << std::endl;
#endif

	model = new Model();

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		std::stringstream ss;
		ss << "ERROR::ASSIMP::" << importer.GetErrorString();
		ASSERT(false, ss.str());
	}
	model->directory = filename.substr(0, filename.find_last_of('/'));

	processNode(scene->mRootNode, scene);


#ifndef NDEBUG
	std::cout << "\tProcessing model done. [" << timer.measureInterval() << " s]"
	          << std::endl;
	std::cout << "\tDone: ";

	std::cout << model->meshes.size() << (model->meshes.size() == 1 ? " mesh, " : " meshes, ");
	std::cout << model->vertexCount() << " vertices, ";
	std::cout << model->faceCount() << " faces." << std::endl;
#endif

	return model;
}

void AssimpLoader::processNode(aiNode* node, const aiScene* scene) {
	// process all the node's meshes (if any)
	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		model->meshes.push_back(processMesh(mesh, scene));
	}

	// then do the same for each of its children
	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		processNode(node->mChildren[i], scene);
	}
}

Mesh AssimpLoader::processMesh(aiMesh* srcMesh, const aiScene* scene) {
	Mesh mesh;

	mesh.topology = MeshTopology::Triangles;

#ifndef NDEBUG
	if (srcMesh->mNumVertices > 0) {
		std::cout << "\tMesh: ";

		std::cout << (srcMesh->mNormals != nullptr ? "has" : "no") << " normals, ";
		std::cout << (srcMesh->mTextureCoords != nullptr ? "has" : "no") << " texcoords.";
		std::cout << std::endl;
	}
#endif

	for (unsigned int i = 0; i < srcMesh->mNumVertices; i++) {
		Vertex vertex;
		vertex.pos.x = srcMesh->mVertices[i].x;
		vertex.pos.y = srcMesh->mVertices[i].y;
		vertex.pos.z = srcMesh->mVertices[i].z;

		if (srcMesh->mNormals != nullptr) {
			vertex.normal.x = srcMesh->mNormals[i].x;
			vertex.normal.y = srcMesh->mNormals[i].y;
			vertex.normal.z = srcMesh->mNormals[i].z;
		} else {
			//TODO oblicz normalne
		}

		if (srcMesh->mTextureCoords[0]) {
			glm::vec2 vec;
			vertex.texCoord.x = srcMesh->mTextureCoords[0][i].x;
			vertex.texCoord.y = srcMesh->mTextureCoords[0][i].y;
		} else {
			vertex.texCoord = glm::vec2(0.0f, 0.0f);
		}

		mesh.vertices.push_back(vertex);
	}

	// process indices
	for (unsigned int i = 0; i < srcMesh->mNumFaces; i++) {
		aiFace face = srcMesh->mFaces[i];
		ASSERT(face.mNumIndices == 3, "Failed to load face with indices != 3.");
		for (unsigned int j = 0; j < 3; j++) {
			mesh.indices.push_back(face.mIndices[j]);
		}
	}

	// process material
	if (srcMesh->mMaterialIndex >= 0) {
		aiMaterial* material = scene->mMaterials[srcMesh->mMaterialIndex];

		std::vector<Texture> diffuseMaps;
		loadTextures(material, aiTextureType_DIFFUSE, "texture_diffuse", diffuseMaps);
		mesh.textures.insert(mesh.textures.end(), diffuseMaps.begin(), diffuseMaps.end());

		std::vector<Texture> specularMaps;
		loadTextures(material, aiTextureType_SPECULAR, "texture_specular", specularMaps);
		mesh.textures.insert(mesh.textures.end(), specularMaps.begin(), specularMaps.end());

#ifndef NDEBUG
		std::cout << "\t\t" << diffuseMaps.size() << " diffuse ";
		std::cout << (diffuseMaps.size() == 1 ? " map: " : " maps: ");
		for (size_t i = 0; i < diffuseMaps.size(); i++) {
			std::cout << diffuseMaps[i].filename << (i == diffuseMaps.size() - 1 ? ".\n" : ", ");
		}

		std::cout << "\t\t" << specularMaps.size() << " specular ";
		std::cout << (specularMaps.size() == 1 ? " map." : " maps.") << std::endl;
		for (size_t i = 0; i < specularMaps.size(); i++) {
			std::cout << specularMaps[i].filename << (i == specularMaps.size() - 1 ? ".\n" : ", ");
		}
#endif

	}

	return mesh;
}

void AssimpLoader::loadTextures(aiMaterial* mat, aiTextureType type, const std::string& typeName,
                                std::vector<Texture>& vec) {
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++) {
		aiString str;
		mat->GetTexture(type, i, &str);

		Texture texture;
		texture.type = typeName;
		texture.filename = std::string(str.C_Str());
		vec.push_back(texture);
	}
}