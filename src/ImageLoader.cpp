#include "ImageLoader.h"

#include <stdexcept>
#include <sstream>
#include <memory>
#include <Timer.h>
#include <iostream>
#include <iomanip>

#define STB_IMAGE_IMPLEMENTATION

#include "stb_image.h"


std::unique_ptr<Image> ImageLoader::loadFromFile(const std::string& filename) {
	auto image = std::make_unique<Image>();

	int width, height, channels;

#ifndef NDEBUG
	Timer timer;
	timer.start();
	std::cout << "Loading image: " << filename << std::endl;
#endif

	image->desiredChannels = STBI_rgb_alpha;
	image->data = stbi_load(filename.c_str(), &width, &height,
	                        &channels, image->desiredChannels);
	image->width = static_cast<unsigned int>(width);
	image->height = static_cast<unsigned int>(height);
	image->channels = static_cast<unsigned int>(channels);

#ifndef NDEBUG
	std::cout << "\tDone. [" << timer.measure() << " s]" << std::endl;
#endif

	return image;
}

Image::~Image() {
	stbi_image_free(data);
}

Image::Image() : data(nullptr) {

}

unsigned long Image::size() {
	return 1UL * desiredChannels * width * height;
}
