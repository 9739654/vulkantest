#include <iostream>
#include <algorithm>
#include <chrono>
#include <set>
#include <unordered_map>
#include <iomanip>
#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <cstring>
#include <array>
#include <set>
#include <cstdio>
#include <map>

#include "Common.h"

#include "GlfwInclude.h"

#ifdef ENABLE_IMGUI

#include <imgui.h>
#include "imgui_impl_glfw_vulkan.h"

#endif

#include "Timer.h"
#include "ImageLoader.h"
#include "ModelLoader.h"
#include "Vulkan/Vulkan.h"
#include "Vulkan/Instance.h"
#include "Vulkan/PhysicalDevice.h"
#include "Vulkan/Queue.h"
#include "Vulkan/CommandBuffer.h"

const VkSampleCountFlagBits SAMPLES = VK_SAMPLE_COUNT_1_BIT;

const std::string MODEL_PATH = "assets/chalet.obj";
const std::string TEXTURE_PATH = "assets/chalet.jpg";

const std::vector<const char*> validationLayers = {
		"VK_LAYER_LUNARG_standard_validation"
};

const std::vector<const char*> deviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

static double inputInterval = 1.0 / 30.0;
static double updateInterval = 1.0 / 60.0;
static double renderInterval = 1.0 / 60.0;


#ifndef NDEBUG
#define ENABLE_VALIDATION_LAYERS
#else
#undef ENABLE_VALIDATION_LAYERS
#endif


namespace vk {

std::string queueFlagToString(VkQueueFlags in) {
	switch (in) {
		// VkQueueFlagBits:
		case VK_QUEUE_GRAPHICS_BIT:
			return "VK_QUEUE_GRAPHICS_BIT";
		case VK_QUEUE_COMPUTE_BIT:
			return "VK_QUEUE_COMPUTE_BIT";
		case VK_QUEUE_TRANSFER_BIT:
			return "VK_QUEUE_TRANSFER_BIT";
		case VK_QUEUE_SPARSE_BINDING_BIT:
			return "VK_QUEUE_SPARSE_BINDING_BIT";
		case VK_QUEUE_FLAG_BITS_MAX_ENUM:
			return "VK_QUEUE_FLAG_BITS_MAX_ENUM";

		default:
			return "UNKNOWN";
	}
}

std::string deviceTypeToString(VkPhysicalDeviceType in) {
	switch (in) {
		// VkPhysicalDeviceType
		case VK_PHYSICAL_DEVICE_TYPE_OTHER:
			return "VK_PHYSICAL_DEVICE_TYPE_OTHER";
		case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
			return "VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU";
		case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
			return "VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU";
		case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
			return "VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU";
		case VK_PHYSICAL_DEVICE_TYPE_CPU:
			return "VK_PHYSICAL_DEVICE_TYPE_CPU";
		default:
			return "UNKNOWN";

	}

}

std::string presentModeToString(VkPresentModeKHR in) {
	switch (in) {
		case VK_PRESENT_MODE_IMMEDIATE_KHR:
			return "VK_PRESENT_MODE_IMMEDIATE_KHR";
		case VK_PRESENT_MODE_MAILBOX_KHR:
			return "VK_PRESENT_MODE_MAILBOX_KHR";
		case VK_PRESENT_MODE_FIFO_KHR:
			return "VK_PRESENT_MODE_FIFO_KHR";
		case VK_PRESENT_MODE_FIFO_RELAXED_KHR:
			return "VK_PRESENT_MODE_FIFO_RELAXED_KHR";
		case VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR:
			return "VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR";
		case VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR:
			return "VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR";
		default:
			return "UNKNOWN";
	}
}

} // END NAMESPACE vk



class Application {
public:
	Application() : windowListener(this) {

	}

	void run() {
		initWindow();
		initVulkan();
#ifdef ENABLE_IMGUI
		initImgui();
#endif

		mainLoop();

#ifdef ENABLE_IMGUI
		cleanupImgui();
#endif
		cleanupVulkan();
		cleanupWindow();
	}


private:

	class AppWindowListener : public WindowListener {

	private:
		Application* app;

	public:
		explicit AppWindowListener(Application* app) : app(app) {}

		void onSizeChanged(int width, int height) override {
			app->recreateSwapchain();
		}
	};

	friend class AppWindowListener;

	AppWindowListener windowListener;

	Window window;

	vk::Instance instance;
	VkDebugReportCallbackEXT callback;
	vk::Surface surface;

	vk::PhysicalDevice physicalDevice;
	vk::Device device;

	vk::Queue graphicsQueue;
	vk::Queue presentQueue;

	VkSwapchainKHR swapChain;
	std::vector<VkImage> swapChainImages;
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;
	std::vector<VkImageView> swapChainImageViews; // one view for each swapchain image
	std::vector<VkFramebuffer> swapChainFramebuffers; // one framebuffer for each swapchain image

	VkRenderPass renderPass;
	VkDescriptorSetLayout descriptorSetLayout;
	VkPipelineLayout pipelineLayout;
	VkPipeline graphicsPipeline;
	VkPipelineCache pipelineCache; // not used

	VkCommandPool commandPool;

	VkImage depthImage;
	VkDeviceMemory depthImageMemory;
	VkImageView depthImageView;

	Model* model;

	std::vector<VkImage> textureImage;
	std::vector<VkDeviceMemory> textureImageMemory;
	std::vector<VkImageView> textureImageView;
	std::vector<VkSampler> textureSampler;

	std::vector<VkBuffer> vertexBuffer;
	std::vector<VkDeviceMemory> vertexBufferMemory;
	std::vector<VkBuffer> indexBuffer;
	std::vector<VkDeviceMemory> indexBufferMemory;

	VkBuffer uniformBuffer;
	VkDeviceMemory uniformBufferMemory;

	VkDescriptorPool descriptorPool;
	VkDescriptorSet descriptorSet;

	std::vector<vk::CommandBuffer> commandBuffers; // one command buffer for each framebuffer

	/**
	 * Image has been acquired and is ready for rendering.
	 */
	VkSemaphore imageAvailableSemaphore;

	/**
	 * Rendering has finished and presentation can happen.
	 */
	VkSemaphore renderFinishedSemaphore;

	Timer timer;


#ifdef ENABLE_IMGUI
	VkCommandPool igCommandPool;
	std::vector<vk::CommandBuffer> igCommandBuffers;

	//TODO: poprawić funkcję DebugLog itd...
	static void check_vk_result(VkResult err) {
		if (err == 0) return;
		printf("VkResult %d\n", err);
		if (err < 0) {
			abort();
		}
	}

	void initImgui() {
		ImGui_ImplGlfwVulkan_Init_Data init_data = {
				allocator: nullptr,
				gpu: *physicalDevice,
				device: *device,
				render_pass: renderPass,
				pipeline_cache: pipelineCache,
				descriptor_pool: descriptorPool,
				check_vk_result: check_vk_result
		};
		ImGui_ImplGlfwVulkan_Init(*window, true, &init_data);

		ImGui::StyleColorsClassic();

		vk::QueueFamilyIndices indices = physicalDevice.getQueueFamilyIndices(&surface);

		VkResult err;

		// Create command pool
		{
			vk::QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice);

			VkCommandPoolCreateInfo poolInfo = {
					sType: VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
					pNext: nullptr,
					flags: 0,
					queueFamilyIndex: static_cast<uint32_t>(queueFamilyIndices.graphicsFamily)
			};

			device.createCommandPool(&poolInfo, &igCommandPool);
			IFDEBUG(std::cout << "IMGUI: command pool created." << std::endl);
		}

		// Create command buffers
		{
			igCommandBuffers.resize(swapChainFramebuffers.size());

			VkCommandBufferAllocateInfo allocInfo = {
					sType: VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
					pNext: nullptr,
					commandPool: igCommandPool,
					level: VK_COMMAND_BUFFER_LEVEL_PRIMARY,
					commandBufferCount: static_cast<uint32_t>(igCommandBuffers.size())
			};

			device.allocateCommandBuffers(&allocInfo, igCommandBuffers);
			IFDEBUG(std::cout << "IMGUI: command buffers allocated." << std::endl);
		}

		// Upload Fonts
		{
			err = device.resetCommandPool(igCommandPool);
			check_vk_result(err);
			VkCommandBufferBeginInfo beginInfo = {};
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

			vk::CommandBuffer igCommandBuffer = igCommandBuffers[0];
			err = igCommandBuffer.begin(&beginInfo);
			check_vk_result(err);

			ImGui_ImplGlfwVulkan_CreateFontsTexture(*igCommandBuffer);

			igCommandBuffer.end();
			check_vk_result(err);

			VkSubmitInfo end_info = {};
			end_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			end_info.commandBufferCount = 1;
			end_info.pCommandBuffers = &*igCommandBuffer;
			err = graphicsQueue.submit(1, &end_info, VK_NULL_HANDLE);
			check_vk_result(err);

			err = device.waitIdle();
			check_vk_result(err);
			ImGui_ImplGlfwVulkan_InvalidateFontUploadObjects();

			IFDEBUG(std::cout << "IMGUI: fonts uploaded." << std::endl);
		}

	}

	void cleanupImgui() {
		device.destroyCommandPool(igCommandPool);
		ImGui_ImplGlfwVulkan_Shutdown();
	}

#endif // end ENABLE_IMGUI

	void initWindow() {
		window.init();
		window.setListener(&windowListener);
	}

	void initVulkan() {
		createInstance();
		setupDebugCallback();
		createSurface();
		pickPhysicalDevice();
		createLogicalDevice();
		createSwapChain();
		IFDEBUG(std::cout << "Swapchain has " << swapChainImages.size() << " images." << std::endl);
		createImageViews();
		createRenderPass();
		createDescriptorSetLayout();
		createGraphicsPipeline();
		createCommandPool();
		createDepthResources();
		createFramebuffers();

		loadModel();
		createVertexBuffers();
		createIndexBuffers();

		createTextureImages();
		createTextureImageViews();
		createTextureSamplers();

		createUniformBuffer();
		createDescriptorPool();

		createDescriptorSet(0); // TODO

		createCommandBuffers();
		createSemaphores();
	}

	void mainLoop() {
		timer.start();

		double now = timer.now();

		static double nextInput = now;
		static double nextUpdate = now;
		static double nextRender = now;

		static Timer inputTimer, updateTimer, renderTimer;

		bool wasInput, wasUpdate, wasRender;

		while (!window.shouldClose()) {
			now = timer.now();

			if (nextInput <= now) {
				nextInput += inputInterval;
				inputTimer.frame();

				Window::pollEvents();
			}

			if (nextUpdate <= now) {
				nextUpdate += updateInterval;
				updateTimer.frame();

				updateUniformBuffer();
			}

			if (nextRender <= now) {
				nextRender += renderInterval;
				renderTimer.frame();

				drawFrame();
			}

			char ss[100];
			snprintf(ss, 100, "avInput=%.1f avUpdate=%.1f avRender=%.1f",
			         inputTimer.getFps(), updateTimer.getFps(), renderTimer.getFps());

			window.setTitle(ss);

			double nextAction = MIN3(nextInput, nextUpdate, nextRender);
			double sleep = nextAction - now;
			if (sleep > 0) {
				timer.sleep(sleep);
			}

			timer.frame();
		}

		device.waitIdle();
	}

	void cleanupSwapChain() {
		device.destroyImageView(depthImageView);
		device.destroyImage(depthImage);
		device.freeMemory(depthImageMemory);

		for (auto framebuffer : swapChainFramebuffers) {
			device.destroyFramebuffer(framebuffer);
		}

		device.freeCommandBuffers(commandPool, commandBuffers);

		device.destroyPipeline(graphicsPipeline);
		device.destroyPipelineLayout(pipelineLayout);
		device.destroyRenderPass(renderPass);

		for (auto imageView : swapChainImageViews) {
			device.destroyImageView(imageView);
		}

		device.destroySwapchain(swapChain);
	}

	void cleanupVulkan() {
		cleanupSwapChain();

		for (size_t i = 0; i < textureSampler.size(); i++) {
			device.destroySampler(textureSampler[i]);
			device.destroyImageView(textureImageView[i]);
			device.destroyImage(textureImage[i]);
			device.freeMemory(textureImageMemory[i]);
		}

		device.destroyDescriptorPool(descriptorPool);
		device.destroyDescriptorSetLayout(descriptorSetLayout);

		device.destroyBuffer(uniformBuffer);
		device.freeMemory(uniformBufferMemory);

		for (size_t i = 0; i < indexBuffer.size(); i++) {
			device.destroyBuffer(indexBuffer[i]);
			device.freeMemory(indexBufferMemory[i]);

			device.destroyBuffer(vertexBuffer[i]);
			device.freeMemory(vertexBufferMemory[i]);
		}

		device.destroySemaphore(renderFinishedSemaphore);
		device.destroySemaphore(imageAvailableSemaphore);

		device.destroyCommandPool(commandPool);

		device.destroy();

		instance.destroyDebugReportCallback(callback);
		instance.destroySurface(surface);
		instance.destroy();
	}

	void cleanupWindow() {
		window.destroy();
		glfwTerminate();
	}

	void createInstance() {
#ifdef ENABLE_VALIDATION_LAYERS
		ASSERT(checkValidationLayerSupport(), "Validation layers requested, but not available.");
#endif

		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = "Hello Triangle";
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = "No Engine";
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.apiVersion = VK_API_VERSION_1_0;

		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;

		auto extensions = getRequiredExtensions();
		createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
		createInfo.ppEnabledExtensionNames = extensions.data();

#ifdef ENABLE_VALIDATION_LAYERS
		createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		createInfo.ppEnabledLayerNames = validationLayers.data();
#else
		createInfo.enabledLayerCount = 0;
#endif
		instance = vk::Instance::create(&createInfo);
	}

	void setupDebugCallback() {
#ifdef ENABLE_VALIDATION_LAYERS

		VkDebugReportCallbackCreateInfoEXT createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
		createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
		createInfo.pfnCallback = debugCallback;

		instance.createDebugReportCallback(&createInfo, &callback);
#endif
	}

	void createSurface() {
		surface = instance.createSurface(window);
	}

	void pickPhysicalDevice() {
		std::vector<vk::PhysicalDevice>* devices = instance.enumeratePhysicalDevices();

		IFDEBUG(
				printPhysicalDevices(*devices);
				std::cout << std::endl;
		);

		std::map<vk::PhysicalDevice, double> deviceRatings;

		ratePhysicalDevices(deviceRatings, *devices);

		IFDEBUG(
				printDeviceRating(deviceRatings);
				std::cout << std::endl;
		);

		double currentRating = -1;

		for (const auto& device : *devices) {
			double rating = deviceRatings[device];
			if (rating > currentRating) {
				physicalDevice = device;
				currentRating = rating;
			}
		}

		if (currentRating < 0) {
			throw std::runtime_error("Failed to find a suitable GPU.");
		}

		IFDEBUG(std::cout << "Selected device: " << physicalDevice.getName() << std::endl);
	}

	void createLogicalDevice() {
		vk::QueueFamilyIndices indices = physicalDevice.getQueueFamilyIndices(&surface);

		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
		std::set<int> uniqueQueueFamilies = {indices.graphicsFamily, indices.presentFamily};

		float queuePriority = 1.0f;
		for (int queueFamily : uniqueQueueFamilies) {
			VkDeviceQueueCreateInfo queueCreateInfo = {};
			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfo.queueFamilyIndex = static_cast<uint32_t>(queueFamily);
			queueCreateInfo.queueCount = 1;
			queueCreateInfo.pQueuePriorities = &queuePriority;
			queueCreateInfos.push_back(queueCreateInfo);
		}

		VkPhysicalDeviceFeatures deviceFeatures = {}; // required device features
		deviceFeatures.samplerAnisotropy = VK_TRUE;

		VkDeviceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

		createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
		createInfo.pQueueCreateInfos = queueCreateInfos.data();

		createInfo.pEnabledFeatures = &deviceFeatures;

		createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
		createInfo.ppEnabledExtensionNames = deviceExtensions.data();

#ifdef ENABLE_VALIDATION_LAYERS
		createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		createInfo.ppEnabledLayerNames = validationLayers.data();
#else
		createInfo.enabledLayerCount = 0;
#endif

		device = physicalDevice.createDevice(&createInfo);

		graphicsQueue = device.getQueue(indices.graphicsFamily, 0);
		presentQueue = device.getQueue(indices.presentFamily, 0);
	}

	void createSwapChain() {
		SwapChainSupportDetails swapChainSupport;
		physicalDevice.querySwapchainSupport(surface, swapChainSupport);

		VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
		VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
		VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);

		IFDEBUG(std::cout << "Selected present mode: " << vk::presentModeToString(presentMode) << std::endl);

		uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
		if (swapChainSupport.capabilities.maxImageCount > 0 &&
		    imageCount > swapChainSupport.capabilities.maxImageCount) {
			imageCount = swapChainSupport.capabilities.maxImageCount;
		}

		VkSwapchainCreateInfoKHR createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.surface = *surface;

		createInfo.minImageCount = imageCount;
		createInfo.imageFormat = surfaceFormat.format;
		createInfo.imageColorSpace = surfaceFormat.colorSpace;
		createInfo.imageExtent = extent;
		createInfo.imageArrayLayers = 1; //  This is always 1 unless you are developing a stereoscopic 3D application.
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

		vk::QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
		uint32_t queueFamilyIndices[] = {static_cast<uint32_t>(indices.graphicsFamily),
		                                 static_cast<uint32_t>(indices.presentFamily)};

		if (indices.graphicsFamily != indices.presentFamily) {
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		} else {
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		}

		createInfo.preTransform = swapChainSupport.capabilities.currentTransform; // current == no transform
		createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		createInfo.presentMode = presentMode;
		createInfo.clipped = VK_TRUE;

		device.createSwapchain(&createInfo, &swapChain);

		device.getSwapchainImages(swapChain, swapChainImages);

		swapChainImageFormat = surfaceFormat.format;
		swapChainExtent = extent;
	}

	void createImageViews() {
		swapChainImageViews.resize(swapChainImages.size());

		for (uint32_t i = 0; i < swapChainImages.size(); i++) {
			swapChainImageViews[i] = createImageView(swapChainImages[i], swapChainImageFormat,
			                                         VK_IMAGE_ASPECT_COLOR_BIT);
		}
	}

	void createRenderPass() {
		VkAttachmentDescription colorAttachment = {
				flags: 0,
				format: swapChainImageFormat,
				samples: SAMPLES,
				loadOp: VK_ATTACHMENT_LOAD_OP_CLEAR,
				storeOp: VK_ATTACHMENT_STORE_OP_STORE,
				stencilLoadOp: VK_ATTACHMENT_LOAD_OP_DONT_CARE,
				stencilStoreOp: VK_ATTACHMENT_STORE_OP_DONT_CARE,
				initialLayout: VK_IMAGE_LAYOUT_UNDEFINED,
				finalLayout: VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
		};

		VkAttachmentDescription depthAttachment = {
				flags: 0,
				format: findDepthFormat(),
				samples: SAMPLES,
				loadOp: VK_ATTACHMENT_LOAD_OP_CLEAR,
				storeOp: VK_ATTACHMENT_STORE_OP_DONT_CARE,
				stencilLoadOp: VK_ATTACHMENT_LOAD_OP_DONT_CARE,
				stencilStoreOp: VK_ATTACHMENT_STORE_OP_DONT_CARE,
				initialLayout: VK_IMAGE_LAYOUT_UNDEFINED,
				finalLayout: VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
		};

		VkAttachmentReference colorAttachmentRef = {
				attachment: 0,
				layout: VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		};

		VkAttachmentReference depthAttachmentRef = {
				attachment: 1,
				layout: VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
		};

		VkSubpassDescription subpass = {
				flags: 0,
				pipelineBindPoint: VK_PIPELINE_BIND_POINT_GRAPHICS,
				inputAttachmentCount: 0,
				pInputAttachments: nullptr,
				colorAttachmentCount: 1,
				pColorAttachments: &colorAttachmentRef,
				pResolveAttachments: nullptr,
				pDepthStencilAttachment: &depthAttachmentRef,
				preserveAttachmentCount: 0,
				pPreserveAttachments: nullptr
		};

		VkSubpassDependency dependency = {
				srcSubpass: VK_SUBPASS_EXTERNAL,
				dstSubpass: 0,
				srcStageMask: VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
				dstStageMask: VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
				srcAccessMask: 0,
				dstAccessMask: VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
				dependencyFlags: 0
		};

		std::array<VkAttachmentDescription, 2> attachments = {colorAttachment, depthAttachment};
		VkRenderPassCreateInfo renderPassInfo = {
				sType: VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
				pNext: nullptr,
				flags: 0,
				attachmentCount: static_cast<uint32_t>(attachments.size()),
				pAttachments: attachments.data(),
				subpassCount: 1,
				pSubpasses: &subpass,
				dependencyCount: 1,
				pDependencies: &dependency
		};

		device.createRenderPass(&renderPassInfo, &renderPass);
	}

	void createDescriptorSetLayout() {
		VkDescriptorSetLayoutBinding uboLayoutBinding = {};
		uboLayoutBinding.binding = 0;
		uboLayoutBinding.descriptorCount = 1;
		uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		uboLayoutBinding.pImmutableSamplers = nullptr;
		uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

		VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
		samplerLayoutBinding.binding = 1;
		samplerLayoutBinding.descriptorCount = 1;
		samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		samplerLayoutBinding.pImmutableSamplers = nullptr;
		samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		std::array<VkDescriptorSetLayoutBinding, 2> bindings = {uboLayoutBinding,
		                                                        samplerLayoutBinding};
		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
		layoutInfo.pBindings = bindings.data();

		device.createDescriptorSetLayout(&layoutInfo, &descriptorSetLayout);
	}

	void createGraphicsPipeline() {
		auto vertShaderCode = readFile("shaders/vert.spv");
		auto fragShaderCode = readFile("shaders/frag.spv");

		VkShaderModule vertShaderModule = createShaderModule(vertShaderCode);
		VkShaderModule fragShaderModule = createShaderModule(fragShaderCode);

		VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
		vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
		vertShaderStageInfo.module = vertShaderModule;
		vertShaderStageInfo.pName = "main";

		VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
		fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		fragShaderStageInfo.module = fragShaderModule;
		fragShaderStageInfo.pName = "main";

		VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

		VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

		auto bindingDescription = Vertex::getBindingDescription();
		auto attributeDescriptions = Vertex::getAttributeDescriptions();

		vertexInputInfo.vertexBindingDescriptionCount = 1;
		vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
		vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
		vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

		VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
		inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		inputAssembly.primitiveRestartEnable = VK_FALSE;

		VkViewport viewport = {};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = swapChainExtent.width;
		viewport.height = swapChainExtent.height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		VkRect2D scissor = {
				.offset = {0, 0},
				.extent = swapChainExtent
		};

		VkPipelineViewportStateCreateInfo viewportState = {};
		viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportState.viewportCount = 1;
		viewportState.pViewports = &viewport;
		viewportState.scissorCount = 1;
		viewportState.pScissors = &scissor;

		VkPipelineRasterizationStateCreateInfo rasterizer = {};
		rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizer.depthClampEnable = VK_FALSE;
		rasterizer.rasterizerDiscardEnable = VK_FALSE;
		rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
		rasterizer.lineWidth = 1.0f;
		rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
		rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		rasterizer.depthBiasEnable = VK_FALSE;

		VkPipelineMultisampleStateCreateInfo multisampling = {};
		multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampling.sampleShadingEnable = VK_FALSE;
		multisampling.rasterizationSamples = SAMPLES;

		VkPipelineDepthStencilStateCreateInfo depthStencil = {};
		depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthStencil.depthTestEnable = VK_TRUE;
		depthStencil.depthWriteEnable = VK_TRUE;
		depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
		depthStencil.depthBoundsTestEnable = VK_FALSE;
		depthStencil.stencilTestEnable = VK_FALSE;

		VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
		colorBlendAttachment.colorWriteMask =
				VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
				VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachment.blendEnable = VK_FALSE;

		VkPipelineColorBlendStateCreateInfo colorBlending = {};
		colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlending.logicOpEnable = VK_FALSE;
		colorBlending.logicOp = VK_LOGIC_OP_COPY;
		colorBlending.attachmentCount = 1;
		colorBlending.pAttachments = &colorBlendAttachment;
		colorBlending.blendConstants[0] = 0.0f;
		colorBlending.blendConstants[1] = 0.0f;
		colorBlending.blendConstants[2] = 0.0f;
		colorBlending.blendConstants[3] = 0.0f;

		VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = 1;
		pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;

		device.createPipelineLayout(&pipelineLayoutInfo, &pipelineLayout);

		VkGraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineInfo.stageCount = 2;
		pipelineInfo.pStages = shaderStages;
		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &inputAssembly;
		pipelineInfo.pViewportState = &viewportState;
		pipelineInfo.pRasterizationState = &rasterizer;
		pipelineInfo.pMultisampleState = &multisampling;
		pipelineInfo.pDepthStencilState = &depthStencil;
		pipelineInfo.pColorBlendState = &colorBlending;
		pipelineInfo.layout = pipelineLayout;
		pipelineInfo.renderPass = renderPass;
		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

		device.createGraphicsPipelines(VK_NULL_HANDLE, 1, &pipelineInfo, &graphicsPipeline);

		device.destroyShaderModule(fragShaderModule);
		device.destroyShaderModule(vertShaderModule);
	}

	void createFramebuffers() {
		swapChainFramebuffers.resize(swapChainImageViews.size());

		for (size_t i = 0; i < swapChainImageViews.size(); i++) {
			std::array<VkImageView, 2> attachments = {
					swapChainImageViews[i],
					depthImageView
			};

			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = renderPass;
			framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
			framebufferInfo.pAttachments = attachments.data();
			framebufferInfo.width = swapChainExtent.width;
			framebufferInfo.height = swapChainExtent.height;
			framebufferInfo.layers = 1;

			device.createFramebuffer(&framebufferInfo, &swapChainFramebuffers[i]);
		}
	}

	void createCommandPool() {
		vk::QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice);

		VkCommandPoolCreateInfo poolInfo = {
				sType: VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
				pNext: nullptr,
				flags: 0,
				queueFamilyIndex: static_cast<uint32_t>(queueFamilyIndices.graphicsFamily)
		};

		device.createCommandPool(&poolInfo, &commandPool);
	}

	void createDepthResources() {
		VkFormat depthFormat = findDepthFormat();

		createImage(swapChainExtent.width, swapChainExtent.height, depthFormat,
		            VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &depthImage, &depthImageMemory);
		depthImageView = createImageView(depthImage, depthFormat,
		                                 VK_IMAGE_ASPECT_DEPTH_BIT);

		transitionImageLayout(depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED,
		                      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
	}

	VkFormat
	findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
		for (VkFormat format : candidates) {
			VkFormatProperties props{};
			vkGetPhysicalDeviceFormatProperties(*physicalDevice, format, &props);

			if (tiling == VK_IMAGE_TILING_LINEAR &&
			    (props.linearTilingFeatures & features) == features) {
				return format;
			} else if (tiling == VK_IMAGE_TILING_OPTIMAL &&
			           (props.optimalTilingFeatures & features) == features) {
				return format;
			}
		}

		throw std::runtime_error("failed to find supported format!");
	}

	VkFormat findDepthFormat() {
		return findSupportedFormat(
				{VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT,
				 VK_FORMAT_D24_UNORM_S8_UINT},
				VK_IMAGE_TILING_OPTIMAL,
				VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
		);
	}

	bool hasStencilComponent(VkFormat format) {
		return format == VK_FORMAT_D32_SFLOAT_S8_UINT ||
		       format == VK_FORMAT_D24_UNORM_S8_UINT;
	}

	VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags) {
		VkImageViewCreateInfo viewInfo = {};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = image;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = format;

		viewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

		viewInfo.subresourceRange.aspectMask = aspectFlags;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = 1;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = 1;

		VkImageView imageView;
		device.createImageView(&viewInfo, &imageView);

		return imageView;
	}

	void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
	                 VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage* pImage,
	                 VkDeviceMemory* pImageMemory) {
		VkImageCreateInfo imageInfo = {};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.extent.width = width;
		imageInfo.extent.height = height;
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.format = format;
		imageInfo.tiling = tiling;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageInfo.usage = usage;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		device.createImage(&imageInfo, pImage);

		VkMemoryRequirements memRequirements;
		device.getImageMemoryRequirements(*pImage, &memRequirements);

		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits,
		                                           properties);

		device.allocateMemory(&allocInfo, pImageMemory);
		device.bindImageMemory(*pImage, *pImageMemory, 0);
	}

	void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout,
	                           VkImageLayout newLayout) {
		VkCommandBuffer commandBuffer = beginSingleTimeCommands();

		VkImageMemoryBarrier barrier = {};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = oldLayout;
		barrier.newLayout = newLayout;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.image = image;

		if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

			if (hasStencilComponent(format)) {
				barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
			}
		} else {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		}

		barrier.subresourceRange.baseMipLevel = 0;
		barrier.subresourceRange.levelCount = 1;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = 1;

		VkPipelineStageFlags sourceStage;
		VkPipelineStageFlags destinationStage;

		if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
		    newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
		           newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		} else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
		           newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
			                        VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		} else {
			throw std::invalid_argument("unsupported layout transition!");
		}

		vkCmdPipelineBarrier(
				commandBuffer,
				sourceStage, destinationStage,
				0,
				0, nullptr,
				0, nullptr,
				1, &barrier
		);

		endSingleTimeCommands(commandBuffer);
	}

	void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
		VkCommandBuffer commandBuffer = beginSingleTimeCommands();

		VkBufferImageCopy region = {};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;
		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = 1;
		region.imageOffset = {0, 0, 0};
		region.imageExtent = {
				width,
				height,
				1
		};

		vkCmdCopyBufferToImage(commandBuffer, buffer, image,
		                       VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

		endSingleTimeCommands(commandBuffer);
	}

	void loadModel() {
		AssimpLoader loader;
		model = loader.loadFromFile(MODEL_PATH);
	}

	void createTextureImages() {
		size_t count = model->meshes.size();
		textureImage.resize(count);
		textureImageMemory.resize(count);
		for (size_t i = 0; i < count; i++) {
			createTextureImage(i);
		}
	}

	void createTextureImage(size_t index) {
		int texWidth, texHeight, texChannels;

		ImageLoader imageLoader;
		auto image = imageLoader.loadFromFile(TEXTURE_PATH);

		VkDeviceSize imageSize = image->size();

		if (!image->data) {
			throw std::runtime_error("failed to load texture image!");
		}

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		             VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		             VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer,
		             stagingBufferMemory);

		copyMemory(image->data, stagingBufferMemory, imageSize);

		createImage(image->width, image->height, VK_FORMAT_R8G8B8A8_UNORM,
		            VK_IMAGE_TILING_OPTIMAL,
		            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
		            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &textureImage[index], &textureImageMemory[index]);

		transitionImageLayout(textureImage[index], VK_FORMAT_R8G8B8A8_UNORM,
		                      VK_IMAGE_LAYOUT_UNDEFINED,
		                      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		copyBufferToImage(stagingBuffer, textureImage[index], static_cast<uint32_t>(image->width),
		                  static_cast<uint32_t>(image->height));
		transitionImageLayout(textureImage[index], VK_FORMAT_R8G8B8A8_UNORM,
		                      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		device.destroyBuffer(stagingBuffer);
		device.freeMemory(stagingBufferMemory);
	}

	void createTextureImageViews() {
		size_t count = model->meshes.size();
		textureImageView.resize(count);
		for (size_t i = 0; i < count; i++) {
			createTextureImageView(i);
		}
	}

	void createTextureImageView(size_t index) {
		textureImageView[index] = createImageView(textureImage[index], VK_FORMAT_R8G8B8A8_UNORM,
		                                          VK_IMAGE_ASPECT_COLOR_BIT);
	}

	void createTextureSamplers() {
		size_t count = model->meshes.size();
		textureSampler.resize(count);
		for (size_t i = 0; i < count; i++) {
			createTextureSampler(i);
		}
	}

	void createTextureSampler(size_t index) {
		VkSamplerCreateInfo samplerInfo = {};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = VK_FILTER_LINEAR;
		samplerInfo.minFilter = VK_FILTER_LINEAR;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.anisotropyEnable = VK_TRUE;
		samplerInfo.maxAnisotropy = 16;
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;
		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;


		device.createSampler(&samplerInfo, &textureSampler[index]);
	}

	void createVertexBuffers() {
		size_t count = model->meshes.size();
		vertexBuffer.resize(count);
		vertexBufferMemory.resize(count);
		for (size_t i = 0; i < count; i++) {
			createVertexBuffer(i);
		}
	}

	void createVertexBuffer(size_t index) {
		createBuffer(bufferSize(model->meshes[index].vertices),
		             VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		             VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer[index], vertexBufferMemory[index]);
		bufferData(model->meshes[index].vertices, vertexBuffer[index]);
	}

	void createIndexBuffers() {
		size_t count = model->meshes.size();
		indexBuffer.resize(count);
		indexBufferMemory.resize(count);
		for (size_t i = 0; i < count; i++) {
			createIndexBuffer(i);
		}
	}

	void createIndexBuffer(size_t index) {
		createBuffer(bufferSize(model->meshes[index].indices),
		             VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		             VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer[index], indexBufferMemory[index]);
		bufferData(model->meshes[index].indices, indexBuffer[index]);
	}

	/**
	 * Copies size bytes from src memory into dst device memory
	 * @param src
	 * @param dst
	 * @param size
	 */
	void copyMemory(const void* src, VkDeviceMemory dst, size_t size) {
		void* data;
		device.mapMemory(dst, 0, size, 0, &data);
		memcpy(data, src, size);
		device.unmapMemory(dst);
	}

	/**
	 * Copies size bytes from src device memory into dst memory
	 * @param src
	 * @param dst
	 * @param size
	 */
	void copyMemory(VkDeviceMemory src, void* dst, size_t size) {
		void* data;
		device.mapMemory(src, 0, size, 0, &data);
		memcpy(dst, data, size);
		device.unmapMemory(src);
	}

	/**
	 * Copies size bytes from src into buffer
	 * @param src
	 * @param size
	 * @param buffer
	 */
	void bufferData(const void* src, unsigned long size, VkBuffer& buffer) {
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		             VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		             VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer,
		             stagingBufferMemory);

		copyMemory(src, stagingBufferMemory, size);
		copyBuffer(stagingBuffer, buffer, size);

		device.destroyBuffer(stagingBuffer);
		device.freeMemory(stagingBufferMemory);
	}

	/**
	 * Copies data from vec into buffer
	 * @tparam T
	 * @param vec
	 * @param buffer
	 * @param memory
	 */
	template<typename T>
	void bufferData(const std::vector<T>& vec, VkBuffer& buffer) {
		bufferData(vec.data(), bufferSize(vec), buffer);
	}

	void createUniformBuffer() {
		VkDeviceSize bufferSize = sizeof(UniformBufferObject);
		createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		             VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		             VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffer,
		             uniformBufferMemory);
	}

	void createDescriptorPool() {
		/*std::array<VkDescriptorPoolSize, 2> poolSizes = {};
		poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		poolSizes[0].descriptorCount = 1;
		poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolSizes[1].descriptorCount = 1;*/

		uint32_t descriptors = 2;
		std::vector<VkDescriptorPoolSize> poolSizes = {
				{VK_DESCRIPTOR_TYPE_SAMPLER,                descriptors},
				{VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, descriptors},
				{VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,          descriptors},
				{VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,          descriptors},
				{VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER,   descriptors},
				{VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER,   descriptors},
				{VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,         descriptors},
				{VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,         descriptors},
				{VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, descriptors},
				{VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, descriptors},
				{VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,       descriptors}
		};

		VkDescriptorPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
		poolInfo.pPoolSizes = poolSizes.data();
		poolInfo.maxSets = descriptors; // max sets nie może być mniejsze niż descriptors

		device.createDescriptorPool(&poolInfo, &descriptorPool);
	}

	void createDescriptorSet(size_t index) {
		VkDescriptorSetLayout layouts[] = {descriptorSetLayout};
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = descriptorPool;
		allocInfo.descriptorSetCount = 1;
		allocInfo.pSetLayouts = layouts;

		device.allocateDescriptorSets(&allocInfo, &descriptorSet);

		VkDescriptorBufferInfo bufferInfo = {};
		bufferInfo.buffer = uniformBuffer;
		bufferInfo.offset = 0;
		bufferInfo.range = sizeof(UniformBufferObject);

		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = textureImageView[index];
		imageInfo.sampler = textureSampler[index];

		std::vector<VkWriteDescriptorSet> descriptorWrites(2);

		descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[0].dstSet = descriptorSet;
		descriptorWrites[0].dstBinding = 0;
		descriptorWrites[0].dstArrayElement = 0;
		descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		descriptorWrites[0].descriptorCount = 1;
		descriptorWrites[0].pBufferInfo = &bufferInfo;

		descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[1].dstSet = descriptorSet;
		descriptorWrites[1].dstBinding = 1;
		descriptorWrites[1].dstArrayElement = 0;
		descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		descriptorWrites[1].descriptorCount = 1;
		descriptorWrites[1].pImageInfo = &imageInfo;

		std::vector<VkCopyDescriptorSet> descriptorCopies;
		device.updateDescriptorSets(descriptorWrites, descriptorCopies);
	}

	void
	createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
	             VkBuffer& buffer, VkDeviceMemory& bufferMemory) {
		VkBufferCreateInfo bufferInfo = {};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = size;
		bufferInfo.usage = usage;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		device.createBuffer(&bufferInfo, &buffer);

		VkMemoryRequirements memRequirements{};
		device.getBufferMemoryRequirements(buffer, &memRequirements);

		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits,
		                                           properties);

		device.allocateMemory(&allocInfo, &bufferMemory);
		device.bindBufferMemory(buffer, bufferMemory, 0);
	}

	VkCommandBuffer beginSingleTimeCommands() {
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = commandPool;
		allocInfo.commandBufferCount = 1;

		VkCommandBuffer commandBuffer;
		device.allocateCommandBuffers(&allocInfo, &commandBuffer);

		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		vkBeginCommandBuffer(commandBuffer, &beginInfo);

		return commandBuffer;
	}

	void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
		vkEndCommandBuffer(commandBuffer);

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffer;

		graphicsQueue.submit(1, &submitInfo, VK_NULL_HANDLE);
		graphicsQueue.waitIdle();

		device.freeCommandBuffers(commandPool, 1, &commandBuffer);
	}

	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
		VkCommandBuffer commandBuffer = beginSingleTimeCommands();

		VkBufferCopy copyRegion = {};
		copyRegion.size = size;
		vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

		endSingleTimeCommands(commandBuffer);
	}

	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
		VkPhysicalDeviceMemoryProperties memProperties{};
		vkGetPhysicalDeviceMemoryProperties(*physicalDevice, &memProperties);

		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
			if ((typeFilter & (1 << i)) &&
			    (memProperties.memoryTypes[i].propertyFlags & properties) ==
			    properties) {
				return i;
			}
		}

		throw std::runtime_error("failed to find suitable memory type!");
	}

	void createCommandBuffers() {
		commandBuffers.resize(swapChainFramebuffers.size());

		VkCommandBufferAllocateInfo allocInfo = {
				sType: VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
				pNext: nullptr,
				commandPool: commandPool,
				level: VK_COMMAND_BUFFER_LEVEL_PRIMARY,
				commandBufferCount: static_cast<uint32_t>(commandBuffers.size())
		};

		device.allocateCommandBuffers(&allocInfo, commandBuffers);

		VkCommandBufferBeginInfo beginInfo = {
				sType: VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
				pNext: nullptr,
				flags: VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
				pInheritanceInfo: nullptr
		};
		for (size_t i = 0; i < commandBuffers.size(); i++) {
			commandBuffers[i].begin(&beginInfo);

			std::array<VkClearValue, 2> clearValues = {};
			clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
			clearValues[1].depthStencil = {1.0f, 0};

			VkRenderPassBeginInfo renderPassInfo = {
					sType: VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
					pNext: nullptr,
					renderPass: renderPass,
					framebuffer: swapChainFramebuffers[i],
					renderArea: {
							offset: {0, 0},
							extent: swapChainExtent},
					clearValueCount: static_cast<uint32_t>(clearValues.size()),
					pClearValues: clearValues.data()
			};

			commandBuffers[i].beginRenderPass(&renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
			commandBuffers[i].bindPipeline(VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

			for (size_t j = 0; j < vertexBuffer.size(); j++) {
				VkBuffer vertexBuffers[] = {vertexBuffer[j]};
				VkDeviceSize offsets[] = {0};
				commandBuffers[i].bindVertexBuffers(0, 1, vertexBuffers, offsets);
				commandBuffers[i].bindIndexBuffer(indexBuffer[j], 0, VK_INDEX_TYPE_UINT32);
				commandBuffers[i].bindDescriptorSets(VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1,
				                                     &descriptorSet, 0, nullptr);

				commandBuffers[i].drawIndexed(static_cast<uint32_t>(model->meshes[j].indices.size()), 1, 0, 0, 0);
			}

			commandBuffers[i].endRenderPass();
			commandBuffers[i].end();
		}
	}

	void createSemaphores() {
		VkSemaphoreCreateInfo semaphoreInfo = {
				sType: VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
				pNext: nullptr,
				flags: 0
		};

		device.createSemaphore(&semaphoreInfo, &imageAvailableSemaphore);
		device.createSemaphore(&semaphoreInfo, &renderFinishedSemaphore);
	}

	void updateUniformBuffer() {
		static const auto pi = glm::pi<float>();
		static Timer updateTimer;

		UniformBufferObject ubo = {};
		float speed = 1; // radians / sec
		static double rotation = 0;
		rotation += updateTimer.delta();

		while (rotation > pi) {
			rotation -= 2 * pi;
		}
		while (rotation < pi) {
			rotation += 2 * pi;
		}

		ubo.model = glm::rotate(glm::mat4(1.0f), static_cast<float>(rotation * speed), glm::vec3(0.0f, 0.0f, 1.0f));
		ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		ubo.proj = glm::perspective(glm::radians(45.0f),
		                            swapChainExtent.width / (float) swapChainExtent.height,
		                            0.1f, 10.0f);
		ubo.proj[1][1] *= -1; // Dlaczego?

		device.downloadMemory(uniformBufferMemory, &ubo, sizeof(ubo), 0, 0x0);

		updateTimer.frame();
	}

	void drawFrame() {
		uint32_t imageIndex;

		presentQueue.waitIdle();

		//const uint64_t timeout = 1000 * 1000 * 1000 * 1; // nano seconds
		const uint64_t timeout = std::numeric_limits<uint64_t>::max();
		VkResult result = device.acquireNextImage(swapChain, timeout, imageAvailableSemaphore, VK_NULL_HANDLE,
		                                          &imageIndex);

		if (result == VK_ERROR_OUT_OF_DATE_KHR) {
			recreateSwapchain();
			return;
		} else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
			throw std::runtime_error("Failed to acquire swap chain image!");
		}

		submitDrawCommands(imageIndex);
	}

	void submitDrawCommands(uint32_t imageIndex) {
		// Each entry in the waitStages array corresponds to the semaphore with the same index in pWaitSemaphores.
		VkSemaphore waitSemaphores[] = {imageAvailableSemaphore};
		VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
		VkSemaphore renderFinishedSemaphores[] = {renderFinishedSemaphore};

		VkSubmitInfo submitInfo = {
				sType: VK_STRUCTURE_TYPE_SUBMIT_INFO,
				pNext: nullptr,
				waitSemaphoreCount: 1,
				pWaitSemaphores: waitSemaphores,
				pWaitDstStageMask: waitStages,
				commandBufferCount: 1,
				pCommandBuffers: &*commandBuffers[imageIndex],
				signalSemaphoreCount: 1,
				pSignalSemaphores: renderFinishedSemaphores
		};

		graphicsQueue.submit(1, &submitInfo, VK_NULL_HANDLE);

#ifdef ENABLE_IMGUI
		ImGui_ImplGlfwVulkan_NewFrame();
		buildImgui();

		beginImguiFrame(imageIndex);
		ImGui_ImplGlfwVulkan_Render(*igCommandBuffers[imageIndex]);
		endImguiFrame(imageIndex);
#endif

		VkSwapchainKHR swapChains[] = {swapChain};

		VkPresentInfoKHR presentInfo = {
				sType: VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
				pNext: nullptr,
				waitSemaphoreCount: 1,
				pWaitSemaphores: renderFinishedSemaphores,
				swapchainCount: 1,
				pSwapchains: swapChains,
				pImageIndices: &imageIndex,
				pResults: nullptr
		};
		VkResult result = presentQueue.present(&presentInfo);
		if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR) {
			recreateSwapchain();
		}
		VK_ASSERT(result, "Failed to present queue.");

	}

#ifdef ENABLE_IMGUI
	void beginImguiFrame(uint32_t imageIndex) {
		device.resetCommandPool(igCommandPool, 0);
		{
			VkCommandBufferBeginInfo info = {
					sType: VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
					pNext: nullptr,
					flags: VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
					pInheritanceInfo: nullptr
			};
			igCommandBuffers[imageIndex].begin(&info);
		}

		{
			std::array<VkClearValue, 2> clearValues = {};
			clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
			clearValues[1].depthStencil = {1.0f, 0};

			VkRenderPassBeginInfo info = {
					sType: VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
					pNext: nullptr,
					renderPass: renderPass,
					framebuffer: swapChainFramebuffers[imageIndex],
					renderArea: {
							offset: {0, 0},
							extent: swapChainExtent
					},
					clearValueCount: 2,
					pClearValues: clearValues.data()
			};
			igCommandBuffers[imageIndex].beginRenderPass(&info, VK_SUBPASS_CONTENTS_INLINE);
		}
	}

	void endImguiFrame(uint32_t imageIndex) {
		igCommandBuffers[imageIndex].endRenderPass();
		{
			VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			VkSubmitInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			info.waitSemaphoreCount = 0;
			//info.pWaitSemaphores = &g_PresentCompleteSemaphore[g_FrameIndex];
			info.pWaitDstStageMask = &wait_stage;
			info.commandBufferCount = 1;
			info.pCommandBuffers = &*igCommandBuffers[imageIndex];
			info.signalSemaphoreCount = 0;
			//info.pSignalSemaphores = &g_RenderCompleteSemaphore[g_FrameIndex];

			igCommandBuffers[imageIndex].end();

			//err = vkResetFences(g_Device, 1, &g_Fence[g_FrameIndex]);
			graphicsQueue.submit(1, &info, VK_NULL_HANDLE);
		}
	}

	void newImguiFrame() {
		static Timer imguiTimer;

		ImGuiIO& io = ImGui::GetIO();
		int width, height, fbWidth, fbHeight;
		window.getSize(&width, &height);
		io.DisplaySize = ImVec2(width, height);

		window.getFramebufferSize(&fbWidth, &fbHeight);
		io.DisplayFramebufferScale.x = width > 0 ? ((float) fbWidth / width) : 0;
		io.DisplayFramebufferScale.y = height > 0 ? ((float) fbHeight / height) : 0;

		io.DeltaTime = static_cast<float>(imguiTimer.delta());

		if (window.getAttribute(Window::WindowAttribute::Focused)) {
			double x, y;
			glfwGetCursorPos(*window, &x, &y);
			io.MousePos.x = static_cast<float>(x);
			io.MousePos.y = static_cast<float>(y);
		} else {
			io.MousePos = ImVec2(-FLT_MIN, -FLT_MIN);
		}

		for (int i = 0; i < 5; i++) {
			io.MouseDown[i] = glfwGetMouseButton(*window, i) != GLFW_RELEASE;
		}

		glfwSetInputMode(*window, GLFW_CURSOR, io.MouseDrawCursor ? GLFW_CURSOR_HIDDEN : GLFW_CURSOR_NORMAL);

		imguiTimer.frame();
	}

	void buildImgui() {
		static bool show_another_window = false;
		ImGui::Begin("Another Window", &show_another_window);
		ImGui::Text("Hello from another window!");
		ImGui::End();
	}

#endif

	VkShaderModule createShaderModule(const std::vector<char>& code) {
		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = code.size();
		createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

		VkShaderModule shaderModule;
		device.createShaderModule(&createInfo, &shaderModule);

		return shaderModule;
	}

	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
		if (availableFormats.size() == 1 &&
		    availableFormats[0].format == VK_FORMAT_UNDEFINED) {
			return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
		}

		for (const auto& availableFormat : availableFormats) {
			if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM &&
			    availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				return availableFormat;
			}
		}

		return availableFormats[0];
	}

	VkPresentModeKHR
	chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) {
		VkPresentModeKHR bestMode = VK_PRESENT_MODE_IMMEDIATE_KHR;

		IFDEBUG(
				std::cout << "Available present modes:" << std::endl;
				for (const auto& mode : availablePresentModes) {
					std::cout << '\t' << vk::presentModeToString(mode) << std::endl;
				}
				std::cout << std::endl;
		)

		for (const auto& availablePresentMode : availablePresentModes) {
			if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
				return availablePresentMode;

			} else if (availablePresentMode == VK_PRESENT_MODE_FIFO_KHR) {
				bestMode = availablePresentMode;
			}
		}

		return bestMode;
	}

	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
			return capabilities.currentExtent;
		} else {
			int width, height;
			window.getSize(&width, &height);

			VkExtent2D actualExtent = {
					static_cast<uint32_t>(width),
					static_cast<uint32_t>(height)
			};

			actualExtent.width = std::max(capabilities.minImageExtent.width,
			                              std::min(capabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(capabilities.minImageExtent.height,
			                               std::min(capabilities.maxImageExtent.height, actualExtent.height));

			return actualExtent;
		}
	}

	void printPhysicalDevices(const std::vector<vk::PhysicalDevice>& devices) const {
		std::cout << "Found physical devices:" << std::endl;
		for (const auto& device : devices) {

			VkPhysicalDeviceProperties props{};
			device.getProperties(&props);
			std::cout << '\t' << props.deviceName << ':' << std::endl;
			std::cout << "\t\tType: " << vk::deviceTypeToString(props.deviceType) << std::endl;
			std::cout << "\t\tAPI version: " << VK_VERSION_MAJOR(props.apiVersion)
			          << '.' << VK_VERSION_MINOR(props.apiVersion)
			          << '.' << VK_VERSION_PATCH(props.apiVersion) << std::endl;
			std::cout << "\t\tDriver version: " << VK_VERSION_MAJOR(props.driverVersion)
			          << '.' << ((props.driverVersion >> 14) & 0xff) << std::endl;

			printQueues(device, true, 2);
		}
	}

	void
	ratePhysicalDevices(std::map<vk::PhysicalDevice, double>& map,
	                    const std::vector<vk::PhysicalDevice>& devices) const {
		for (const auto& device : devices) {
			if (isDeviceSuitable(device)) {
				std::unique_ptr<VkPhysicalDeviceProperties> props = device.getProperties();
				switch (props->deviceType) {
					case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
						map[device] = getWithDef(map, device, 0.0) + 1000;
						break;
					case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
						map[device] = getWithDef(map, device, 0.0) + 100;
						break;
					case VK_PHYSICAL_DEVICE_TYPE_CPU:
					case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
						map[device] = getWithDef(map, device, 0.0) + 10;
						break;
					case VK_PHYSICAL_DEVICE_TYPE_OTHER:
					default:
						break;
				}
			} else {
				map[device] = -1;
			}
		}
	}

	void printDeviceRating(std::map<vk::PhysicalDevice, double>& map) {
		std::cout << "Device ratings:" << std::endl;
		for (const auto& entry : map) {
			std::cout << '\t' << entry.first.getName() << ": " << entry.second << std::endl;
		}
	}

	bool isDeviceSuitable(vk::PhysicalDevice device) const {
		vk::QueueFamilyIndices indices = findQueueFamilies(device);

		bool extensionsSupported = checkDeviceExtensionSupport(*device);

		bool swapChainAdequate = false;
		if (extensionsSupported) {
			SwapChainSupportDetails swapChainSupport;
			device.querySwapchainSupport(surface, swapChainSupport);
			swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
		}

		auto supportedFeatures = device.getFeatures();

		return indices.isComplete() && extensionsSupported && swapChainAdequate &&
		       supportedFeatures->samplerAnisotropy;
	}

	bool checkDeviceExtensionSupport(VkPhysicalDevice device) const {
		uint32_t extensionCount;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

		std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

		for (const auto& extension : availableExtensions) {
			requiredExtensions.erase(extension.extensionName);
		}

		return requiredExtensions.empty();
	}

	vk::QueueFamilyIndices findQueueFamilies(vk::PhysicalDevice device) const {
		vk::QueueFamilyIndices indices;
		auto* queueFamilies = device.getQueueFamilyProperties();

		uint32_t i = 0;
		for (const auto& queueFamily : *queueFamilies) {
			if (queueFamily.queueCount > 0 &&
			    queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				indices.graphicsFamily = i;
			}

			bool presentSupport = device.getSurfaceSupportKHR(i, &surface);

			if (queueFamily.queueCount > 0 && presentSupport) {
				indices.presentFamily = i;
			}

			if (indices.isComplete()) {
				break;
			}

			i++;
		}

		delete queueFamilies;
		return indices;
	}

	void printQueues(vk::PhysicalDevice device, bool title, size_t indent) const {
		auto* queueFamilies = device.getQueueFamilyProperties();
		uint32_t i = 0;

		if (title) {
			for (size_t ind = 0; ind < indent; ind++) {
				std::cout << "\t";
			}
			std::cout << "Queues:" << std::endl;

			indent += 1;
		}

		for (const auto& queueFamily : *queueFamilies) {
			for (size_t ind = 0; ind < indent; ind++) {
				std::cout << "\t";
			}
			std::cout << '#' << i << ": count=" << queueFamily.queueCount;

			if (queueFamily.queueCount > 0) {
				std::cout << " commands={ ";

				for (size_t j = 0; j < sizeof(queueFamily.queueFlags); j++) {
					VkQueueFlags flag = queueFamily.queueFlags & (1 << j);
					if (flag) {
						std::cout << vk::queueFlagToString(flag) << " ";
					}
				}

				std::cout << "}";
			}

			std::cout << std::endl;
			i++;
		}
	}

	std::vector<const char*> getRequiredExtensions() {
		std::vector<const char*> extensions;

		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		for (uint32_t i = 0; i < glfwExtensionCount; i++) {
			extensions.push_back(glfwExtensions[i]);
		}

#ifdef ENABLE_VALIDATION_LAYERS
		extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
#endif

		return extensions;
	}

	bool checkValidationLayerSupport() {
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		for (const char* layerName : validationLayers) {
			bool layerFound = false;

			for (const auto& layerProperties : availableLayers) {
				if (strcmp(layerName, layerProperties.layerName) == 0) {
					layerFound = true;
					break;
				}
			}

			if (!layerFound) {
				return false;
			}
		}

		return true;
	}

	static std::vector<char> readFile(const std::string& filename) {
		std::ifstream file(filename, std::ios::ate | std::ios::binary);

		if (!file.is_open()) {
			throw std::runtime_error("failed to open file!");
		}

		auto fileSize = (size_t) file.tellg();
		std::vector<char> buffer(fileSize);

		file.seekg(0);
		file.read(buffer.data(), fileSize);

		file.close();

		return buffer;
	}

	static VKAPI_ATTR VkBool32 VKAPI_CALL
	debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj,
	              size_t location, int32_t code, const char* layerPrefix, const char* msg,
	              void* userData) {
		std::cerr << "validation layer: " << msg << std::endl;

		return VK_FALSE;
	}

	void recreateSwapchain() {
		device.waitIdle();

		cleanupSwapChain();

		createSwapChain();
		createImageViews();
		createRenderPass();
		createGraphicsPipeline();
		createDepthResources();
		createFramebuffers();
		createCommandBuffers();
	}

	template<typename T>
	unsigned long bufferSize(const std::vector<T>& vec) {
		return vec.empty() ? 0 : sizeof(vec[0]) * vec.size();
	}

};

int main() {
	Application app;

	std::cout << std::setprecision(3);
	std::cout << std::fixed;

	try {
		app.run();
	} catch (const std::runtime_error& e) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
