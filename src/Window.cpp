#include "Window.h"

#include <string>
#include <stdexcept>

#include "Monitor.h"

static Window* getWindow(GLFWwindow* handle) {
	void* userPointer = glfwGetWindowUserPointer(handle);
	return reinterpret_cast<Window*>(userPointer);
}

static void glfwWindowSizeCallback(GLFWwindow* handle, int width, int height) {
	auto listener = getWindow(handle)->getListener();
	listener->onSizeChanged(width, height);
}

static void glfwWindowPositionCallback(GLFWwindow* handle, int x, int y) {
	auto listener = getWindow(handle)->getListener();
	listener->onPositionChanged(x, y);
}

static void glfwFramebufferSizeCallback(GLFWwindow* handle, int width, int height) {
	auto listener = getWindow(handle)->getListener();
	listener->onFramebufferSizeChanged(width, height);
}

static void glfwWindowClosedCallback(GLFWwindow* handle) {
	auto listener = getWindow(handle)->getListener();
	listener->onCloseRequest(); // TODO proper func name
}

static void glfwWindowFocusedCallback(GLFWwindow* handle, int focused) {
	auto listener = getWindow(handle)->getListener();
	listener->onFocused(focused == GLFW_TRUE);
}

static void glfwWindowIconifiedCallback(GLFWwindow* handle, int iconified) {
	auto listener = getWindow(handle)->getListener();
	listener->onIconified(iconified == GLFW_TRUE);
}

static void glfwWindowRefreshCallback(GLFWwindow* handle) {
	auto listener = getWindow(handle)->getListener();
	listener->onRefreshed();
}

Window::Window(int width, int height) : width(width), height(height) {

}

Window::~Window() {
	if (handle != nullptr) {
		destroy();
	}
}

/**
 * Create a Vulkan window
 */
void Window::init() {
	if (glfwInit() != GLFW_TRUE) {
		throw std::runtime_error("failed to initialize GLFW!");
	}

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	handle = glfwCreateWindow(width, height, "Default window", nullptr, nullptr);

	glfwSetWindowUserPointer(handle, this);
}

GLFWwindow* Window::getHandle() const {
	return handle;
}

GLFWwindow* Window::operator*() const {
	return getHandle();
}

void Window::destroy() {
	glfwDestroyWindow(handle);
	handle = nullptr;
}

bool Window::shouldClose() const {
	return glfwWindowShouldClose(handle) == GLFW_TRUE;
}

void Window::setShouldClose(bool active) {
	glfwSetWindowShouldClose(handle, active);
}

void Window::setTitle(const std::string& title) {
	setTitle(title.c_str());
}

void Window::setTitle(const char* title) {
	glfwSetWindowTitle(handle, title);
}

void Window::setIcon() {
	//TODO
}

void Window::getPosition(int* x, int* y) const {
	glfwGetWindowPos(handle, x, y);
}

void Window::setPosition(int x, int y) {
	glfwSetWindowPos(handle, x, y);
}

void Window::getSize(int* width, int* height) const {
	glfwGetWindowSize(handle, width, height);
}

void Window::setSizeLimits(int minWidth, int minHeight, int maxWidth, int maxHeight) {
	glfwSetWindowSizeLimits(handle, minWidth, minHeight, maxWidth, maxHeight);
}

void Window::setAspectRatio(int numer, int denom) {
	glfwSetWindowAspectRatio(handle, numer, denom);
}

void Window::setSize(int width, int height) {
	glfwSetWindowSize(handle, width, height);
}

void Window::getFramebufferSize(int* width, int* height) const {
	glfwGetFramebufferSize(handle, width, height);
}

void Window::getWindowFrameSize(int* left, int* top, int* right, int* bottom) const {
	glfwGetWindowFrameSize(handle, left, top, right, bottom);
}

void Window::iconify() {
	glfwIconifyWindow(handle);
}

void Window::restore() {
	glfwRestoreWindow(handle);
}

void Window::maximize() {
	glfwMaximizeWindow(handle);
}

void Window::show() {
	glfwShowWindow(handle);
}

void Window::hide() {
	glfwHideWindow(handle);
}

void Window::focus() {
	glfwFocusWindow(handle);
}

int Window::getAttribute(WindowAttribute attribute) const {
	//TODO return values??
	return glfwGetWindowAttrib(handle, attribute);
}

// callbacks

void Window::swapBuffers() {
	glfwSwapBuffers(handle);
}

void Window::makeContextCurrent() {
	glfwMakeContextCurrent(handle);
}

bool Window::isFullscreen() const {
	return glfwGetWindowMonitor(handle) == nullptr;
}

Monitor Window::getMonitor() const {
	GLFWmonitor* monitorHandle = glfwGetWindowMonitor(handle);

	auto monitorHandleArray = (void**) &monitorHandle;
	auto monitorPointer = (Monitor*) monitorHandleArray;

	return *monitorPointer;
}

void Window::setMonitor(Monitor* monitor) {
	//TODO
	GLFWmonitor* monitorHandle = monitor->getHandle();
	const VideoMode* videoMode = monitor->getVideoMode();
	glfwSetWindowMonitor(handle, monitorHandle, 0, 0, videoMode->width, videoMode->height,
	                     videoMode->refreshRate);
}

WindowListener* Window::getListener() {
	return listener;
}

void Window::setListener(WindowListener* listener) {
	this->listener = listener;
	if (listener == nullptr) {
		removeCallbacks();
	} else {
		setCallbacks();
	}
}

/// STATIC ///

void Window::pollEvents() {
	glfwPollEvents();
}

Window* Window::getCurrentContext() {
	//TODO

}

void Window::setCallbacks() {
	glfwSetWindowSizeCallback(handle, glfwWindowSizeCallback);
	glfwSetWindowPosCallback(handle, glfwWindowPositionCallback);
	glfwSetFramebufferSizeCallback(handle, glfwFramebufferSizeCallback);
	glfwSetWindowCloseCallback(handle, glfwWindowClosedCallback);
	glfwSetWindowFocusCallback(handle, glfwWindowFocusedCallback);
	glfwSetWindowIconifyCallback(handle, glfwWindowIconifiedCallback);
	glfwSetWindowRefreshCallback(handle, glfwWindowRefreshCallback);
}

void Window::removeCallbacks() {
	glfwSetWindowSizeCallback(handle, nullptr);
	glfwSetWindowPosCallback(handle, nullptr);
	glfwSetFramebufferSizeCallback(handle, nullptr);
	glfwSetWindowCloseCallback(handle, nullptr);
	glfwSetWindowFocusCallback(handle, nullptr);
	glfwSetWindowIconifyCallback(handle, nullptr);
	glfwSetWindowRefreshCallback(handle, nullptr);
}

