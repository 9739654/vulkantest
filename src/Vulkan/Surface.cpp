#include "Vulkan/Surface.h"

#include "Common.h"

namespace vk {

Surface::Surface() : handle(nullptr) {}

Surface::Surface(VkSurfaceKHR handle) : handle(handle) {}

VkSurfaceKHR Surface::getHandle() const {
	return handle;
}

VkSurfaceKHR Surface::operator*() const {
	return getHandle();
}

bool Surface::notValid() const {
	return handle == nullptr;
}

}