#include "Vulkan/CommandBuffer.h"

#include "Vulkan/VulkanCommon.h"

vk::CommandBuffer::CommandBuffer() : handle(nullptr) {}

vk::CommandBuffer::CommandBuffer(VkCommandBuffer handle) : handle(handle) {}

VkCommandBuffer const& vk::CommandBuffer::getHandle() const {
	return handle;
}

VkCommandBuffer const& vk::CommandBuffer::operator*() const {
	return getHandle();
}

bool vk::CommandBuffer::notValid() const {
	return handle == nullptr;
}

VkResult vk::CommandBuffer::begin(const VkCommandBufferBeginInfo* pBeginInfo) {
	return vkBeginCommandBuffer(handle, pBeginInfo);
}

VkResult vk::CommandBuffer::end() {
	VkResult result = vkEndCommandBuffer(handle);
	VK_ASSERT(result, "Failed to record command buffer.");
	return result;
}

VkResult vk::CommandBuffer::reset(VkCommandBufferResetFlags flags) {
	VkResult result = vkResetCommandBuffer(handle, flags);
	VK_ASSERT(result, "Failed to reset command buffer.");
	return result;
}

void vk::CommandBuffer::bindPipeline(VkPipelineBindPoint pipelineBindPoint, VkPipeline pipeline) {
	vkCmdBindPipeline(handle, pipelineBindPoint, pipeline);
}

void vk::CommandBuffer::setViewport(uint32_t firstViewport, uint32_t viewportCount,
                                    const VkViewport* pViewports) {
	vkCmdSetViewport(handle, firstViewport, viewportCount, pViewports);
}

void vk::CommandBuffer::setScissor(uint32_t firstScissor, uint32_t scissorCount,
                                   const VkRect2D* pScissors) {
	vkCmdSetScissor(handle, firstScissor, scissorCount, pScissors);
}

void vk::CommandBuffer::setLineWidth(float lineWidth) {
	vkCmdSetLineWidth(handle, lineWidth);
}

void vk::CommandBuffer::setDepthBias(float depthBiasConstantFactor, float depthBiasClamp,
                                     float depthBiasSlopeFactor) {
	vkCmdSetDepthBias(handle, depthBiasConstantFactor, depthBiasClamp,
	                  depthBiasSlopeFactor);
}

void vk::CommandBuffer::setBlendConstants(const float* blendConstants) {
	vkCmdSetBlendConstants(handle, blendConstants);
}

void vk::CommandBuffer::setDepthBounds(float minDepthBounds, float maxDepthBounds) {
	vkCmdSetDepthBounds(handle, minDepthBounds, maxDepthBounds);
}

void vk::CommandBuffer::setStencilComapreMask(VkStencilFaceFlags faceMask, uint32_t compareMask) {
	vkCmdSetStencilCompareMask(handle, faceMask, compareMask);
}

void vk::CommandBuffer::setStencilWriteMask(VkStencilFaceFlags faceMask, uint32_t writeMask) {
	vkCmdSetStencilWriteMask(handle, faceMask, writeMask);
}

void vk::CommandBuffer::SetStencilReference(VkStencilFaceFlags faceMask, uint32_t reference) {
	vkCmdSetStencilReference(handle, faceMask, reference);
}

void vk::CommandBuffer::bindDescriptorSets(VkPipelineBindPoint pipelineBindpoint,
                                           VkPipelineLayout layout, uint32_t firstSet,
                                           uint32_t descriptorSetCount,
                                           const VkDescriptorSet* pDescriptorSets,
                                           uint32_t dynamicOffsetCount,
                                           const uint32_t* pDynamicOffsets) {
	vkCmdBindDescriptorSets(handle, pipelineBindpoint, layout, firstSet,
	                        descriptorSetCount, pDescriptorSets, dynamicOffsetCount,
	                        pDynamicOffsets);
}

void vk::CommandBuffer::bindIndexBuffer(VkBuffer buffer, VkDeviceSize offset, VkIndexType indexType) {
	vkCmdBindIndexBuffer(handle, buffer, offset, indexType);
}

void vk::CommandBuffer::bindVertexBuffers(uint32_t firstBinding, uint32_t bindingCount,
                                          const VkBuffer* pBuffers, const VkDeviceSize* pOffsets) {
	vkCmdBindVertexBuffers(handle, firstBinding, bindingCount, pBuffers, pOffsets);
}

void vk::CommandBuffer::draw(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex,
                             uint32_t firstInstance) {
	vkCmdDraw(handle, vertexCount, instanceCount, firstVertex, firstInstance);
}

void
vk::CommandBuffer::drawIndexed(uint32_t indexCount, uint32_t instanceCount, uint32_t firstIndex,
                               int32_t vertexOffset, uint32_t firstInstance) {
	vkCmdDrawIndexed(handle, indexCount, instanceCount, firstIndex, vertexOffset,
	                 firstInstance);
}

void vk::CommandBuffer::drawIndirect(VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount,
                                     uint32_t stride) {
	vkCmdDrawIndirect(handle, buffer, offset, drawCount, stride);
}

void
vk::CommandBuffer::drawIndexedIndirect(VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount,
                                       uint32_t stride) {
	vkCmdDrawIndexedIndirect(handle, buffer, offset, drawCount, stride);
}

void vk::CommandBuffer::dispatch(uint32_t groupCountX, uint32_t groupCountY, uint32_t groupCountZ) {
	vkCmdDispatch(handle, groupCountX, groupCountY, groupCountZ);
}

void vk::CommandBuffer::dispatchIndirect(VkBuffer buffer, VkDeviceSize offset) {
	vkCmdDispatchIndirect(handle, buffer, offset);
}

void vk::CommandBuffer::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, uint32_t regionCount,
                                   const VkBufferCopy* pRegions) {
	vkCmdCopyBuffer(handle, srcBuffer, dstBuffer, regionCount, pRegions);
}

void vk::CommandBuffer::copyImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage,
                                  VkImageLayout dstImageLayout, uint32_t regionCount,
                                  const VkImageCopy* pRegions) {
	vkCmdCopyImage(handle, srcImage, srcImageLayout, dstImage, dstImageLayout,
	               regionCount, pRegions);
}

void vk::CommandBuffer::blitImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage,
                                  VkImageLayout dstImageLayout, uint32_t regionCount,
                                  const VkImageBlit* pRegions, VkFilter filter) {
	vkCmdBlitImage(handle, srcImage, srcImageLayout, dstImage, dstImageLayout,
	               regionCount, pRegions, filter);
}

void vk::CommandBuffer::copyBufferToImage(VkBuffer srcBuffer, VkImage dstImage,
                                          VkImageLayout dstImageLayout, uint32_t regionCount,
                                          const VkBufferImageCopy* pRegions) {
	vkCmdCopyBufferToImage(handle, srcBuffer, dstImage, dstImageLayout, regionCount,
	                       pRegions);
}

void vk::CommandBuffer::copyImageToBuffer(VkImage srcImage, VkImageLayout srcImageLayout,
                                          VkBuffer dstBuffer, uint32_t regionCount,
                                          const VkBufferImageCopy* pRegions) {
	vkCmdCopyImageToBuffer(handle, srcImage, srcImageLayout, dstBuffer, regionCount,
	                       pRegions);
}

void
vk::CommandBuffer::updateBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize dataSize,
                                const void* pData) {
	vkCmdUpdateBuffer(handle, dstBuffer, dstOffset, dataSize, pData);
}

void vk::CommandBuffer::fillBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize size,
                                   uint32_t data) {
	vkCmdFillBuffer(handle, dstBuffer, dstOffset, size, data);
}

void vk::CommandBuffer::clearColorImage(VkImage image, VkImageLayout imageLayout,
                                        const VkClearColorValue* pColor, uint32_t rangeCount,
                                        const VkImageSubresourceRange* pRanges) {
	vkCmdClearColorImage(handle, image, imageLayout, pColor, rangeCount, pRanges);
}

void vk::CommandBuffer::clearDepthStencilImage(VkImage image, VkImageLayout imageLayout,
                                               const VkClearDepthStencilValue* pDepthStencil,
                                               uint32_t rangeCount,
                                               const VkImageSubresourceRange* pRanges) {
	vkCmdClearDepthStencilImage(handle, image, imageLayout, pDepthStencil, rangeCount, pRanges);
}

void
vk::CommandBuffer::clearAttachments(uint32_t attachmentCount, const VkClearAttachment* pAttachments,
                                    uint32_t rectCount, const VkClearRect* pRects) {
	vkCmdClearAttachments(handle, attachmentCount, pAttachments, rectCount, pRects);
}

void
vk::CommandBuffer::resolveImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage,
                                VkImageLayout dstImageLayout, uint32_t regionCount,
                                const VkImageResolve* pRegions) {
	vkCmdResolveImage(handle, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
}

void vk::CommandBuffer::setEvent(VkEvent event, VkPipelineStageFlags stageMask) {
	vkCmdSetEvent(handle, event, stageMask);
}

void vk::CommandBuffer::resetEvent(VkEvent event, VkPipelineStageFlags stageMask) {
	vkCmdResetEvent(handle, event, stageMask);
}

void vk::CommandBuffer::waitEvents(uint32_t eventCount, const VkEvent* pEvents,
                                   VkPipelineStageFlags srcStageMask,
                                   VkPipelineStageFlags dstStageMask, uint32_t memoryBarrierCount,
                                   const VkMemoryBarrier* pMemoryBarriers,
                                   uint32_t bufferMemoryBarrierCount,
                                   const VkBufferMemoryBarrier* pBufferMemoryBarriers,
                                   uint32_t imageMemoryBarrierCount,
                                   const VkImageMemoryBarrier* pImageMemoryBarriers) {
	vkCmdWaitEvents(handle, eventCount, pEvents, srcStageMask, dstStageMask, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);
}

void vk::CommandBuffer::pipelineBarrier(VkPipelineStageFlags srcStageMask,
                                        VkPipelineStageFlags dstStageMask,
                                        VkDependencyFlags dependencyFlags,
                                        uint32_t memoryBarrierCount,
                                        const VkMemoryBarrier* pMemoryBarriers,
                                        uint32_t bufferMemoryBarrierCount,
                                        const VkBufferMemoryBarrier* pBufferMemoryBarriers,
                                        uint32_t imageMemoryBarrierCount,
                                        const VkImageMemoryBarrier* pImageMemoryBarriers) {
	vkCmdPipelineBarrier(handle, srcStageMask, dstStageMask, dependencyFlags, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);
}

void vk::CommandBuffer::beginQuery(VkQueryPool queryPool, uint32_t query, VkQueryControlFlags flags) {
	vkCmdBeginQuery(handle, queryPool, query, flags);
}

void vk::CommandBuffer::endQuery(VkQueryPool queryPool, uint32_t query) {
	vkCmdEndQuery(handle, queryPool, query);
}

void
vk::CommandBuffer::resetQueryPool(VkQueryPool queryPool, uint32_t firstQuery, uint32_t queryCount) {
	vkCmdResetQueryPool(handle, queryPool, firstQuery, queryCount);
}

void vk::CommandBuffer::writeTimestamp(VkPipelineStageFlagBits pipelineStage, VkQueryPool queryPool,
                                       uint32_t query) {
	vkCmdWriteTimestamp(handle, pipelineStage, queryPool, query);
}

void vk::CommandBuffer::copyQueryPoolResults(VkQueryPool queryPool, uint32_t firstQuery,
                                             uint32_t queryCount, VkBuffer dstBuffer,
                                             VkDeviceSize dstOffset, VkDeviceSize stride,
                                             VkQueryResultFlags flags) {
	vkCmdCopyQueryPoolResults(handle, queryPool, firstQuery, queryCount, dstBuffer, dstOffset, stride, flags);
}

void vk::CommandBuffer::pushConstants(VkPipelineLayout layout, VkShaderStageFlags stageFlags,
                                      uint32_t offset, uint32_t size, const void* pValues) {
	vkCmdPushConstants(handle, layout, stageFlags, offset, size, pValues);
}

void vk::CommandBuffer::beginRenderPass(const VkRenderPassBeginInfo* pRenderPassBegin,
                                        VkSubpassContents contents) {
	vkCmdBeginRenderPass(handle, pRenderPassBegin, contents);
}

void vk::CommandBuffer::nextSubpass(VkSubpassContents contents) {
	vkCmdNextSubpass(handle, contents);
}

void vk::CommandBuffer::endRenderPass() {
	vkCmdEndRenderPass(handle);
}

void vk::CommandBuffer::executeCommands(uint32_t commandBufferCount,
                                        const VkCommandBuffer* pCommandBuffers) {
	vkCmdExecuteCommands(handle, commandBufferCount, pCommandBuffers);
}
