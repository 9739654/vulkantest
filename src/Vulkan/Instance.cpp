#include "Vulkan/Instance.h"

#include "Common.h"
#include "Window.h"

#include "Vulkan/Surface.h"
#include "Vulkan/PhysicalDevice.h"

namespace vk {

Instance::Instance() : handle(nullptr) {}

Instance::Instance(VkInstance handle) : handle(handle) {}

VkInstance Instance::getHandle() const {
	return handle;
}

VkInstance Instance::operator*() const {
	return getHandle();
}

Surface Instance::createSurface(Window& window, ALLOCATOR) {
	VkSurfaceKHR surfaceHandle;
	VkResult result = glfwCreateWindowSurface(handle, *window, pAllocator,
	                                          &surfaceHandle);
	VK_ASSERT(result, "Failed to create window surface.");
	return Surface(surfaceHandle);
}

void Instance::destroy(ALLOCATOR) {
	vkDestroyInstance(handle, pAllocator);
	handle = nullptr;
}

std::vector<PhysicalDevice>* Instance::enumeratePhysicalDevices() {
	uint32_t count = 0;
	VkResult r = vkEnumeratePhysicalDevices(handle, &count, nullptr);
	ASSERT(count > 0, "Failed to find GPUs with Vulkan support.");
	VK_ASSERT(r, "Failed to enumerate physical devices.");

	std::vector<VkPhysicalDevice> handles(count);
	r = vkEnumeratePhysicalDevices(handle, &count, handles.data());
	VK_ASSERT(r, "Failed to enumerate physical devices.");

	auto vec = new std::vector<PhysicalDevice>();
	vec->reserve(count);
	for (int i = 0; i < count; i++) {
		vec->push_back(PhysicalDevice(handles[i]));
	}

	return vec;
}

PFN_vkVoidFunction Instance::getProcAddr(const std::string& name) {
	PFN_vkVoidFunction addr = nullptr;
	addr = vkGetInstanceProcAddr(handle, name.c_str());
	ASSERT(addr != nullptr, "Failed to get instance procedure address.");
	return addr;
}

Instance
Instance::create(VkInstanceCreateInfo* createInfo, ALLOCATOR) {
	VkInstance handle = nullptr;
	VkResult result = vkCreateInstance(createInfo, pAllocator, &handle);
	ASSERT(handle != nullptr, "Failed to create instance: handle == null.");
	VK_ASSERT(result, "Failed to create instance.");
	return Instance(handle);
}

void Instance::destroySurface(Surface& surface, ALLOCATOR) {
	vkDestroySurfaceKHR(handle, surface.getHandle(), pAllocator);
}

void Instance::createDebugReportCallback(const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
                                         ALLOCATOR, VkDebugReportCallbackEXT* pCallback) {
	auto func_v = getProcAddr("vkCreateDebugReportCallbackEXT");
	auto func = (PFN_vkCreateDebugReportCallbackEXT) func_v;
	ASSERT(func != nullptr, "Extension not present.");
	VkResult result = func(handle, pCreateInfo, pAllocator, pCallback);
	VK_ASSERT(result, "Failed to create debug report.");
}

void Instance::createDebugReportCallback(const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
                                         VkDebugReportCallbackEXT* pCallback) {
	createDebugReportCallback(pCreateInfo, nullptr, pCallback);
}

void Instance::destroyDebugReportCallback(VkDebugReportCallbackEXT callback, ALLOCATOR) {
	auto func = (PFN_vkDestroyDebugReportCallbackEXT) getProcAddr(
		"vkDestroyDebugReportCallbackEXT");
	func(handle, callback, pAllocator);
}

void Instance::enumerateExtensionProperties(const char* pLayerName, std::vector<VkExtensionProperties>& vec) {
	uint32_t count;
	VkResult result;
	result = vkEnumerateInstanceExtensionProperties(pLayerName, &count, nullptr);
	VK_ASSERT(result, "Failed to enumerate instance extension properties.");
	vec.resize(count);
	result = vkEnumerateInstanceExtensionProperties(pLayerName, &count, vec.data());
	{
		std::string message("Failed to enumerate instance extension properties. Found ");
		message += std::to_string(count);
		message += '.';
		VK_ASSERT(result, message);
	}
}

void Instance::enumerateExtensionProperties(const std::string& layerName, std::vector<VkExtensionProperties>& vec) {
	return enumerateExtensionProperties(layerName.c_str(), vec);
}

void Instance::enumerateExtensionProperties(std::vector<VkExtensionProperties>& vec) {
	return enumerateExtensionProperties((const char*) nullptr, vec);
}

void Instance::enumerateInstanceLayerProperties(std::vector<VkLayerProperties>& vec) {
	uint32_t count;
	vkEnumerateInstanceLayerProperties(&count, nullptr);
	vec.resize(count);
	vkEnumerateInstanceLayerProperties(&count, vec.data());
}

}
