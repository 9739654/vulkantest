#include "Vulkan/Queue.h"

vk::Queue::Queue() : handle(nullptr) {}

vk::Queue::Queue(VkQueue handle) : handle(handle) {}

VkQueue const& vk::Queue::getHandle() const {
	return handle;
}

VkQueue const& vk::Queue::operator*() const {
	return getHandle();
}

bool vk::Queue::notValid() const {
	return handle == nullptr;
}

VkResult vk::Queue::submit(uint32_t submitCount, const VkSubmitInfo* pSubmits, VkFence fence) {
	VkResult result = vkQueueSubmit(handle, submitCount, pSubmits, fence);
	VK_ASSERT(result, "Failed to submit command buffer to queue.");
	return result;
}

VkResult vk::Queue::waitIdle() {
	VkResult result = vkQueueWaitIdle(handle);
	VK_ASSERT(result, "Failed to wait for queue.");
	return result;
}

VkResult
vk::Queue::bindSparse(uint32_t bindInfoCount, const VkBindSparseInfo* pBindInfo, VkFence fence) {
	VkResult result = vkQueueBindSparse(handle, bindInfoCount, pBindInfo, fence);
	VK_ASSERT(result, "Failed to bind sparse.");
	return result;
}

/**
 *
 * @param pPresentInfo pPresentInfo must be a pointer to a valid VkPresentInfoKHR structure
 * @return
 * Success:
 * <ul>
 * <li>VK_SUCCESS</li>
 * <li>VK_SUBOPTIMAL_KHR</li>
 * </ul>
 * Failure:
 * <ul>
 * <li>VK_ERROR_OUT_OF_HOST_MEMORY</li>
 * <li>VK_ERROR_OUT_OF_DEVICE_MEMORY</li>
 * <li>VK_ERROR_DEVICE_LOST</li>
 * <li>VK_ERROR_OUT_OF_DATE_KHR</li>
 * <li>VK_ERROR_SURFACE_LOST_KHR</li>
 * </ul>
 */
VkResult vk::Queue::present(const VkPresentInfoKHR* pPresentInfo) {
	VkResult result = vkQueuePresentKHR(handle, pPresentInfo);
	//VK_ASSERT(result, "Failed to present queue.");
	return result;
}
