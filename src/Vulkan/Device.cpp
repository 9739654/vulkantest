#include "Vulkan/Device.h"

#include <cstring>

vk::Device::Device() : handle(nullptr) {

}

vk::Device::Device(VkDevice handle) : handle(handle) {

}

VkDevice vk::Device::getHandle() const {
	return handle;
}

VkDevice vk::Device::operator*() const {
	return getHandle();
}

bool vk::Device::notValid() const {
	return handle == nullptr;
}

void
vk::Device::downloadMemory(VkDeviceMemory memory, void* src, VkDeviceSize size, VkDeviceSize offset,
                           VkMemoryMapFlags flags) {
	void* data;
	mapMemory(memory, offset, size, flags, &data);
	memcpy(data, src, size);
	unmapMemory(memory);
}

PFN_vkVoidFunction vk::Device::getProcAddr(const std::string& name) {
	PFN_vkVoidFunction addr = vkGetDeviceProcAddr(handle, name.c_str());
	ASSERT(addr != nullptr, "Failed to get device procedure address.");
	return addr;
}

void vk::Device::destroy(const VkAllocationCallbacks* pAllocator) {
	vkDestroyDevice(handle, pAllocator);
	handle = nullptr;
}

void vk::Device::getQueue(int queueFamilyIndex, uint32_t queueIndex, VkQueue* pQueue) {
	getQueue(static_cast<uint32_t>(queueFamilyIndex), queueIndex, pQueue);
}

void vk::Device::getQueue(uint32_t queueFamilyIndex, uint32_t queueIndex, VkQueue* pQueue) {
	vkGetDeviceQueue(handle, queueFamilyIndex, queueIndex, pQueue);
}

vk::Queue vk::Device::getQueue(int queueFamilyIndex, uint32_t queueIndex) {
	VkQueue queue;
	getQueue(queueFamilyIndex, queueIndex, &queue);
	return vk::Queue(queue);
}

VkResult vk::Device::waitIdle() {
	VkResult result = vkDeviceWaitIdle(handle);
	VK_ASSERT(result, "Failed to wait for device idle.");
	return result;
}

VkResult vk::Device::allocateMemory(const VkMemoryAllocateInfo* pAllocateInfo,
                                    const VkAllocationCallbacks* pAllocator, VkDeviceMemory* pMemory) {
	VkResult result = vkAllocateMemory(handle, pAllocateInfo, pAllocator, pMemory);
	VK_ASSERT(result, "Failed to allocate memory.");
	return result;
}

void
vk::Device::allocateMemory(const VkMemoryAllocateInfo* pAllocateInfo, VkDeviceMemory* pMemory) {
	allocateMemory(pAllocateInfo, nullptr, pMemory);
}


void vk::Device::freeMemory(VkDeviceMemory memory, const VkAllocationCallbacks* pAllocator) {
	vkFreeMemory(handle, memory, pAllocator);
}

/**
 * Map a memory object into application address space
 * @param memory is the VkDeviceMemory object to be mapped.
 * @param offset is a zero-based byte offset from the beginning of the memory object.
 * @param size is the size of the memory range to map, or VK_WHOLE_SIZE to map from offset to the end of the allocation.
 * @param flags is reserved for future use.
 * @param ppData points to a pointer in which is returned a host-accessible pointer to the beginning of the mapped range. This pointer minus offset must be aligned to at least VkPhysicalDeviceLimits::minMemoryMapAlignment.
 */
VkResult vk::Device::mapMemory(VkDeviceMemory memory, VkDeviceSize offset, VkDeviceSize size,
                               VkMemoryMapFlags flags, void** ppData) {
	VkResult result = vkMapMemory(handle, memory, offset, size, flags, ppData);
	VK_ASSERT(result, "Failed to map memory.");
	return result;
}

void vk::Device::unmapMemory(VkDeviceMemory memory) {
	vkUnmapMemory(handle, memory);
}

VkResult vk::Device::flushMappedMemoryRanges(uint32_t memoryRangeCount,
                                             const VkMappedMemoryRange* pMemoryRanges) {
	VkResult result = vkFlushMappedMemoryRanges(handle, memoryRangeCount, pMemoryRanges);
	VK_ASSERT(result, "Failed to flush mapped memory ranges.");
	return result;
}

VkResult vk::Device::invalidateMappedMemoryRanges(uint32_t memoryRangeCount,
                                                  const VkMappedMemoryRange* pMemoryRanges) {
	VkResult result = vkInvalidateMappedMemoryRanges(handle, memoryRangeCount, pMemoryRanges);
	VK_ASSERT(result, "Failed to invalidate mapped memory ranges.");
	return result;
}

void vk::Device::getMemoryCommitment(VkDeviceMemory memory, VkDeviceSize* pCommitedMemoryInBytes) {
	vkGetDeviceMemoryCommitment(handle, memory, pCommitedMemoryInBytes);
}

VkResult
vk::Device::bindBufferMemory(VkBuffer buffer, VkDeviceMemory memory, VkDeviceSize memoryOffset) {
	VkResult result = vkBindBufferMemory(handle, buffer, memory, memoryOffset);
	VK_ASSERT(result, "Failed to bind buffer memory.");
	return result;
}

VkResult vk::Device::bindImageMemory(VkImage image, VkDeviceMemory memory, VkDeviceSize memoryOffset) {
	VkResult result = vkBindImageMemory(handle, image, memory, memoryOffset);
	VK_ASSERT(result, "Failed to bind image memory.");
	return result;
}

void vk::Device::getBufferMemoryRequirements(VkBuffer buffer,
                                             VkMemoryRequirements* pMemoryRequirements) {
	vkGetBufferMemoryRequirements(handle, buffer, pMemoryRequirements);
}

void
vk::Device::getImageMemoryRequirements(VkImage image, VkMemoryRequirements* pMemoryRequirements) {
	vkGetImageMemoryRequirements(handle, image, pMemoryRequirements);
}

void vk::Device::getImageSparseMemoryRequirements(VkImage image,
                                                  uint32_t* pSparseMemoryRequirementsCount,
                                                  VkSparseImageMemoryRequirements* pSparseMemoryRequirements) {
	vkGetImageSparseMemoryRequirements(handle, image, pSparseMemoryRequirementsCount,
	                                   pSparseMemoryRequirements);
}

VkResult vk::Device::createFence(const VkFenceCreateInfo* pCreateInfo,
                                 const VkAllocationCallbacks* pAllocator, VkFence* pFence) {
	VkResult result = vkCreateFence(handle, pCreateInfo, pAllocator, pFence);
	VK_ASSERT(result, "Failed to create fence.");
	return result;
}

void vk::Device::createFence(const VkFenceCreateInfo* pCreateInfo, VkFence* pFence) {
	createFence(pCreateInfo, nullptr, pFence);
}

void vk::Device::destroyFence(VkFence fence, const VkAllocationCallbacks* pAllocator) {
	vkDestroyFence(handle, fence, pAllocator);
}

VkResult vk::Device::resetFences(uint32_t fenceCount, const VkFence* pFences) {
	VkResult result = vkResetFences(handle, fenceCount, pFences);
	VK_ASSERT(result, "Failed to reset fences.");
	return result;
}

VkResult vk::Device::getFenceStatus(VkFence fence) {
	return vkGetFenceStatus(handle, fence);
}

VkResult vk::Device::waitForFences(uint32_t fenceCount, const VkFence* pFences, bool waitAll,
                                   uint64_t timeout) {
	VkResult result = vkWaitForFences(handle, fenceCount, pFences, static_cast<VkBool32>(waitAll),
	                                  timeout);
	VK_ASSERT(result, "Failed to wait for fences.")
	return result;
}

VkResult vk::Device::createSemaphore(const VkSemaphoreCreateInfo* pCreateInfo,
                                     const VkAllocationCallbacks* pAllocator, VkSemaphore* pSemaphore) {
	VkResult result = vkCreateSemaphore(handle, pCreateInfo, pAllocator, pSemaphore);
	VK_ASSERT(result, "Failed to create semaphore.")
	return result;
}

void
vk::Device::createSemaphore(const VkSemaphoreCreateInfo* pCreateInfo, VkSemaphore* pSemaphore) {
	createSemaphore(pCreateInfo, nullptr, pSemaphore);
}

void vk::Device::destroySemaphore(VkSemaphore semaphore, const VkAllocationCallbacks* pAllocator) {
	vkDestroySemaphore(handle, semaphore, pAllocator);
}

VkResult vk::Device::createEvent(const VkEventCreateInfo* pCreateInfo,
                                 const VkAllocationCallbacks* pAllocator, VkEvent* pEvent) {
	VkResult result = vkCreateEvent(handle, pCreateInfo, pAllocator, pEvent);
	VK_ASSERT(result, "Failed to create event.");
	return result;
}

void vk::Device::createEvent(const VkEventCreateInfo* pCreateInfo, VkEvent* pEvent) {
	createEvent(pCreateInfo, nullptr, pEvent);
}

void vk::Device::destroyEvent(VkEvent event, const VkAllocationCallbacks* pAllocator) {
	vkDestroyEvent(handle, event, pAllocator);
}

VkResult vk::Device::getEventStatus(VkEvent event) {
	return vkGetEventStatus(handle, event);
}

VkResult vk::Device::setEvent(VkEvent event) {
	VkResult result = vkSetEvent(handle, event);
	VK_ASSERT(result, "Failed to set event.")
	return result;
}

VkResult vk::Device::resetEvent(VkEvent event) {
	VkResult result = vkResetEvent(handle, event);
	VK_ASSERT(result, "Failed to reset event.");
	return result;
}

VkResult vk::Device::createQueryPool(const VkQueryPoolCreateInfo* pCreateInfo,
                                     const VkAllocationCallbacks* pAllocator, VkQueryPool* pQueryPool) {
	VkResult result = vkCreateQueryPool(handle, pCreateInfo, pAllocator, pQueryPool);
	VK_ASSERT(result, "Failed to create query pool.");
	return result;
}

void
vk::Device::createQueryPool(const VkQueryPoolCreateInfo* pCreateInfo, VkQueryPool* pQueryPool) {
	createQueryPool(pCreateInfo, nullptr, pQueryPool);
}

void vk::Device::destroyQueryPool(VkQueryPool queryPool, const VkAllocationCallbacks* pAllocator) {
	vkDestroyQueryPool(handle, queryPool, pAllocator);
}

VkResult
vk::Device::getQueryPoolResults(VkQueryPool queryPool, uint32_t firstQuery, uint32_t queryCount,
                                size_t dataSize, void* pData, VkDeviceSize stride,
                                VkQueryResultFlags flags) {
	VkResult result = vkGetQueryPoolResults(handle, queryPool, firstQuery, queryCount, dataSize,
	                                        pData, stride, flags);
	VK_ASSERT(result, "Failed to get query pool results.");
	return result;
}

VkResult vk::Device::createBuffer(const VkBufferCreateInfo* pCreateInfo,
                                  const VkAllocationCallbacks* pAllocator, VkBuffer* pBuffer) {
	VkResult result = vkCreateBuffer(handle, pCreateInfo, pAllocator, pBuffer);
	VK_ASSERT(result, "Failed to create buffer.");
	return result;
}

void vk::Device::createBuffer(const VkBufferCreateInfo* pCreateInfo, VkBuffer* pBuffer) {
	createBuffer(pCreateInfo, nullptr, pBuffer);
}

void vk::Device::destroyBuffer(VkBuffer buffer, const VkAllocationCallbacks* pAllocator) {
	vkDestroyBuffer(handle, buffer, pAllocator);
}

VkResult vk::Device::createBufferView(const VkBufferViewCreateInfo* pCreateInfo,
                                      const VkAllocationCallbacks* pAllocator, VkBufferView* pView) {
	VkResult result = vkCreateBufferView(handle, pCreateInfo, pAllocator, pView);
	VK_ASSERT(result, "Failed to create buffer view.");
	return result;
}

void vk::Device::createBufferView(const VkBufferViewCreateInfo* pCreateInfo, VkBufferView* pView) {
	createBufferView(pCreateInfo, nullptr, pView);
}

void
vk::Device::destroyBufferView(VkBufferView bufferView, const VkAllocationCallbacks* pAllocator) {
	vkDestroyBufferView(handle, bufferView, pAllocator);
}

VkResult vk::Device::createImage(const VkImageCreateInfo* pCreateInfo,
                                 const VkAllocationCallbacks* pAllocator, VkImage* pImage) {
	VkResult result = vkCreateImage(handle, pCreateInfo, pAllocator, pImage);
	VK_ASSERT(result, "Failed to create image.");
	return result;
}

void vk::Device::createImage(const VkImageCreateInfo* pCreateInfo, VkImage* pImage) {
	createImage(pCreateInfo, nullptr, pImage);
}

void vk::Device::destroyImage(VkImage image, const VkAllocationCallbacks* pAllocator) {
	vkDestroyImage(handle, image, pAllocator);
}

void vk::Device::getImageSubresourceLayout(VkImage image, const VkImageSubresource* pSubresource,
                                           VkSubresourceLayout* pLayout) {
	vkGetImageSubresourceLayout(handle, image, pSubresource, pLayout);
}

VkResult vk::Device::createImageView(const VkImageViewCreateInfo* pCreateInfo,
                                     const VkAllocationCallbacks* pAllocator, VkImageView* pView) {
	VkResult result = vkCreateImageView(handle, pCreateInfo, pAllocator, pView);
	VK_ASSERT(result, "Failed to create image view.");
	return result;
}

void vk::Device::createImageView(const VkImageViewCreateInfo* pCreateInfo, VkImageView* pView) {
	createImageView(pCreateInfo, nullptr, pView);
}

void vk::Device::destroyImageView(VkImageView imageView, const VkAllocationCallbacks* pAllocator) {
	vkDestroyImageView(handle, imageView, pAllocator);
}

VkResult vk::Device::createShaderModule(const VkShaderModuleCreateInfo* pCreateInfo,
                                        const VkAllocationCallbacks* pAllocator,
                                        VkShaderModule* pShaderModule) {
	VkResult result = vkCreateShaderModule(handle, pCreateInfo, pAllocator, pShaderModule);
	VK_ASSERT(result, "Failed to create shader module.");
	return result;
}

void vk::Device::createShaderModule(const VkShaderModuleCreateInfo* pCreateInfo,
                                    VkShaderModule* pShaderModule) {
	createShaderModule(pCreateInfo, nullptr, pShaderModule);
}

void vk::Device::destroyShaderModule(VkShaderModule shaderModule,
                                     const VkAllocationCallbacks* pAllocator) {
	vkDestroyShaderModule(handle, shaderModule, pAllocator);
}

VkResult vk::Device::createPipelineCache(const VkPipelineCacheCreateInfo* pCreateInfo,
                                         const VkAllocationCallbacks* pAllocator,
                                         VkPipelineCache* pPipelineCache) {
	VkResult result = vkCreatePipelineCache(handle, pCreateInfo, pAllocator, pPipelineCache);
	VK_ASSERT(result, "Failed to create pipeline cache.");
	return result;
}

void vk::Device::createPipelineCache(const VkPipelineCacheCreateInfo* pCreateInfo,
                                     VkPipelineCache* pPipelineCache) {
	createPipelineCache(pCreateInfo, nullptr, pPipelineCache);
}

void vk::Device::destroyPipelineCache(VkPipelineCache pipelineCache,
                                      const VkAllocationCallbacks* pAllocator) {
	vkDestroyPipelineCache(handle, pipelineCache, pAllocator);
}

void
vk::Device::getPipelineCacheData(VkPipelineCache pipelineCache, size_t* pDataSize, void* pData) {
	VkResult r = vkGetPipelineCacheData(handle, pipelineCache, pDataSize, pData);
	VK_ASSERT(r, "Failed to get pipeline cache data.");
}

VkResult vk::Device::mergePipelineCaches(VkPipelineCache dstCache, uint32_t srcCacheCount,
                                         const VkPipelineCache* pSrcCaches) {
	VkResult result = vkMergePipelineCaches(handle, dstCache, srcCacheCount, pSrcCaches);
	VK_ASSERT(result, "Failed to merge pipeline caches.");
	return result;
}

VkResult vk::Device::createGraphicsPipelines(VkPipelineCache pipelineCache, uint32_t createInfoCount,
                                             const VkGraphicsPipelineCreateInfo* pCreateInfos,
                                             const VkAllocationCallbacks* pAllocator,
                                             VkPipeline* pPipelines) {
	VkResult result = vkCreateGraphicsPipelines(handle, pipelineCache, createInfoCount, pCreateInfos,
	                                            pAllocator, pPipelines);
	VK_ASSERT(result, "Failed to create graphics pipelines.");
	return result;
}

void vk::Device::createGraphicsPipelines(VkPipelineCache pipelineCache, uint32_t createInfoCount,
                                         const VkGraphicsPipelineCreateInfo* pCreateInfos,
                                         VkPipeline* pPipelines) {
	createGraphicsPipelines(pipelineCache, createInfoCount, pCreateInfos, nullptr, pPipelines);
}

VkResult vk::Device::createComputePipelines(VkPipelineCache pipelineCache, uint32_t createInfoCount,
                                            const VkComputePipelineCreateInfo* pCreateInfos,
                                            const VkAllocationCallbacks* pAllocator,
                                            VkPipeline* pPipelines) {
	VkResult result = vkCreateComputePipelines(handle, pipelineCache, createInfoCount, pCreateInfos,
	                                           pAllocator, pPipelines);
	VK_ASSERT(result, "Failed to create compute pipelines.");
	return result;
}

void vk::Device::createComputePipelines(VkPipelineCache pipelineCache, uint32_t createInfoCount,
                                        const VkComputePipelineCreateInfo* pCreateInfos,
                                        VkPipeline* pPipelines) {
	createComputePipelines(pipelineCache, createInfoCount, pCreateInfos, nullptr, pPipelines);
}

void vk::Device::destroyPipeline(VkPipeline pipeline, const VkAllocationCallbacks* pAllocator) {
	vkDestroyPipeline(handle, pipeline, pAllocator);
}

VkResult vk::Device::createPipelineLayout(const VkPipelineLayoutCreateInfo* pCreateInfo,
                                          const VkAllocationCallbacks* pAllocator,
                                          VkPipelineLayout* pPipelineLayout) {
	VkResult result = vkCreatePipelineLayout(handle, pCreateInfo, pAllocator, pPipelineLayout);
	VK_ASSERT(result, "Failed to create pipelines layout.");
	return result;
}

void vk::Device::createPipelineLayout(const VkPipelineLayoutCreateInfo* pCreateInfo,
                                      VkPipelineLayout* pPipelineLayout) {
	createPipelineLayout(pCreateInfo, nullptr, pPipelineLayout);
}

void vk::Device::destroyPipelineLayout(VkPipelineLayout pipelineLayout,
                                       const VkAllocationCallbacks* pAllocator) {
	vkDestroyPipelineLayout(handle, pipelineLayout, pAllocator);
}

VkResult vk::Device::createSampler(const VkSamplerCreateInfo* pCreateInfo,
                                   const VkAllocationCallbacks* pAllocator, VkSampler* pSampler) {
	VkResult result = vkCreateSampler(handle, pCreateInfo, pAllocator, pSampler);
	VK_ASSERT(result, "Failed to create sampler.");
	return result;
}

void vk::Device::createSampler(const VkSamplerCreateInfo* pCreateInfo, VkSampler* pSampler) {
	createSampler(pCreateInfo, nullptr, pSampler);
}

void vk::Device::destroySampler(VkSampler sampler, const VkAllocationCallbacks* pAllocator) {
	vkDestroySampler(handle, sampler, pAllocator);
}

VkResult vk::Device::createDescriptorSetLayout(const VkDescriptorSetLayoutCreateInfo* pCreateInfo,
                                               const VkAllocationCallbacks* pAllocator,
                                               VkDescriptorSetLayout* pSetLayout) {
	VkResult result = vkCreateDescriptorSetLayout(handle, pCreateInfo, pAllocator, pSetLayout);
	VK_ASSERT(result, "Failed to create descriptor set layout.");
	return result;
}

void vk::Device::createDescriptorSetLayout(const VkDescriptorSetLayoutCreateInfo* pCreateInfo,
                                           VkDescriptorSetLayout* pSetLayout) {
	createDescriptorSetLayout(pCreateInfo, nullptr, pSetLayout);
}

void vk::Device::destroyDescriptorSetLayout(VkDescriptorSetLayout descriptorSetLayout,
                                            const VkAllocationCallbacks* pAllocator) {
	vkDestroyDescriptorSetLayout(handle, descriptorSetLayout, pAllocator);
}

VkResult vk::Device::createDescriptorPool(const VkDescriptorPoolCreateInfo* pCreateInfo,
                                          const VkAllocationCallbacks* pAllocator,
                                          VkDescriptorPool* pDescriptorPool) {
	VkResult result = vkCreateDescriptorPool(handle, pCreateInfo, pAllocator, pDescriptorPool);
	VK_ASSERT(result, "Failed to create descriptor pool.");
	return result;
}

void vk::Device::createDescriptorPool(const VkDescriptorPoolCreateInfo* pCreateInfo,
                                      VkDescriptorPool* pDescriptorPool) {
	createDescriptorPool(pCreateInfo, nullptr, pDescriptorPool);
}

void vk::Device::destroyDescriptorPool(VkDescriptorPool descriptorPool,
                                       const VkAllocationCallbacks* pAllocator) {
	vkDestroyDescriptorPool(handle, descriptorPool, pAllocator);
}

VkResult
vk::Device::resetDescriptorPool(VkDescriptorPool descriptorPool, VkDescriptorPoolResetFlags flags) {
	VkResult result = vkResetDescriptorPool(handle, descriptorPool, flags);
	VK_ASSERT(result, "Failed to reset descriptor pool.");
	return result;
}

VkResult vk::Device::allocateDescriptorSets(const VkDescriptorSetAllocateInfo* pAllocateInfo,
                                            VkDescriptorSet* pDescriptorSets) {
	VkResult result = vkAllocateDescriptorSets(handle, pAllocateInfo, pDescriptorSets);
	VK_ASSERT(result, "Failed to allocate descriptor sets.");
	return result;
}

VkResult vk::Device::freeDescriptorSets(VkDescriptorPool descriptorPool, uint32_t descriptorSetCount,
                                        const VkDescriptorSet* pDescriptorSets) {
	VkResult result = vkFreeDescriptorSets(handle, descriptorPool, descriptorSetCount,
	                                       pDescriptorSets);
	VK_ASSERT(result, "Failed to free descriptor sets.");
	return result;
}

void vk::Device::updateDescriptorSets(uint32_t descriptorWriteCount,
                                      const VkWriteDescriptorSet* pDescriptorWrites,
                                      uint32_t descriptorCopyCount,
                                      const VkCopyDescriptorSet* pDescriptorCopies) {
	vkUpdateDescriptorSets(handle, descriptorWriteCount, pDescriptorWrites, descriptorCopyCount,
	                       pDescriptorCopies);
}

void vk::Device::updateDescriptorSets(const std::vector<VkWriteDescriptorSet>& descriptorSets,
                                      const std::vector<VkCopyDescriptorSet>& descriptorCopies) {
	updateDescriptorSets(static_cast<uint32_t>(descriptorSets.size()), descriptorSets.data(),
	                     static_cast<uint32_t>(descriptorCopies.size()),
	                     descriptorCopies.data());
}

VkResult vk::Device::createFramebuffer(const VkFramebufferCreateInfo* pCreateInfo,
                                       const VkAllocationCallbacks* pAllocator,
                                       VkFramebuffer* pFramebuffer) {
	VkResult result = vkCreateFramebuffer(handle, pCreateInfo, pAllocator, pFramebuffer);
	VK_ASSERT(result, "Failed to create framebuffer.");
	return result;
}

VkResult vk::Device::createFramebuffer(const VkFramebufferCreateInfo* pCreateInfo,
                                       VkFramebuffer* pFramebuffer) {
	VkResult result = vkCreateFramebuffer(handle, pCreateInfo, nullptr, pFramebuffer);
	VK_ASSERT(result, "Failed to create framebuffer.");
	return result;
}

void
vk::Device::destroyFramebuffer(VkFramebuffer framebuffer, const VkAllocationCallbacks* pAllocator) {
	vkDestroyFramebuffer(handle, framebuffer, pAllocator);
}

VkResult vk::Device::createRenderPass(const VkRenderPassCreateInfo* pCreateInfo,
                                      const VkAllocationCallbacks* pAllocator,
                                      VkRenderPass* pRenderPass) {
	VkResult result = vkCreateRenderPass(handle, pCreateInfo, pAllocator, pRenderPass);
	VK_ASSERT(result, "Failed to create render pass.");
	return result;
}

void
vk::Device::createRenderPass(const VkRenderPassCreateInfo* pCreateInfo, VkRenderPass* pRenderPass) {
	createRenderPass(pCreateInfo, nullptr, pRenderPass);
}

void
vk::Device::destroyRenderPass(VkRenderPass renderPass, const VkAllocationCallbacks* pAllocator) {
	vkDestroyRenderPass(handle, renderPass, pAllocator);
}

void vk::Device::getRenderAreaGranularity(VkRenderPass renderPass, VkExtent2D* pGranularity) {
	vkGetRenderAreaGranularity(handle, renderPass, pGranularity);
}

VkResult vk::Device::createCommandPool(const VkCommandPoolCreateInfo* pCreateInfo,
                                       const VkAllocationCallbacks* pAllocator,
                                       VkCommandPool* pCommandPool) {
	VkResult result = vkCreateCommandPool(handle, pCreateInfo, pAllocator, pCommandPool);
	VK_ASSERT(result, "Failed to create command pool.");
	return result;
}

VkResult vk::Device::createCommandPool(const VkCommandPoolCreateInfo* pCreateInfo,
                                       VkCommandPool* pCommandPool) {
	VkResult result = createCommandPool(pCreateInfo, nullptr, pCommandPool);
	VK_ASSERT(result, "Failed to create command pool.");
	return result;
}

void
vk::Device::destroyCommandPool(VkCommandPool commandPool, const VkAllocationCallbacks* pAllocator) {
	vkDestroyCommandPool(handle, commandPool, pAllocator);
}

VkResult vk::Device::resetCommandPool(VkCommandPool commandPool, VkCommandPoolResetFlags flags) {
	VkResult result = vkResetCommandPool(handle, commandPool, flags);
	VK_ASSERT(result, "Failed to reset command pool.");
	return result;
}

VkResult vk::Device::allocateCommandBuffers(const VkCommandBufferAllocateInfo* pAllocateInfo,
                                            VkCommandBuffer* pCommandBuffers) {
	VkResult result = vkAllocateCommandBuffers(handle, pAllocateInfo, pCommandBuffers);
	VK_ASSERT(result, "Failed to allocate command buffers.");
	return result;
}

VkResult vk::Device::allocateCommandBuffers(const VkCommandBufferAllocateInfo* pAllocateInfo,
                                            const std::vector<vk::CommandBuffer>& commandBuffers) {
	return allocateCommandBuffers(pAllocateInfo, (VkCommandBuffer*) commandBuffers.data());
}

void vk::Device::freeCommandBuffers(VkCommandPool commandPool, uint32_t commandBufferCount,
                                    const VkCommandBuffer* pCommandBuffers) {
	vkFreeCommandBuffers(handle, commandPool, commandBufferCount, pCommandBuffers);
}

void vk::Device::freeCommandBuffers(VkCommandPool commandPool,
                                    const std::vector<VkCommandBuffer>& commandBuffers) {
	freeCommandBuffers(commandPool, static_cast<uint32_t>(commandBuffers.size()),
	                   commandBuffers.data());
}

void vk::Device::freeCommandBuffers(VkCommandPool commandPool,
                                    const std::vector<vk::CommandBuffer>& commandBuffers) {
	freeCommandBuffers(commandPool, (const std::vector<VkCommandBuffer>&) commandBuffers);
}

VkResult vk::Device::createSwapchain(VkSwapchainCreateInfoKHR* pCreateInfo,
                                     const VkAllocationCallbacks* pAllocator,
                                     VkSwapchainKHR* pSwapchain) {
	VkResult result = vkCreateSwapchainKHR(handle, pCreateInfo, pAllocator, pSwapchain);
	//VK_ASSERT(result, "Failed to create swapchain.");
	return result;
}

VkResult
vk::Device::createSwapchain(VkSwapchainCreateInfoKHR* pCreateInfo, VkSwapchainKHR* pSwapchain) {
	return createSwapchain(pCreateInfo, nullptr, pSwapchain);
}

void
vk::Device::destroySwapchain(VkSwapchainKHR swapchain, const VkAllocationCallbacks* pAllocator) {
	vkDestroySwapchainKHR(handle, swapchain, pAllocator);
}

VkResult vk::Device::getSwapchainImages(VkSwapchainKHR swapchain, uint32_t* pSwapchainImageCount,
                                        VkImage* pSwapchainImages) {
	VkResult result = vkGetSwapchainImagesKHR(handle, swapchain, pSwapchainImageCount,
	                                          pSwapchainImages);
	//VK_ASSERT(result, "Failed to get swapchain images.");
	return result;
}

VkResult
vk::Device::getSwapchainImages(VkSwapchainKHR swapchain, std::vector<VkImage>& swapchainImages) {
	uint32_t count;
	VkResult result = getSwapchainImages(swapchain, &count, nullptr);
	VK_ASSERT(result, "Failed to get swapchain images.");
	swapchainImages.resize(count);

	result = getSwapchainImages(swapchain, &count, swapchainImages.data());
	VK_ASSERT(result, "Failed to get swapchain images.");
	return result;
}

VkResult vk::Device::acquireNextImage(VkSwapchainKHR swapchain, uint64_t timeout, VkSemaphore semaphore, VkFence fence,
                                      uint32_t* pImageIndex) {
	VkResult r = vkAcquireNextImageKHR(handle, swapchain, timeout, semaphore, fence, pImageIndex);
	VK_ASSERT(r, "Failed to acquire next image.");
	return r;
}











