#include "Vulkan/PhysicalDevice.h"

#include <memory>

#include "Vulkan/VulkanCommon.h"
#include "Vulkan/Surface.h"

namespace vk {

PhysicalDevice::PhysicalDevice() : handle(nullptr) {}

PhysicalDevice::PhysicalDevice(VkPhysicalDevice handle) : handle(handle) {}

VkPhysicalDevice PhysicalDevice::getHandle() const {
	return handle;
}

VkPhysicalDevice PhysicalDevice::operator*() const {
	return getHandle();
}

bool PhysicalDevice::notValid() const {
	return handle == nullptr;
}

unique_ptr<VkPhysicalDeviceFeatures> PhysicalDevice::getFeatures() {
	auto features = std::make_unique<VkPhysicalDeviceFeatures>();
	vkGetPhysicalDeviceFeatures(handle, features.get());
	return features;
}

unique_ptr<VkFormatProperties> PhysicalDevice::getFormatProperties(VkFormat format) {
	auto props = std::make_unique<VkFormatProperties>();
	vkGetPhysicalDeviceFormatProperties(handle, format, props.get());
	return props;
}

unique_ptr<VkImageFormatProperties>
PhysicalDevice::getImageFormatProperties(VkFormat format, VkImageType type, VkImageTiling tiling,
                                         VkImageUsageFlags usage, VkImageCreateFlags flags) {
	auto props = std::make_unique<VkImageFormatProperties>();
	auto result = vkGetPhysicalDeviceImageFormatProperties(handle, format, type, tiling, usage,
	                                                       flags, props.get());
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Failed to get physical device image format properties.");
	}
	return props;
}

void PhysicalDevice::getProperties(VkPhysicalDeviceProperties* properties) const {
	vkGetPhysicalDeviceProperties(handle, properties);
}

unique_ptr<VkPhysicalDeviceProperties> PhysicalDevice::getProperties() const {
	auto props = std::make_unique<VkPhysicalDeviceProperties>();
	vkGetPhysicalDeviceProperties(handle, props.get());
	return props;
}

std::vector<VkQueueFamilyProperties>* PhysicalDevice::getQueueFamilyProperties() {
	auto vec = new std::vector<VkQueueFamilyProperties>();
	uint32_t count;

	vkGetPhysicalDeviceQueueFamilyProperties(handle, &count, nullptr);

	vec->resize(count);
	vkGetPhysicalDeviceQueueFamilyProperties(handle, &count, vec->data());
	return vec;
}

unique_ptr<VkPhysicalDeviceMemoryProperties> PhysicalDevice::getMemoryProperties() {
	auto props = std::make_unique<VkPhysicalDeviceMemoryProperties>();
	vkGetPhysicalDeviceMemoryProperties(handle, props.get());
	return props;
}

Device
PhysicalDevice::createDevice(VkDeviceCreateInfo* pCreateInfo, ALLOCATOR) {
	VkDevice device;
	VkResult result = vkCreateDevice(handle, pCreateInfo, pAllocator, &device);
	VK_ASSERT(result, "Failed to create device.");
	return Device(device);
}

std::vector<VkExtensionProperties>*
PhysicalDevice::enumerateExtensionProperties(const std::string& layerName) {
	return enumerateExtensionProperties(layerName.c_str());
}

std::vector<VkExtensionProperties>*
PhysicalDevice::enumerateExtensionProperties(const char* pLayerName) {
	uint32_t count;
	VkResult result;

	result = vkEnumerateDeviceExtensionProperties(handle, pLayerName, &count, nullptr);
	VK_ASSERT(result, "Failed to enumerate device extension properties count.");

	auto vec = new std::vector<VkExtensionProperties>();
	vec->resize(count);

	result = vkEnumerateDeviceExtensionProperties(handle, pLayerName, &count, vec->data());
	VK_ASSERT(result, "Failed to enumerate device extension properties.");

	return vec;
}


std::vector<VkLayerProperties>* PhysicalDevice::enumerateLayerProperties() {
	uint32_t count;
	VkResult result;
	result = vkEnumerateDeviceLayerProperties(handle, &count, nullptr);
	VK_ASSERT(result, "Failed to enumerate device layer properties count.");

	auto vec = new std::vector<VkLayerProperties>();
	vkEnumerateDeviceLayerProperties(handle, &count, vec->data());
	VK_ASSERT(result, "Failed to enumerate device layer properties.");

	return vec;
}

std::vector<VkSparseImageFormatProperties>*
PhysicalDevice::getSparseImageFromatProperties(VkFormat format, VkImageType type,
                                               VkSampleCountFlagBits samples,
                                               VkImageUsageFlags usage, VkImageTiling tiling) {
	uint32_t count;
	vkGetPhysicalDeviceSparseImageFormatProperties(handle, format, type, samples, usage, tiling,
	                                               &count, nullptr);

	auto vec = new std::vector<VkSparseImageFormatProperties>();
	vec->resize(count);

	vkGetPhysicalDeviceSparseImageFormatProperties(handle, format, type, samples, usage, tiling,
	                                               &count, vec->data());

	return vec;
}

QueueFamilyIndices PhysicalDevice::getQueueFamilyIndices(Surface* surface) {
	QueueFamilyIndices indices;

	auto queueFamilyProperties = getQueueFamilyProperties();

	uint32_t index = 0;
	for (const auto& queueFamily : *queueFamilyProperties) {
		if (queueFamily.queueCount > 0 &&
		    queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			indices.graphicsFamily = index;
		}

		bool presentSupport = getSurfaceSupportKHR(index, surface);

		if (queueFamily.queueCount > 0 && presentSupport) {
			indices.presentFamily = index;
		}

		if (indices.isComplete()) {
			break;
		}

		index++;
	}

	delete queueFamilyProperties;
	return indices;
}

/**
 * To determine whether a queue family of a physical device supports presentation to a given surface.
 * @param queueFamilyIndex - queue family
 * @param surface - surface
 * @return
 */
bool PhysicalDevice::getSurfaceSupportKHR(uint32_t queueFamilyIndex, const Surface* surface) const {
	VkBool32 supported;
	vkGetPhysicalDeviceSurfaceSupportKHR(handle, queueFamilyIndex, surface->getHandle(), &supported);
	return supported == VK_TRUE;
}

void PhysicalDevice::getSurfaceCapabilities(Surface surface, VkSurfaceCapabilitiesKHR* pSurfaceCapabilities) const {
	VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(handle, surface.getHandle(),
	                                                            pSurfaceCapabilities);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Failed to get surface capabilities.");
	}
}

std::vector<VkSurfaceFormatKHR> PhysicalDevice::getSurfaceFormats(Surface surface) const {
	uint32_t count;
	vkGetPhysicalDeviceSurfaceFormatsKHR(handle, surface.getHandle(), &count, nullptr);
	std::vector<VkSurfaceFormatKHR> vec;
	if (count > 0) {
		vec.resize(count);
		vkGetPhysicalDeviceSurfaceFormatsKHR(handle, surface.getHandle(), &count,
		                                     vec.data());
	}
	return vec;
}

void PhysicalDevice::getSurfaceFormats(Surface surface, std::vector<VkSurfaceFormatKHR>& formats) const {
	uint32_t count;
	vkGetPhysicalDeviceSurfaceFormatsKHR(handle, surface.getHandle(), &count, nullptr);

	if (count > 0) {
		formats.resize(count);
		vkGetPhysicalDeviceSurfaceFormatsKHR(handle, surface.getHandle(), &count,
		                                     formats.data());
	}
}

std::vector<VkPresentModeKHR> PhysicalDevice::getSurfacePresentModesKHR(Surface surface) const {
	uint32_t count;
	vkGetPhysicalDeviceSurfacePresentModesKHR(handle, surface.getHandle(), &count, nullptr);
	std::vector<VkPresentModeKHR> vec;
	if (count > 0) {
		vec.resize(count);
		vkGetPhysicalDeviceSurfacePresentModesKHR(handle, surface.getHandle(), &count,
		                                          vec.data());
	}
	return vec;
}

void
PhysicalDevice::getSurfacePresentModesKHR(Surface surface, std::vector<VkPresentModeKHR>& modes) const {
	uint32_t count;
	vkGetPhysicalDeviceSurfacePresentModesKHR(handle, surface.getHandle(), &count, nullptr);

	if (count > 0) {
		modes.resize(count);
		vkGetPhysicalDeviceSurfacePresentModesKHR(handle, surface.getHandle(), &count,
		                                          modes.data());
	}
}

std::string PhysicalDevice::getName() const {
	VkPhysicalDeviceProperties props{};
	getProperties(&props);
	return std::string(props.deviceName);
}

} // END NAMESPACE vk