#ifndef VULKANTEST_TIMER_H
#define VULKANTEST_TIMER_H

#include <chrono>
#include <string>
#include <memory>
#include <thread>

#include "GlfwInclude.h"
#include "Common.h"

class Timer {

private:
	double startPoint;
	double lastMeasure;
	double lastFrame;

	size_t avarageLength = 10;
	double* avarageFps;
	size_t counter = 0;
	size_t fpsIndex = 0;

public:

	Timer() {
		start();
		avarageFps = new double[avarageLength];
	}

	void start() {
		startPoint = lastMeasure = lastFrame = glfwGetTime();
	}

	/**
	 * Measure time since start.
	 * @return
	 */
	double measure() {
		lastMeasure = glfwGetTime();
		return lastMeasure - startPoint;
	}

	/**
	 * Measure time since last measure.
	 * @return
	 */
	double measureInterval() {
		auto now = glfwGetTime();
		double duration = now - lastMeasure;
		lastMeasure = now;
		return duration;
	}

	/**
	 * Get time since start.
	 * @return
	 */
	double time() {
		auto now = glfwGetTime();
		double duration = now - startPoint;
		return duration;
	}

	/**
	 * Marks end of frame
	 */
	void frame() {
		avarageFps[fpsIndex] = delta();
		if (counter < avarageLength) {
			counter += 1;
		}
		if (fpsIndex == avarageLength) {
			fpsIndex = 0;
		} else {
			fpsIndex += 1;
		}

		lastFrame = glfwGetTime();
	}

	double getFps() {
		if (counter == 0) {
			return 0;
		}
		double avarage = 0;
		for (size_t i=0; i<MIN2(avarageLength, counter); i++) {
			avarage += avarageFps[i];
		}
		return 1 / (avarage / MIN2(avarageLength, counter));
	}

	/**
	 * Get last frame duration
	 * @return
	 */
	double delta() {
		auto now = glfwGetTime();
		double duration = now - lastFrame;
		return duration;
	}

	void sleep(double time) {
		std::this_thread::sleep_for(std::chrono::duration<double>(time));
	}

	double now() {
		return glfwGetTime();
	}
};

#endif //VULKANTEST_TIMER_H
