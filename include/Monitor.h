#ifndef VULKANTEST_MONITOR_H
#define VULKANTEST_MONITOR_H

#include <vector>

#include "GlfwInclude.h"

typedef GLFWvidmode VideoMode;

class Monitor {
private:
	GLFWmonitor* handle;

public:
	GLFWmonitor* getHandle() const;

	/**
	 * This function returns the current video mode of this monitor. If you have created a full screen window for this monitor, the return value will depend on whether that window is iconified.
	 * The returned array is allocated and freed by GLFW. You should not free it yourself. It is valid until the specified monitor is disconnected or the library is terminated.
	 * @return The current mode of the monitor, or NULL if an error occurred.
	 */
	const VideoMode* getVideoMode() const;

	bool equals(Monitor* other) const;

	static std::vector<Monitor>* getMonitors();
};


#endif //VULKANTEST_MONITOR_H
