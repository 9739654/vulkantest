#ifndef VULKANTEST_DEVICE_H
#define VULKANTEST_DEVICE_H

#include "Common.h"

#include <vector>

#include "Vulkan/VulkanCommon.h"
#include "Vulkan/Queue.h"
#include "Vulkan/CommandBuffer.h"

namespace vk {

class Device {
private:
	VkDevice handle;

public:
	Device();

	explicit Device(VkDevice handle);

	VkDevice getHandle() const;

	VkDevice operator*() const;

	bool notValid() const;

	///

	void downloadMemory(VkDeviceMemory memory, void* src, VkDeviceSize size, VkDeviceSize offset = 0,
	                    VkMemoryMapFlags flags = 0);

	///

	PFN_vkVoidFunction getProcAddr(const std::string& name);

	void destroy(const VkAllocationCallbacks* pAllocator = nullptr);

	void getQueue(int queueFamilyIndex, uint32_t queueIndex, VkQueue* pQueue);

	void getQueue(uint32_t queueFamilyIndex, uint32_t queueIndex, VkQueue* pQueue);

	vk::Queue getQueue(int queueFamilyIndex, uint32_t queueIndex);

	VkResult waitIdle();

	VkResult allocateMemory(const VkMemoryAllocateInfo* pAllocateInfo, const VkAllocationCallbacks* pAllocator,
	                        VkDeviceMemory* pMemory);

	void allocateMemory(const VkMemoryAllocateInfo* pAllocateInfo, VkDeviceMemory* pMemory);

	void freeMemory(VkDeviceMemory memory, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult
	mapMemory(VkDeviceMemory memory, VkDeviceSize offset, VkDeviceSize size, VkMemoryMapFlags flags, void** ppData);

	void unmapMemory(VkDeviceMemory memory);

	VkResult flushMappedMemoryRanges(uint32_t memoryRangeCount, const VkMappedMemoryRange* pMemoryRanges);

	VkResult invalidateMappedMemoryRanges(uint32_t memoryRangeCount, const VkMappedMemoryRange* pMemoryRanges);

	void getMemoryCommitment(VkDeviceMemory memory, VkDeviceSize* pCommitedMemoryInBytes);

	VkResult bindBufferMemory(VkBuffer buffer, VkDeviceMemory memory, VkDeviceSize memoryOffset);

	VkResult bindImageMemory(VkImage image, VkDeviceMemory memory, VkDeviceSize memoryOffset);

	void getBufferMemoryRequirements(VkBuffer buffer, VkMemoryRequirements* pMemoryRequirements);

	void getImageMemoryRequirements(VkImage image, VkMemoryRequirements* pMemoryRequirements);

	void getImageSparseMemoryRequirements(VkImage image, uint32_t* pSparseMemoryRequirementsCount,
	                                      VkSparseImageMemoryRequirements* pSparseMemoryRequirements);

	VkResult
	createFence(const VkFenceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkFence* pFence);

	void createFence(const VkFenceCreateInfo* pCreateInfo, VkFence* pFence);

	void destroyFence(VkFence fence, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult resetFences(uint32_t fenceCount, const VkFence* pFences);

	/**
	 * Return the status of a fence.
	 *
	 * On success, this command returns:<br>
	 * VK_SUCCESS<br>
	 * VK_NOT_READY<br>
	 *
	 * On failure, this command returns:<br>
	 * VK_ERROR_OUT_OF_HOST_MEMORY<br>
	 * VK_ERROR_OUT_OF_DEVICE_MEMORY<br>
	 * VK_ERROR_DEVICE_LOST
	 *
	 * @param fence is the handle of the fence to query.
	 * @return
	 */
	VkResult getFenceStatus(VkFence fence);

	VkResult waitForFences(uint32_t fenceCount, const VkFence* pFences, bool waitAll, uint64_t timeout);

	VkResult createSemaphore(const VkSemaphoreCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                         VkSemaphore* pSemaphore);

	void createSemaphore(const VkSemaphoreCreateInfo* pCreateInfo, VkSemaphore* pSemaphore);

	void destroySemaphore(VkSemaphore semaphore, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult
	createEvent(const VkEventCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkEvent* pEvent);

	void createEvent(const VkEventCreateInfo* pCreateInfo, VkEvent* pEvent);

	void destroyEvent(VkEvent event, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult getEventStatus(VkEvent event);

	VkResult setEvent(VkEvent event);

	VkResult resetEvent(VkEvent event);

	VkResult createQueryPool(const VkQueryPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                         VkQueryPool* pQueryPool);

	void createQueryPool(const VkQueryPoolCreateInfo* pCreateInfo, VkQueryPool* pQueryPool);

	void destroyQueryPool(VkQueryPool queryPool, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult
	getQueryPoolResults(VkQueryPool queryPool, uint32_t firstQuery, uint32_t queryCount, size_t dataSize, void* pData,
	                    VkDeviceSize stride, VkQueryResultFlags flags);

	VkResult
	createBuffer(const VkBufferCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkBuffer* pBuffer);

	void createBuffer(const VkBufferCreateInfo* pCreateInfo, VkBuffer* pBuffer);

	void destroyBuffer(VkBuffer buffer, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult createBufferView(const VkBufferViewCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                          VkBufferView* pView);

	void createBufferView(const VkBufferViewCreateInfo* pCreateInfo, VkBufferView* pView);

	void destroyBufferView(VkBufferView bufferView, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult
	createImage(const VkImageCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkImage* pImage);

	void createImage(const VkImageCreateInfo* pCreateInfo, VkImage* pImage);

	void destroyImage(VkImage image, const VkAllocationCallbacks* pAllocator = nullptr);

	void getImageSubresourceLayout(VkImage image, const VkImageSubresource* pSubresource, VkSubresourceLayout* pLayout);

	VkResult createImageView(const VkImageViewCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                         VkImageView* pView);

	void createImageView(const VkImageViewCreateInfo* pCreateInfo, VkImageView* pView);

	void destroyImageView(VkImageView imageView, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult createShaderModule(const VkShaderModuleCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                            VkShaderModule* pShaderModule);

	void createShaderModule(const VkShaderModuleCreateInfo* pCreateInfo, VkShaderModule* pShaderModule);

	void destroyShaderModule(VkShaderModule shaderModule, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult createPipelineCache(const VkPipelineCacheCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                             VkPipelineCache* pPipelineCache);

	void createPipelineCache(const VkPipelineCacheCreateInfo* pCreateInfo, VkPipelineCache* pPipelineCache);

	void destroyPipelineCache(VkPipelineCache pipelineCache, const VkAllocationCallbacks* pAllocator = nullptr);

	void getPipelineCacheData(VkPipelineCache pipelineCache, size_t* pDataSize, void* pData);

	VkResult mergePipelineCaches(VkPipelineCache dstCache, uint32_t srcCacheCount, const VkPipelineCache* pSrcCaches);

	VkResult
	createGraphicsPipelines(VkPipelineCache pipelineCache, uint32_t createInfoCount,
	                        const VkGraphicsPipelineCreateInfo* pCreateInfos, const VkAllocationCallbacks* pAllocator,
	                        VkPipeline* pPipelines);

	void createGraphicsPipelines(VkPipelineCache pipelineCache, uint32_t createInfoCount,
	                             const VkGraphicsPipelineCreateInfo* pCreateInfos, VkPipeline* pPipelines);

	VkResult
	createComputePipelines(VkPipelineCache pipelineCache, uint32_t createInfoCount,
	                       const VkComputePipelineCreateInfo* pCreateInfos, const VkAllocationCallbacks* pAllocator,
	                       VkPipeline* pPipelines);

	void createComputePipelines(VkPipelineCache pipelineCache, uint32_t createInfoCount,
	                            const VkComputePipelineCreateInfo* pCreateInfos, VkPipeline* pPipelines);

	void destroyPipeline(VkPipeline pipeline, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult
	createPipelineLayout(const VkPipelineLayoutCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                     VkPipelineLayout* pPipelineLayout);

	void createPipelineLayout(const VkPipelineLayoutCreateInfo* pCreateInfo, VkPipelineLayout* pPipelineLayout);

	void destroyPipelineLayout(VkPipelineLayout pipelineLayout, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult
	createSampler(const VkSamplerCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSampler* pSampler);

	void createSampler(const VkSamplerCreateInfo* pCreateInfo, VkSampler* pSampler);

	void destroySampler(VkSampler sampler, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult createDescriptorSetLayout(const VkDescriptorSetLayoutCreateInfo* pCreateInfo,
	                                   const VkAllocationCallbacks* pAllocator, VkDescriptorSetLayout* pSetLayout);

	void
	createDescriptorSetLayout(const VkDescriptorSetLayoutCreateInfo* pCreateInfo, VkDescriptorSetLayout* pSetLayout);

	void destroyDescriptorSetLayout(VkDescriptorSetLayout descriptorSetLayout,
	                                const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult
	createDescriptorPool(const VkDescriptorPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                     VkDescriptorPool* pDescriptorPool);

	void createDescriptorPool(const VkDescriptorPoolCreateInfo* pCreateInfo, VkDescriptorPool* pDescriptorPool);

	void destroyDescriptorPool(VkDescriptorPool descriptorPool, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult resetDescriptorPool(VkDescriptorPool descriptorPool, VkDescriptorPoolResetFlags flags);

	VkResult
	allocateDescriptorSets(const VkDescriptorSetAllocateInfo* pAllocateInfo, VkDescriptorSet* pDescriptorSets);

	VkResult
	freeDescriptorSets(VkDescriptorPool descriptorPool, uint32_t descriptorSetCount,
	                   const VkDescriptorSet* pDescriptorSets);

	void updateDescriptorSets(uint32_t descriptorWriteCount, const VkWriteDescriptorSet* pDescriptorWrites,
	                          uint32_t descriptorCopyCount, const VkCopyDescriptorSet* pDescriptorCopies);

	void updateDescriptorSets(const std::vector<VkWriteDescriptorSet>& descriptorSets,
	                          const std::vector<VkCopyDescriptorSet>& descriptorCopies);

	VkResult createFramebuffer(const VkFramebufferCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                           VkFramebuffer* pFramebuffer);

	VkResult createFramebuffer(const VkFramebufferCreateInfo* pCreateInfo, VkFramebuffer* pFramebuffer);

	void destroyFramebuffer(VkFramebuffer framebuffer, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult createRenderPass(const VkRenderPassCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                          VkRenderPass* pRenderPass);

	void createRenderPass(const VkRenderPassCreateInfo* pCreateInfo, VkRenderPass* pRenderPass);

	void destroyRenderPass(VkRenderPass renderPass, const VkAllocationCallbacks* pAllocator = nullptr);

	void getRenderAreaGranularity(VkRenderPass renderPass, VkExtent2D* pGranularity);

	VkResult createCommandPool(const VkCommandPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator,
	                           VkCommandPool* pCommandPool);

	VkResult createCommandPool(const VkCommandPoolCreateInfo* pCreateInfo, VkCommandPool* pCommandPool);

	void destroyCommandPool(VkCommandPool commandPool, const VkAllocationCallbacks* pAllocator = nullptr);

	VkResult resetCommandPool(VkCommandPool commandPool, VkCommandPoolResetFlags flags = 0);

	VkResult
	allocateCommandBuffers(const VkCommandBufferAllocateInfo* pAllocateInfo, VkCommandBuffer* pCommandBuffers);

	VkResult
	allocateCommandBuffers(const VkCommandBufferAllocateInfo* pAllocateInfo,
	                       const std::vector<vk::CommandBuffer>& commandBuffers);

	void
	freeCommandBuffers(VkCommandPool commandPool, uint32_t commandBufferCount, const VkCommandBuffer* pCommandBuffers);

	void freeCommandBuffers(VkCommandPool commandPool, const std::vector<VkCommandBuffer>& commandBuffers);

	void freeCommandBuffers(VkCommandPool commandPool, const std::vector<vk::CommandBuffer>& commandBuffers);

	/* */

	VkResult createSwapchain(VkSwapchainCreateInfoKHR* pCreateInfo, ALLOCATOR, VkSwapchainKHR* pSwapchain);

	VkResult createSwapchain(VkSwapchainCreateInfoKHR* pCreateInfo, VkSwapchainKHR* pSwapchain);

	void destroySwapchain(VkSwapchainKHR swapchain, ALLOCATOR = nullptr);

	VkResult
	getSwapchainImages(VkSwapchainKHR swapchain, uint32_t* pSwapchainImageCount, VkImage* pSwapchainImages);

	VkResult getSwapchainImages(VkSwapchainKHR swapchain, std::vector<VkImage>& swapchainImages);

	/**
	 *
	 * @param swapchain is the swapchain from which an image is being acquired.
	 * @param timeout indicates how long the function waits, in nanoseconds, if no image is available.
	 * @param semaphore is VK_NULL_HANDLE or a semaphore to signal.
	 * @param fence is VK_NULL_HANDLE or a fence to signal.
	 * @param pImageIndex is a pointer to a uint32_t that is set to the index of the next image to use (i.e. an index
	 * into the array of images returned by vkGetSwapchainImagesKHR).
	 * @return Success:
	 * <ul>
	 * <li>VK_SUCCESS</li>
	 * <li>VK_TIMEOUT</li>
	 * <li>VK_NOT_READY</li>
	 * <li>VK_SUBOPTIMAL_KHR</li>
	 * </ul>
	 * Failure:
	 * <ul>
	 * <li>VK_ERROR_OUT_OF_HOST_MEMORY</li>
	 * <li>VK_ERROR_OUT_OF_DEVICE_MEMORY</li>
	 * <li>VK_ERROR_DEVICE_LOST</li>
	 * <li>VK_ERROR_OUT_OF_DATE_KHR</li>
	 * <li>VK_ERROR_SURFACE_LOST_KHR</li>
	 * </ul>
	 */
	VkResult acquireNextImage(VkSwapchainKHR swapchain, uint64_t timeout, VkSemaphore semaphore, VkFence fence,
	                          uint32_t* pImageIndex);
};


}

#endif //VULKANTEST_DEVICE_H
