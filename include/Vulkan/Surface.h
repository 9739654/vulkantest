#ifndef VULKANTEST_SURFACE_H
#define VULKANTEST_SURFACE_H

#include <Common.h>

#include "Vulkan/VulkanCommon.h"

namespace vk {

class Surface {
private:
	VkSurfaceKHR handle;

public:
	Surface();

	explicit Surface(VkSurfaceKHR handle);

	VkSurfaceKHR getHandle() const;

	VkSurfaceKHR operator*() const;

	bool notValid() const;

};

}

#endif //VULKANTEST_SURFACE_H
