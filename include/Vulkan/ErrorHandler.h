#ifndef VULKANTEST_ERRORHANDLER_H
#define VULKANTEST_ERRORHANDLER_H

#include <string>
#include <functional>

namespace vk {

void setErrorHandler(std::function<void(const std::string&)>);

} // END NAMESPACE vk

#endif //VULKANTEST_ERRORHANDLER_H
