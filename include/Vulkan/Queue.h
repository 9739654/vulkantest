#ifndef VULKANTEST_QUEUE_H
#define VULKANTEST_QUEUE_H

#include "Vulkan/VulkanCommon.h"

namespace vk {

class Queue {
private:
	VkQueue handle;

public:
	Queue();

	explicit Queue(VkQueue handle);

	VkQueue const& getHandle() const;

	VkQueue const& operator*() const;

	bool notValid() const;

	VkResult submit(uint32_t submitCount, const VkSubmitInfo* pSubmits, VkFence fence);

	VkResult waitIdle();

	VkResult bindSparse(uint32_t bindInfoCount, const VkBindSparseInfo* pBindInfo, VkFence fence);

	// -- //

	VkResult present(const VkPresentInfoKHR* pPresentInfo);
};

} // end namespace vk

#endif //VULKANTEST_QUEUE_H
