#ifndef VULKANTEST_VULKAN_H
#define VULKANTEST_VULKAN_H

#include <vector>

#include "GlmInclude.h"


struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

struct UniformBufferObject {
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 proj;
};

#endif //VULKANTEST_VULKAN_H
