#ifndef VULKANTEST_PHYSICALDEVICE_H
#define VULKANTEST_PHYSICALDEVICE_H

#include <memory>
#include <vector>

#include "Common.h"

#include "Vulkan/Surface.h"
#include "Vulkan/Device.h"
#include "Vulkan/VulkanCommon.h"
#include "Vulkan/Vulkan.h"

using std::unique_ptr;

namespace vk {

struct QueueFamilyIndices {
	int graphicsFamily = -1;
	int presentFamily = -1;

	bool isComplete() {
		return graphicsFamily >= 0 && presentFamily >= 0;
	}
};

class Instance;

class PhysicalDevice {
private:
	VkPhysicalDevice handle;

public:
	PhysicalDevice();

	explicit PhysicalDevice(VkPhysicalDevice handle);

	VkPhysicalDevice getHandle() const;

	VkPhysicalDevice operator*() const;

	bool operator<(vk::PhysicalDevice other) const {
		return handle < other.handle;
	}

	bool notValid() const;

	unique_ptr<VkPhysicalDeviceFeatures> getFeatures();

	unique_ptr<VkFormatProperties> getFormatProperties(VkFormat format);

	unique_ptr<VkImageFormatProperties>
	getImageFormatProperties(VkFormat format, VkImageType type, VkImageTiling tiling, VkImageUsageFlags usage,
	                         VkImageCreateFlags flags);

	void getProperties(VkPhysicalDeviceProperties* properties) const;

	unique_ptr<VkPhysicalDeviceProperties> getProperties() const;

	std::vector<VkQueueFamilyProperties>* getQueueFamilyProperties();

	unique_ptr<VkPhysicalDeviceMemoryProperties> getMemoryProperties();

	Device createDevice(VkDeviceCreateInfo* pCreateInfo, ALLOCATOR = nullptr);

	std::vector<VkExtensionProperties>* enumerateExtensionProperties(const std::string& layerName);

	std::vector<VkExtensionProperties>* enumerateExtensionProperties(const char* pLayerName = nullptr);

	std::vector<VkLayerProperties>* enumerateLayerProperties();

	std::vector<VkSparseImageFormatProperties>*
	getSparseImageFromatProperties(VkFormat format, VkImageType type, VkSampleCountFlagBits samples,
	                               VkImageUsageFlags usage, VkImageTiling tiling);

	QueueFamilyIndices getQueueFamilyIndices(Surface* surface);

	bool getSurfaceSupportKHR(uint32_t queueFamilyIndex, const Surface* surface) const;

	/// KHR

	void getSurfaceCapabilities(Surface surface, VkSurfaceCapabilitiesKHR* pSurfaceCapabilities) const;

	std::vector<VkSurfaceFormatKHR> getSurfaceFormats(Surface surface) const;

	void getSurfaceFormats(Surface surface, std::vector<VkSurfaceFormatKHR>& formats) const;

	std::vector<VkPresentModeKHR> getSurfacePresentModesKHR(Surface surface) const;

	void getSurfacePresentModesKHR(Surface surface, std::vector<VkPresentModeKHR>& modes) const;

	void querySwapchainSupport(Surface surface, SwapChainSupportDetails& details) const {
		getSurfaceCapabilities(surface, &details.capabilities);
		getSurfaceFormats(surface, details.formats);
		getSurfacePresentModesKHR(surface, details.presentModes);
	}

	/// end KHR

	std::string getName() const;
};

}

#endif //VULKANTEST_PHYSICALDEVICE_H
