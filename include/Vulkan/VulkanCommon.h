#ifndef VULKANTEST_VULKANCOMMON_H
#define VULKANTEST_VULKANCOMMON_H

#include "GlfwInclude.h"

#define ALLOCATOR const VkAllocationCallbacks* pAllocator

#include <stdexcept>

/**
 * VK_ASSERT
 */

#ifndef NDEBUG
#define VK_ASSERT(result, message) \
switch (result) { \
case VK_SUCCESS: case VK_NOT_READY: case VK_TIMEOUT: case VK_EVENT_SET: case VK_EVENT_RESET: case VK_INCOMPLETE: \
        break; \
default: \
        throw std::runtime_error(message); \
}
#else
#define VK_ASSERR(result, message) while(false){}
#endif



#endif //VULKANTEST_VULKANCOMMON_H
