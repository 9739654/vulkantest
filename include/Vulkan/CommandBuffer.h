#ifndef VULKANTEST_COMMANDBUFFER_H
#define VULKANTEST_COMMANDBUFFER_H

#include "GlfwInclude.h"

namespace vk {

class CommandBuffer {
private:
	VkCommandBuffer handle;

public:
	CommandBuffer();

	explicit CommandBuffer(VkCommandBuffer handle);

	VkCommandBuffer const& getHandle() const;

	VkCommandBuffer const& operator*() const;

	bool notValid() const;

	VkResult begin(const VkCommandBufferBeginInfo* pBeginInfo);

	VkResult end();

	VkResult reset(VkCommandBufferResetFlags flags);

	void bindPipeline(VkPipelineBindPoint pipelineBindPoint, VkPipeline pipeline);

	void setViewport(uint32_t firstViewport, uint32_t viewportCount, const VkViewport* pViewports);

	void setScissor(uint32_t firstScissor, uint32_t scissorCount, const VkRect2D* pScissors);

	void setLineWidth(float lineWidth);

	void setDepthBias(float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor);

	void setBlendConstants(const float blendConstants[4]);

	void setDepthBounds(float minDepthBounds, float maxDepthBounds);

	void setStencilComapreMask(VkStencilFaceFlags faceMask, uint32_t compareMask);

	void setStencilWriteMask(VkStencilFaceFlags faceMask, uint32_t writeMask);

	void SetStencilReference(VkStencilFaceFlags faceMask, uint32_t reference);

	void bindDescriptorSets(VkPipelineBindPoint pipelineBindpoint, VkPipelineLayout layout,
	                        uint32_t firstSet, uint32_t descriptorSetCount,
	                        const VkDescriptorSet* pDescriptorSets, uint32_t dynamicOffsetCount,
	                        const uint32_t* pDynamicOffsets);

	void bindIndexBuffer(VkBuffer buffer, VkDeviceSize offset, VkIndexType indexType);

	void
	bindVertexBuffers(uint32_t firstBinding, uint32_t bindingCount, const VkBuffer* pBuffers,
	                  const VkDeviceSize* pOffsets);

	void draw(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex,
	          uint32_t firstInstance);

	void drawIndexed(uint32_t indexCount, uint32_t instanceCount, uint32_t firstIndex,
	                 int32_t vertexOffset, uint32_t firstInstance);

	void
	drawIndirect(VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount, uint32_t stride);

	void drawIndexedIndirect(VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount,
	                         uint32_t stride);

	void dispatch(uint32_t groupCountX, uint32_t groupCountY, uint32_t groupCountZ);

	void dispatchIndirect(VkBuffer buffer, VkDeviceSize offset);

	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, uint32_t regionCount = 0,
	                const VkBufferCopy* pRegions = nullptr);

	void copyImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage,
	               VkImageLayout dstImageLayout, uint32_t regionCount = 0,
	               const VkImageCopy* pRegions = nullptr);

	void blitImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage,
	               VkImageLayout dstImageLayout, uint32_t regionCount,
	               const VkImageBlit* pRegions, VkFilter filter);

	void copyBufferToImage(VkBuffer srcBuffer, VkImage dstImage, VkImageLayout dstImageLayout, uint32_t regionCount, const VkBufferImageCopy* pRegions);

	void copyImageToBuffer(VkImage srcImage, VkImageLayout srcImageLayout, VkBuffer dstBuffer, uint32_t regionCount, const VkBufferImageCopy* pRegions);

	void updateBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize dataSize, const void* pData);

	void fillBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize size, uint32_t data);

	void clearColorImage(VkImage image, VkImageLayout imageLayout, const VkClearColorValue* pColor, uint32_t rangeCount, const VkImageSubresourceRange* pRanges);

	void clearDepthStencilImage(VkImage image, VkImageLayout imageLayout, const VkClearDepthStencilValue* pDepthStencil, uint32_t rangeCount, const VkImageSubresourceRange* pRanges);

	void clearAttachments(uint32_t attachmentCount, const VkClearAttachment* pAttachments, uint32_t rectCount, const VkClearRect* pRects);

	void resolveImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32_t regionCount, const VkImageResolve* pRegions);

	void setEvent(VkEvent event, VkPipelineStageFlags stageMask);

	void resetEvent(VkEvent event, VkPipelineStageFlags stageMask);

	void waitEvents(uint32_t eventCount, const VkEvent* pEvents, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, uint32_t memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, uint32_t bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, uint32_t imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers);

	void pipelineBarrier(VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, VkDependencyFlags dependencyFlags, uint32_t memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, uint32_t bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, uint32_t imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers);

	void beginQuery(VkQueryPool queryPool, uint32_t query, VkQueryControlFlags flags);

	void endQuery(VkQueryPool queryPool, uint32_t query);

	void resetQueryPool(VkQueryPool queryPool, uint32_t firstQuery, uint32_t queryCount);

	void writeTimestamp(VkPipelineStageFlagBits pipelineStage, VkQueryPool queryPool, uint32_t query);

	void copyQueryPoolResults(VkQueryPool queryPool, uint32_t firstQuery, uint32_t queryCount, VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize stride, VkQueryResultFlags flags);

	void pushConstants(VkPipelineLayout layout, VkShaderStageFlags stageFlags, uint32_t offset, uint32_t size, const void* pValues);

	void beginRenderPass(const VkRenderPassBeginInfo* pRenderPassBegin, VkSubpassContents contents);

	void nextSubpass(VkSubpassContents contents);

	void endRenderPass();

	void executeCommands(uint32_t commandBufferCount, const VkCommandBuffer* pCommandBuffers);

};

} // end namespace vk

#endif //VULKANTEST_COMMANDBUFFER_H
