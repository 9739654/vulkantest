#ifndef VULKANTEST_INSTANCE_H
#define VULKANTEST_INSTANCE_H

#include <stdexcept>
#include <vector>

#include "Common.h"

#include "Window.h"

#include "Vulkan/VulkanCommon.h"

class Window;

namespace vk {

class PhysicalDevice;

class Surface;

class Instance {
private:
	VkInstance handle;

public:

	Instance();

	explicit Instance(VkInstance handle);

	VkInstance getHandle() const;

	VkInstance operator*() const;

	static Instance create(VkInstanceCreateInfo* createInfo, ALLOCATOR = nullptr);

	/**
	 * Creates a Vulkan surface for the specified window.
	 * @param window The window to create the surface for.
	 * @return created surface
	 */
	Surface createSurface(Window& window, ALLOCATOR = nullptr);

	void destroy(ALLOCATOR = nullptr);

	/**
	 * Enumerates the physical devices accessible to a Vulkan instance.
	 * Retrieves a list of physical device objects representing the physical devices installed in the system.
	 * @return
	 */
	std::vector<PhysicalDevice>* enumeratePhysicalDevices();

	/**
	 * Return a function pointer for a command
	 * @param name is the name of the command to obtain.
	 */
	PFN_vkVoidFunction getProcAddr(const std::string& name);

	/// KHR

	void destroySurface(Surface& surface, ALLOCATOR = nullptr);

	/// OTHER

	void createDebugReportCallback(const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, ALLOCATOR, VkDebugReportCallbackEXT* pCallback);

	void createDebugReportCallback(const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
	                               VkDebugReportCallbackEXT* pCallback);

	void destroyDebugReportCallback(VkDebugReportCallbackEXT callback, ALLOCATOR = nullptr);

	static void enumerateExtensionProperties(const char* pLayerName, std::vector<VkExtensionProperties>& vec);

	static void enumerateExtensionProperties(const std::string& layerName, std::vector<VkExtensionProperties>& vec);

	static void enumerateExtensionProperties(std::vector<VkExtensionProperties>& vec);

	static void enumerateInstanceLayerProperties(std::vector<VkLayerProperties>& vec);
};

}

#endif //VULKANTEST_INSTANCE_H
