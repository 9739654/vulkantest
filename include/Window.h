#ifndef VULKANTEST_WINDOW_H
#define VULKANTEST_WINDOW_H

#include "GlfwInclude.h"

#include <string>

class Monitor;

enum ContextClientApi {
	OpenglApi = GLFW_OPENGL_API,
	OpenglEsApi = GLFW_OPENGL_ES_API,
	NoApi = GLFW_NO_API
};

enum ContextCreationApi {
	NativeApi = GLFW_NATIVE_CONTEXT_API,
	EglApi = GLFW_EGL_CONTEXT_API
};

struct ContextVersion {
	int major;
	int minor;
};

enum OpenglProfile {
	CoreProfile = GLFW_OPENGL_CORE_PROFILE,
	CompatibilityProfile = GLFW_OPENGL_COMPAT_PROFILE,
	AnyProfile = GLFW_OPENGL_ANY_PROFILE
};

enum ContextRobustness {
	NoResetNotification = GLFW_NO_RESET_NOTIFICATION,
	LoseOnReset = GLFW_LOSE_CONTEXT_ON_RESET,
	NoRobustness = GLFW_NO_ROBUSTNESS
};

enum ContextReleaseBehavior {
	AnyReleaseBehavior = GLFW_ANY_RELEASE_BEHAVIOR,
	ReleaseBehaviorFlush = GLFW_RELEASE_BEHAVIOR_FLUSH,
	ReleaseBehaviorNone = GLFW_RELEASE_BEHAVIOR_NONE
};

struct ContextHints {
	ContextClientApi contextClientApi = ContextClientApi::OpenglApi;
	ContextCreationApi contextCreationApi = ContextCreationApi::NativeApi;
	ContextVersion contextVersion = {1, 0};
	bool forwardCompatible = false; // TODO default value
	bool debugContext = false;
	OpenglProfile profile = OpenglProfile::AnyProfile;
	ContextRobustness contextRobustness = ContextRobustness::NoRobustness;
	ContextReleaseBehavior contextReleaseBehavior = ContextReleaseBehavior::AnyReleaseBehavior;
	bool noError = false;
};

struct MonitorHints {
	int refreshRate = GLFW_DONT_CARE;
};

struct FramebufferHints {
	int redBits = 8;
	int greenBits = 8;
	int blueBits = 8;
	int alphaBits = 8;
	int depthBits = 24;
	int stencilBits = 8;

	[[deprecated("Accumulation buffers are a legacy OpenGL feature and should not be used in new code.")]]
	struct {
		int redBits = 0;
		int greenBits = 0;
		int blueBits = 0;
		int alphaBits = 0;
	} accumulationBuffer;

	[[deprecated("Auxiliary buffers are a legacy OpenGL feature and should not be used in new code.")]]
	int auxBuffers;

	bool stereo = false;
	int samples = 0;
	bool srgbCapable = false;
	bool doubleBuffer = true;
};

struct WindowHints {
	bool resizable = true;
	bool visible = true;
	bool decorated = true;
	bool focused = true;
	bool autoIconify = true;
	bool floating = false; // always-on-top
	bool maximized = false;
};

/*struct WindowListener {
	std::function<void(int,int)> onSizeChanged;
	std::function<void(int,int)> onPositionChanged;
	std::function<void(int,int)> onFramebufferSizeChanged;
	std::function<void(void)> onCloseRequest;
	std::function<void(bool)> onFocused;
	std::function<void(bool)> onIconified;
	std::function<void(void)> onRefreshed;

	WindowListener() : onSizeChanged(nullptr), onPositionChanged(nullptr), onFramebufferSizeChanged(
		nullptr), onCloseRequest(nullptr), onFocused(nullptr), onIconified(nullptr), onRefreshed(
		nullptr) {

	}
};*/

class WindowListener {
public:
	virtual void onSizeChanged(int width, int height) {}

	virtual void onPositionChanged(int x, int y) {}

	virtual void onFramebufferSizeChanged(int width, int height) {}

	virtual void onCloseRequest() {}

	virtual void onFocused(bool active) {}

	virtual void onIconified(bool active) {}

	virtual void onRefreshed() {}

	virtual ~WindowListener() = default;
};

class Window {

public:
	enum WindowAttribute {
		Focused = GLFW_FOCUSED,
		Iconified = GLFW_ICONIFIED,
		Maximized = GLFW_MAXIMIZED,
		Visible = GLFW_VISIBLE,
		Resizable = GLFW_RESIZABLE,
		Decorated = GLFW_DECORATED,
		Floating = GLFW_FLOATING
	};

private:
	int width, height;

	GLFWwindow* handle = nullptr;

	WindowListener* listener = nullptr;

public:
	explicit Window(int width = 640, int height = 480);

	virtual ~Window();

	void init();

	GLFWwindow* getHandle() const;

	GLFWwindow* operator*() const;

	void destroy();

	bool shouldClose() const;

	void setShouldClose(bool active);

	void setTitle(const std::string& title);

	void setTitle(const char* title);

	void setIcon();

	void getPosition(int* x, int* y) const;

	void setPosition(int x, int y);

	void getSize(int* width, int* height) const;

	void setSizeLimits(int minWidth, int minHeight, int maxWidth, int maxHeight);

	void setAspectRatio(int numer, int denom);

	void setSize(int width, int height);

	void getFramebufferSize(int* width, int* height) const;

	void getWindowFrameSize(int* left, int* top, int* right, int* bottom) const;

	void iconify();

	void restore();

	void maximize();

	void show();

	void hide();

	void focus();

	int getAttribute(WindowAttribute attribute) const;

	void swapBuffers();

	void makeContextCurrent();

	bool isFullscreen() const;

	Monitor getMonitor() const;

	void setMonitor(Monitor* monitor);

	WindowListener* getListener();

	void setListener(WindowListener* listener);

	static void pollEvents();

	static Window* getCurrentContext();

private:

	void setCallbacks();

	void removeCallbacks();


};


#endif //VULKANTEST_WINDOW_H
