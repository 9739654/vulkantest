#ifndef VULKANTEST_IMAGELOADER_H
#define VULKANTEST_IMAGELOADER_H

#include <string>
#include <memory>

struct Image {
	unsigned char* data;
	unsigned int width;
	unsigned int height;
	unsigned int channels;
	unsigned int desiredChannels;

	Image();
	unsigned long size();
	virtual ~Image();
};

class ImageLoader {
public:
	std::unique_ptr<Image> loadFromFile(const std::string& filename);
};

#endif //VULKANTEST_IMAGELOADER_H
