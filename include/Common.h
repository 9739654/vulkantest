#ifndef VULKANTEST_COMMON_H
#define VULKANTEST_COMMON_H

#define ENABLE_IMGUI

/// ENABLE VALIDATION LAYERS

#ifndef NDEBUG
#define ENABLE_VALIDATION_LAYERS
#endif

/// ASSERT

#ifndef NDEBUG
#define ASSERT(test, message) \
if (!(test)) { \
        throw std::runtime_error(message); \
}
#else
#define ASSERT(test, message) while(false){}
#endif

/// DEBUG_LOG

#ifndef NDEBUG
#define IFDEBUG(proc) proc
#else
#define IFDEBUG(proc) while(false){}
#endif

#define MIN2(a, b) ((a) < (b) ? (a) : (b))
#define MIN3(a, b, c) MIN2(MIN2((a), (b)), (c))

#define MAX2(a, b) ((a) > (b) ? (a) : (b))


template<template<class, class, class...> class C, typename K, typename V, typename... Args>
V getWithDef(const C<K, V, Args...>& map, K const& key, const V& defval) {
	typename C<K, V, Args...>::const_iterator it = map.find(key);
	if (it == map.end()) {
		return defval;
	}
	return it->second;
}

#endif //VULKANTEST_COMMON_H
