#ifndef VULKANTEST_MODELLOADER_H
#define VULKANTEST_MODELLOADER_H

#include "GlfwInclude.h"
#include "GlmInclude.h"

#include <string>
#include <bits/unique_ptr.h>
#include <vector>

#include <glm/glm.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>

struct Vertex {
	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec2 texCoord;

	static VkVertexInputBindingDescription getBindingDescription() {
		VkVertexInputBindingDescription bindingDescription = {};
		bindingDescription.binding = 0;
		bindingDescription.stride = sizeof(Vertex);
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindingDescription;
	}

	static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions() {
		std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions = {};

		attributeDescriptions[0].binding = 0;
		attributeDescriptions[0].location = 0;
		attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[0].offset = static_cast<uint32_t>(offsetof(Vertex, pos));

		attributeDescriptions[1].binding = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[1].offset = static_cast<uint32_t>(offsetof(Vertex, normal));

		attributeDescriptions[2].binding = 0;
		attributeDescriptions[2].location = 2;
		attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
		attributeDescriptions[2].offset = static_cast<uint32_t>(offsetof(Vertex, texCoord));

		return attributeDescriptions;
	}

	bool operator==(const Vertex& other) const {
		return pos == other.pos && normal == other.normal && texCoord == other.texCoord;
	}
};

struct Texture {
	unsigned int id;
	std::string type;
	std::string filename;
};

namespace std {
template<>
struct hash<Vertex> {
	size_t operator()(Vertex const& vertex) const {
		return ((hash<glm::vec3>()(vertex.pos) ^ (hash<glm::vec3>()(vertex.normal) << 1)) >> 1) ^
		       (hash<glm::vec2>()(vertex.texCoord) << 1);
	}
};
}

enum MeshTopology {
	Triangles, Quads
};

struct Mesh {
	std::vector<Vertex> vertices;
	std::vector<uint32_t> indices;
	std::vector<Texture> textures;
	MeshTopology topology = Triangles;

};

struct Model {
	std::vector<Mesh> meshes;
	std::string directory;

	unsigned long vertexCount() const {
		unsigned long count = 0;
		for (const auto& mesh : meshes) {
			count += mesh.vertices.size();
		}
		return count;
	}

	unsigned long faceCount() const {
		unsigned long count = 0;
		for (const auto& mesh : meshes) {
			count += mesh.indices.size() / 3;
		}
		return count;
	}

};

/**
 *
 */
class ModelLoader {
public:
	virtual ~ModelLoader() = default;

	virtual Model* loadFromFile(const std::string& filename) = 0;
};

/**
 *
 */
class TinyobjLoader : public ModelLoader {
public:
	Model* loadFromFile(const std::string& filename) override;
};

/**
 *
 */
class AssimpLoader : public ModelLoader {
private:
	Assimp::Importer importer;
	Model* model;

public:
	Model* loadFromFile(const std::string& filename) override;

	void processNode(aiNode* node, const aiScene* scene);

	Mesh processMesh(aiMesh* srcMesh, const aiScene* scene);

	void loadTextures(aiMaterial* mat, aiTextureType type, const std::string& typeName, std::vector<Texture>& vec);
};

#endif //VULKANTEST_MODELLOADER_H
