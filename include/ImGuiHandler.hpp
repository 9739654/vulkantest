#ifndef IMGUIHANDLER_HPP
#define IMGUIHANDLER_HPP

#include <utility>
#include <string>
#include <limits>
#include "../GLFWIncludes.hpp"
#include <imgui/imgui.h>
#include "../Events/Event.hpp"
#include "../Events/KeyboardEvent.hpp"
#include "../Events/MouseEvent.hpp"
#include "ImGuiFunctions.hpp"
#include "../VulkanHandler.hpp"

class ImGuiHandler
{
public:
	typedef const char*(*ImGuiClipboardGetFun)(void*);
	typedef void(*ImGuiClipboardSetFun)(void*, const char*);

	ImGuiHandler(VulkanHandler& vulkanHandler, const std::string fontPath, float fontSize, const std::pair<float, float> mousePosition);
	~ImGuiHandler();

	bool isInitialized() const;
	bool doRenderFailed() const;
	bool doesImGuiWantsKeyboard() const;
	bool doesImGuiWantsMouse() const;
	bool doesImGuiWantsTextInput() const;
	bool setClipboardCallbacks(ImGuiClipboardGetFun clipboardGetCallback, ImGuiClipboardSetFun clipboardSetCallback, void* userData);
	void handleEvent(const Event& event);
	void newFrame(const float deltaTime, float width, float height);
	void render();

private:
	static const int IMGUI_MOUSE_BUTTONS_COUNT = 5;
	static const float DEFAULT_FONT_SIZE;

	VulkanHandler& vulkanHandler;
	int keyCode;
	float mouseX;
	float mouseY;
	float mouseScrollOffset;
	bool imGuiWantsKeyboard;
	bool imGuiWantsMouse;
	bool imGuiWantsTextInput;
	bool mouseButtonDown[IMGUI_MOUSE_BUTTONS_COUNT];
	VkSampler fontSampler = VK_NULL_HANDLE;
	VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
	VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
	VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
	VkPipeline pipeline = VK_NULL_HANDLE;
	VkPipelineCreateFlags pipelineCreateFlags = 0;
	VkBuffer vertexBuffer[VulkanHandler::VK_QUEUED_FRAMES] = {};
	size_t vertexBufferSize[VulkanHandler::VK_QUEUED_FRAMES] = {};
	size_t bufferMemoryAlignment = 256;
	VkDeviceMemory vertexBufferMemory[VulkanHandler::VK_QUEUED_FRAMES] = {};
	VkBuffer indexBuffer[VulkanHandler::VK_QUEUED_FRAMES] = {};
	size_t indexBufferSize[VulkanHandler::VK_QUEUED_FRAMES] = {};
	VkDeviceMemory indexBufferMemory[VulkanHandler::VK_QUEUED_FRAMES] = {};
	VkDeviceMemory fontMemory = VK_NULL_HANDLE;
	VkImage fontImage = VK_NULL_HANDLE;
	VkImageView fontView = VK_NULL_HANDLE;
	VkDeviceMemory uploadBufferMemory = VK_NULL_HANDLE;
	VkBuffer uploadBuffer = VK_NULL_HANDLE;
	bool initialized;
	bool renderFailed;

	void handleKeyboardEvent(const KeyboardEvent& event);
	void handleMouseEvent(const MouseEvent& event);
	static void imGuiRenderDrawLists(ImDrawData* draw_data);
	bool vulkanCreateDeviceObjects();
	void vulkanInvalidateDeviceObjects();
	void vulkanInvalidateFontUploadObjects();
	bool vulkanCreateFontsTexture(VkCommandBuffer command_buffer);
	bool uploadFonts();

	static const uint32_t glslVertShaderSpv[];
	static const uint32_t glslFragShaderSpv[];
};

#endif // IMGUIHANDLER_HPP
